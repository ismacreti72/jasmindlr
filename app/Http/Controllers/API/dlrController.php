<?php

namespace App\Http\Controllers\API;

use App\DLR;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;


class dlrController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dlrJasmin(Request $request){
        $data = array(
            "uid" => $request->get("id"),
            "connector" => $request->get("connector"),
            "message_status" => $request->get("message_status"),
            "done_date" => $request->get("donedate"),
            "sub" => $request->get("sub"),
            "level" => $request->get("level"),
            "message" => $request->get("text"),
            "error" => $request->get("err"),
            "id_smsc" => $request->get("id_smsc"),
            "delivred" => $request->get("dlvrd"),
            "submit_date" => $request->get("subdate")
        );

        DLR::create($data);
        $content = "ACK/Jasmin";
        return response()->json($content, 200);
    }
}
