<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\SendDLR::class,
        Commands\SendReport::class,
        Commands\SendScheduleSMS::class,
        Commands\SendEventSMS::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        //Sending SMS cron services *******************************
        /*$schedule->command('DLR:send')
            ->everyFiveMinutes()
            ->runInBackground();
        $schedule->command('ScheduleSMS:send')
            ->everyTenMinutes()
            ->runInBackground();
        $schedule->command('EventSMS:send')
            ->everyFifteenMinutes()
            ->runInBackground();*/
        //*********************************************************


        //General SMS Usage statistics ****************************
        $schedule->command('DailyReport:send')
            ->dailyAt('4:00')
            ->runInBackground();
        $schedule->command('WeeklyReport:send')
            ->weeklyOn(1,'4:30')
            ->runInBackground();
        $schedule->command('MonthlyReport:send')
            ->monthlyOn(1, '4:50')
            ->runInBackground();
        //*********************************************************


        //General CRMSMS Usage statistics ****************************
        $schedule->command('CRMSMS:send')
            ->dailyAt('11:00')
            ->runInBackground();
        $schedule->command('CRMSMS:send')
            ->dailyAt('16:00')
            ->runInBackground();
        //*********************************************************
        //Send Balance recall alert to user ***********************
      /*  $schedule->command('BalanceRecall:send')
            ->weeklyOn(1,'9:20')
            ->runInBackground();
        //*********************************************************


        //Customer usage report statistics ************************
        $schedule->command('WeeklyUsageReport:send')
            ->weeklyOn(1, '9:00')
            ->runInBackground();
        $schedule->command('MonthlyUsageReport:send')
            ->monthlyOn(1, '9:30')
            ->runInBackground();*/
        //*********************************************************

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
