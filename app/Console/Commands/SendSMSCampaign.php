<?php

namespace App\Console\Commands;

use App\Library\Services\Contracts\SMSAPIServiceInterface;
use Illuminate\Console\Command;

class SendSMSCampaign extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Campaign:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send campaign';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(SMSAPIServiceInterface $SMSAPIServiceInterface)
    {

        try {

            $this->info('['.date("Y-m-d H:i:s").'] Cron campaign started ');
            $this->sendcampaign($SMSAPIServiceInterface);
        }catch (\Exception $e){
            $this->info('['.date("Y-m-d H:i:s").'] Cron campaign started  with error = ' . $e->getMessage());
        }

        return 'cron started';
    }


    public function sendcampaign($SMSAPIServiceInterface){

        $message = "Apportez votre soutien aux DEMUNIS en assistant au CONCERT GOSPEL ce dimanche 30 Juin à 16h au Castel Hall. Infos : 676 727 564. Découvrez les Artistes surprise en cliquant sur : cutit.org/G7pav.  Entrée GRATUITE";
        //$contacts = SubUserContact::inRandomOrder()->offset(0)->limit(1920)->get();
        //$this->info('['.date("Y-m-d H:i:s").'] Count =  '. count($contacts));

        //$SMSAPIServiceInterface->sendsmsapi('stdadmin','Admin@1','678018812,696653522,',$message,'BARAKAH');

        /*foreach ($contacts as $contact){
            $this->info('['.date("Y-m-d H:i:s").'] MSISDN =  '. $contact->msisdn);
            $SMSAPIServiceInterface->sendsmsapi('stdadmin','Admin@1',$contact->msisdn,$message,'BARAKAH');
        }*/

        /*$contactsjeff = "237677181238,237670179127,237678340464,237677181238,237675194684,237650462102,237693416730,237693416730,237675194684,237675194684,
        237677181238,237677762892,237691307138,237694394963,237697124449,237690873151,237693533523,237693846877,237698406595,237678635339,
        237696781268,237673182109,237655274547,237656630933,237653870490,237656351075,237670062888,237673877711,237691079316,237695427930,
        237695427930,237698911164,237677171213,237691047079,237695329208,237696327307,237696352875,237698753122,237690692240,237672548363,
        237695545738,237653466542,237671829928,237696227726,237695003359,237697082455,237650304857,237656313721,237698138252,237698157934,
        237698157934,237698167985,237697329827,237676727564,237656504320,237696665704,237699949228,237696636403,237699481806,237650103745,
        237698365686,237675790402,237690804383,237696746823,237694155977,237693807261,237696636403,237678018812,237695797443,237691606405,
        237697394323,237696653522,237694982052,237695172574,237697782241,237697024372,237694320818,237693457143,237693390496,237695511705,237699165356";

        $SMSAPIServiceInterface->sendsmsapi('stdadmin','Admin@1','675764759',$message,'BARAKAH');*/
        $this->info('['.date("Y-m-d H:i:s").'] Cron campaign ended ');

    }

}

