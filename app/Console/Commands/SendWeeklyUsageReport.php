<?php

namespace App\Console\Commands;

use App\Library\Services\Contracts\MailServiceInterface;
use App\Library\Services\Contracts\SMSAPIServiceInterface;
use App\Library\Services\CoreSMSAPI;
use App\Models\ApiConfigs\Settings;
use App\Models\Sms\MessageAPI;
use App\Models\User\SubUser;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendWeeklyUsageReport extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'WeeklyUsageReport:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Weekly SMS Usage Report to Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(SMSAPIServiceInterface $SMSAPIServiceInterface, MailServiceInterface $mailService)
    {

        try {

            $this->info('['.date("Y-m-d H:i:s").'] Cron Weekly SMS Usage Started');
            $settings = Settings::first();
            $this->sendUsageReport($settings, $SMSAPIServiceInterface);
            $this->info('['.date("Y-m-d H:i:s").'] Cron Weekly SMS Usage End');

        }catch (\Exception $e){
            $this->info('['.date("Y-m-d H:i:s").'] Cron Weekly SMS Usage  with error = ' . $e->getMessage());
        }

        return 'Cron fired';
    }


    public function sendUsageReport($settings, SMSAPIServiceInterface $SMSAPIServiceInterface){

        $currentDate = Carbon::now();
        $from = $currentDate->subDays($currentDate->dayOfWeek - 1)->subWeek()->toDateString(). " 00:00:00";
        $currentDate = Carbon::now();
        $to = $currentDate->subDays($currentDate->dayOfWeek)->toDateString(). " 23:59:59";
        $PERIOD = "durant la semaine écoulée";
        $core = new CoreSMSAPI();

        $users = SubUser::where('is_admin', true)
            ->where('notif_report_enabled', true)
            ->get();

        foreach ($users as $user){

            $sms = MessageAPI::where('master_subuser_id', $user->id)
                ->whereNotNull("messageid")
                ->whereBetween('created_at', [$core->dateToUTC($from), $core->dateToUTC($to)])
                ->orWhere('sub_user_id', $user->id)
                ->whereBetween('created_at', [$core->dateToUTC($from), $core->dateToUTC($to)])
                ->whereNotNull("messageid")
                ->latest()
                ->get();

            $SMS_UNIT_COUNT = 0;
            $CREDIT = $user->credit;
            foreach ($sms as $message){
                $SMS_UNIT_COUNT = $SMS_UNIT_COUNT + $message->total_sms_unit;
            }
            //send notif

            /*
             * Cher Client, vous avez consommé $CREDIT_LEFT SMS $PERIOD. Votre solde actuel est de $CREDIT SMS.
             * Nous vous remercions pour votre confiance.
             */

            //$this->info("user = " . $user->username . " & SMS Unit = " . $SMS_UNIT_COUNT);
            $message = str_replace(array('$SMS_UNIT_COUNT','$CREDIT', '$PERIOD'), array($SMS_UNIT_COUNT, $CREDIT, $PERIOD), $settings->report_usage_message);
            $SMSAPIServiceInterface->sendsms('nexahusr','nex@hu5r',$user->phone, $message, 'NEXAH', null,null,null);

        }
    }

}

