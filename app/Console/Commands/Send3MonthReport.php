<?php

namespace App\Console\Commands;

use App\Library\Services\Contracts\SMSAPIServiceInterface;
use App\Models\ApiConfigs\Settings;
use App\Models\Sms\MessageAPI;
use App\Models\User\SubUser;
use Carbon\Carbon;
use Illuminate\Console\Command;

class Send3MonthReport extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ThreeMonthReport:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily SMS usage repport to administrator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(SMSAPIServiceInterface $SMSAPIServiceInterface)
    {

        try {

            dd(config("app.timezone"), date('Y-m-d H:i:s'));
            $this->info('['.date("Y-m-d H:i:s").'] Cron report started ');
            $settings = Settings::first();
            if ($settings->sendailyrepport == 1) {
                $this->sendSMSReport($settings,$SMSAPIServiceInterface);
            }else{
                $this->info('['.date("Y-m-d H:i:s").'] Cron report disabled ');
            }
            $this->info('['.date("Y-m-d H:i:s").'] Cron report ended ');
        }catch (\Exception $e){
            $this->info('['.date("Y-m-d H:i:s").'] Cron report started  with error = ' . $e->getMessage());
        }

        return 'cron started';
    }


    public function sendSMSReport($settings, $SMSAPIServiceInterface){

        $day =  Carbon::today()->toDateString();
        $admin = SubUser::find($settings->admin_subuser_id);
        if ($settings->sms_report_date != $day){
            $this->info('['.date("Y-m-d H:i:s").'] Sending SMS report started ');

            $info = $this->report();
            $message = "DAILY SMS REPORT : " . Carbon::yesterday()->toDateString().
                "\nSent : ". $info['sent'] .
                "\nDelivered : " . $info['delivered'] . " -> " . $info['dlvr_rate'] .
                "\nUndelivered : ". $info['undelivered'] . " -> " . $info['undlvr_rate'].
                "\n\nActive Users : ". implode(',',$info['active_users']);
            $SMSAPIServiceInterface->sendsms($admin->username,$admin->password,$admin->phone,
                $message,$admin->senderid,null,null,null);
            $settings->sms_report_date = $day;
            $settings->save();
            $this->info('['.date("Y-m-d H:i:s").'] Sending SMS report ended ');
        }
    }


    public function report(){

        $from = Carbon::yesterday()->toDateString().' 00:00:00';
        $to = Carbon::yesterday()->toDateString().' 23:59:59';

        $smsarr = MessageAPI::whereBetween('created_at', [$from, $to])->get();
        $nbsent = count($smsarr);
        $this->info('count = ' . $nbsent);

        $info = array(
            'from' => $from,
            'to' => $to,
            'sent' => $nbsent,
            'delivered' => 0,
            'dlvr_rate' => "0%",
            'undelivered' => 0,
            'undlvr_rate' => "0%",
            'active_users' => array()
        );
        foreach ($smsarr as $sms){
            if ($sms->dlrstatus == 'DELIVRD'){
                $info['delivered'] =  $info['delivered'] + 1;
            }else if($sms->dlrstatus == 'UNDELIV'){
                $info['undelivered'] =  $info['undelivered'] + 1;
            }
            if (!in_array($sms->username, $info['active_users'])){
                array_push($info['active_users'], $sms->username);
            }
        }

        $info['dlvr_rate'] =  $nbsent==0?'0%':round(($info['delivered'] * 100)/ $nbsent, 2).'%';
        $info['undlvr_rate'] =  $nbsent==0?'0%':round(($info['undelivered'] * 100)/ $nbsent, 2).'%';
        return $info;

    }


}

