<?php

namespace App\Console\Commands;

use App\Library\Services\Contracts\MailServiceInterface;
use App\Library\Services\Contracts\SMSAPIServiceInterface;
use App\Models\EventManagement\Event;
use App\Models\EventManagement\EventContact;
use App\Models\Sms\NexahEntreprise;
use App\Models\Sms\NexahUser;
use App\Models\User\SubUser;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendEventSMS extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'EventSMS:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send report message';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(MailServiceInterface $mailService)
    {

        try {

            $this->info('['.date("Y-m-d H:i:s").'] Cron event message started ');
            $this->DailyReport($mailService);
            $this->WeeklyReport($mailService);
            //$this->DailyReport($mailService);
            $this->info('['.date("Y-m-d H:i:s").'] Cron event message ended ');
        }catch (\Exception $e){
            Log::info('['.date("Y-m-d H:i:s").'] Cron event message started  with error = ' . $e->getMessage());
        }
        //Log::info("CRON EVENT SMS FIRED");
        return 'Cron fired';
    }

    public function DailyReport(MailServiceInterface $mailService){

        $date = Carbon::yesterday()->toDateString();
        $from = $date.' 00:00:00';
        $to = $date.' 23:59:59';
        $arrayuser =  NexahUser::all();
        $count = array();
        foreach ($arrayuser as $user){
            $entreprise = NexahEntreprise::where('user_id',$user->id)->where('created_at', '<',$from)
                ->where('created_at', '>',$to)->get();
            array_push($count,array("name"=>$user->name,"entreprise"=>count($entreprise)));
        }
        $data['title'] = 'Daily Report Sales';
        $mailService->sendReportMail($data,$count);
    }

    public function WeeklyReport(MailServiceInterface $mailService){

        $searchDay = 'sunday';
        $searchDay2 = 'Saturday';
        $searchDate = new Carbon(); //or whatever Carbon instance you're using
        $lastSunday = Carbon::createFromTimeStamp(strtotime("last $searchDay", $searchDate->timestamp));
        $lastSaturday = Carbon::createFromTimeStamp(strtotime("last $searchDay2", $searchDate->timestamp));

        Log::info("last monday ".$lastSunday." last saturday ".$lastSaturday);
        $arrayuser =  NexahUser::all();
        $count = array();
        foreach ($arrayuser as $user){
            $entreprise = NexahEntreprise::where('user_id',$user->id)->where('created_at', '<',$lastSunday)
                ->where('created_at', '>',$lastSaturday) ->get();
            array_push($count,array("name"=>$user->name,"entreprise"=>count($entreprise)));
        }
        $data['title'] = 'Daily Report Sales';
        $mailService->sendReportMail($data,$count);
    }

}

