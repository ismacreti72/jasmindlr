<?php

namespace App\Console\Commands;

use App\Library\Services\Contracts\MailServiceInterface;
use App\Library\Services\Contracts\SMSAPIServiceInterface;
use App\Models\ApiConfigs\Settings;
use App\Models\Sms\MessageAPI;
use App\Models\Sms\SMSDLR;
use App\Models\User\SubUser;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SendDLR2 extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DLR:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Delivery Receipt on registred Customer account URL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(SMSAPIServiceInterface $SMSAPIServiceInterface, MailServiceInterface $mailService)
    {

        try {

            $this->info('['.date("Y-m-d H:i:s").'] Cron started ');

            $settings = Settings::first();
            if ($settings->dlrservicestatus == 1) {
                //Fetch user with DR URL
                //$this->resendDLR();
                //$this->getMtnDLR();
                //$this->sendAllDLR();
                ////$this->getDLR();
                ////$this->updateDLR();
                //$this->getDLRTeleoss($SMSAPIServiceInterface);

                //$this->resendUndelivMTNSMSWithMTN($settings);
                //$this->resendUndelivMTNSMSWithOCM();
                //$this->resendUndelivOCMSMSWithOCM();
                //$this->resendUnknownDelivMTNSMSWithMTN();
                //$this->resendUndelivOCMSMSWithOCM2($settings);
                $this->sendAllDLR();
                //$this->getDLRTeleoss($SMSAPIServiceInterface);
                $this->sendAllDLR();
                //$this->sendScheduledSMS();
                //$this->sendEventMessage($SMSAPIServiceInterface);
                //$this->createScheduleEventMessage($SMSAPIServiceInterface);

            }
            /*if ($settings->sendailyrepport == 1) {
                $this->sendSMSReport($settings,$SMSAPIServiceInterface);
            }*/
            //Log::info('['.date("Y-m-d H:i:s").'] Cron Send DLR  with OK = ');

        }catch (\Exception $e){
            $this->info('['.date("Y-m-d H:i:s").'] Cron Send DLR  with error = ' . $e->getMessage());
        }

        return 'Cron fired';
    }

    /*public function getMtnDLR(){
        $msg = MessageAPI::whereIn('network', array('MTN'))
            ->where('created_at', '>=', Carbon::today()->subDays(1)->toDateTimeString())
            ->get();
        $this->info('['.date("Y-m-d H:i:s").'] Getting MTN DLR start count = '. count($msg));
        foreach ($msg as $message) {
            $this->requestMtnDlr($message->smsclientid);
        }
    }*/

    /*public function requestMtnDlr($messageId){

        $sms_args = array(
            'messageId' => $messageId
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://smsvas.com/operatorsmsapi/public/index.php/api/v1/requestMtnDLR');
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sms_args, '', '&'));
        $response = curl_exec($ch);
        $result = json_decode($response, true);
        curl_close($ch);
        $this->info('['.date("Y-m-d H:i:s").'] RequestMtnDlr result = '. json_encode($result));
    }*/

    public function getDLR(){

        $msg = MessageAPI::where('responsecode','=', 2)
            ->whereIn('network', array('OCM','BANKAI'))
            ->where('created_at', '>=', Carbon::today()->subDays(2)->toDateTimeString())
            ->get();

        $this->info('['.date("Y-m-d H:i:s").'] Getting Orange DLR start count = '. count($msg));
        foreach ($msg as $message) {
            $dlrsms = SMSDLR::find($message->smsclientid);
            if ($dlrsms != null) {
                $deliveryStatus = $dlrsms->deliveryStatus;
                $this->info('[' . date("Y-m-d H:i:s") . '] DLR = ' . $deliveryStatus);

                if ($deliveryStatus == 'DeliveredToTerminal') {
                    $message->actualerrcode = '200';
                    $message->dlrstatus = 'DELIVRD';
                    $message->responsecode = 1;
                    $message->responsedescription = 'Success';
                } else if ($deliveryStatus == 'DeliveredToNetwork') {
                    $message->actualerrcode = '201';
                    $message->dlrstatus = 'UNDELIV';
                    $message->responsecode = 0;
                    $message->responsedescription = 'Undelivered';
                } else if ($deliveryStatus == 'DeliveryImpossible') {
                    $message->actualerrcode = '355';
                    $message->dlrstatus = 'UNDELIV';
                    $message->responsecode = 0;
                    $message->responsedescription = 'Undelivered';
                } else if ($deliveryStatus == 'MessageWaiting') {
                    $message->actualerrcode = '354';
                    $message->dlrstatus = 'UNDELIV';
                    $message->responsecode = 0;
                    $message->responsedescription = 'Undelivered';
                }else if($deliveryStatus == 'DeliveryUncertain'){
                    $message->actualerrcode = '356';
                    $message->dlrstatus = 'UNDELIV';
                    $message->responsecode = 0;
                    $message->responsedescription = 'Undelivered';
                }

                $message->details = $dlrsms->deliveryStatus;
                $message->undeliveredreason = $dlrsms->deliveryStatus;
                if ( $message->submittime == null &&  $message->senttime == null){
                    $message->submittime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($message->created_at)));
                    $message->senttime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($message->created_at)));
                }

                $message->deliverytime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($dlrsms->deliverytime)));
                $message->dlrsent = 0;
                $message->save();
            }
        }
        $this->info('['.date("Y-m-d H:i:s").'] Getting Orange DLR end');

    }

    public function updateDLR(){

        $msg = MessageAPI::where('created_at', '>=', Carbon::today()->subDays(2)->toDateTimeString())
            ->whereIn('network', array('OCM','BANKAI'))
            ->where('details','<>', 'DeliveredToTerminal')
            ->get();
        $this->info('['.date("Y-m-d H:i:s").'] Updating Orange DLR start');

        foreach ($msg as $message) {

            $dlrsms = SMSDLR::find($message->smsclientid);
            if ($dlrsms != null) {
                $deliveryStatus = $dlrsms->deliveryStatus;
                if ($deliveryStatus != $message->details) {
                    $this->info('[' . date("Y-m-d H:i:s") . '] DLR = ' .$message->details.' -> '. $deliveryStatus);

                    if ($deliveryStatus == 'DeliveredToTerminal') {
                        $message->actualerrcode = '200';
                        $message->dlrstatus = 'DELIVRD';
                        $message->responsecode = 1;
                        $message->responsedescription = 'Success';
                    } else if ($deliveryStatus == 'DeliveredToNetwork') {
                        $message->actualerrcode = '201';
                        $message->dlrstatus = 'UNDELIV';
                        $message->responsecode = 0;
                        $message->responsedescription = 'UNDELIV';
                    } else if ($deliveryStatus == 'DeliveryImpossible') {
                        $message->actualerrcode = '355';
                        $message->dlrstatus = 'UNDELIV';
                        $message->responsecode = 0;
                        $message->responsedescription = 'Undelivered';
                    } else if ($deliveryStatus == 'MessageWaiting') {
                        $message->actualerrcode = '354';
                        $message->dlrstatus = 'UNDELIV';
                        $message->responsecode = 0;
                        $message->responsedescription = 'Undelivered';
                    }else if($deliveryStatus == 'DeliveryUncertain'){
                        $message->actualerrcode = '356';
                        $message->dlrstatus = 'UNDELIV';
                        $message->responsecode = 0;
                        $message->responsedescription = 'Undelivered';
                    }

                    $message->details = $dlrsms->deliveryStatus;
                    $message->undeliveredreason = $dlrsms->deliveryStatus;
                    $message->dlrsent = 0;
                    if ( $message->submittime == null &&  $message->senttime == null){
                        $message->submittime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($message->created_at)));
                        $message->senttime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($message->created_at)));
                    }

                    $message->deliverytime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($dlrsms->deliverytime)));
                    $message->save();
                }
            }
        }
        $this->info('['.date("Y-m-d H:i:s").'] Updating Orange DLR end');

    }


    public function sendAllDLR(){

        /*$users =  DB::table('api_users')
            ->select('username')
            ->where('has_dlr_url','=',true)
            ->get();*/
        $users =  DB::table('api_sub_users')
            ->select('username')
            //->where('has_dlr_url','=',true)
            ->whereNotNull('batch_dlr_url')
            ->latest()
            ->get();
        $usernames = collect($users)->map(function($x){ return (array)$x; })->toArray();

        $messages = DB::table('api_messages')
            ->select((DB::raw('*')))
            ->whereIn('dlrstatus', array('DELIVRD','UNDELIV'))
            ->where('dlrsent','=', false)
            ->where('created_at', '>=', Carbon::today()->subDays(1)->toDateTimeString())
            ->whereIn('username', $usernames)
            ->latest()->get();

        $this->info('['.date("Y-m-d H:i:s").'] Sent All DLR : Started count = '. sizeof($messages));

        $users = array();
        $userlist = array();
        for ($i=0; $i<sizeof($messages); $i++){
            if (!in_array($messages[$i]->username,$userlist)) {
                array_push($userlist, $messages[$i]->username);
                //$this->info('[' . date("Y-m-d H:i:s") . '] user['.$i.'] =  ' . $messages[$i]->username);

                $dlrlist = array();
                $userdlr = array(
                    'username' => $messages[$i]->username,
                    'password' => $messages[$i]->password,
                    'dlrlist' => $dlrlist
                );
                for ($j=0; $j<sizeof($messages); $j++) {

                    if ($messages[$j]->username === $messages[$i]->username) {
                        //$this->info('[' . date("Y-m-d H:i:s") . '] username['.$j.']  =  ' . $messages[$j]->username);
                        array_push($dlrlist, $messages[$j]);
                    }
                }

                array_push($userdlr['dlrlist'], $dlrlist);
                array_push($users, $userdlr);

            }
        }

        $this->sendbachdlr($users);
        $this->info('['.date("Y-m-d H:i:s").'] Sent All DLR : Ended');
    }


    public function resendDLR(){

        $messages = DB::table('api_messages')
            ->select((DB::raw('*')))
            //->whereIn('dlrstatus', array('DELIVRD','UNDELIV'))
            ->where('dlrsent','=', -1)
            ->whereBetween('created_at', ['2019-08-01 00:00:00','2019-08-01 23:59:59'])
            ->where('username', 'skysoft')
            ->latest()->get();

        $this->info('['.date("Y-m-d H:i:s").'] resent  DLR : Started count = '. sizeof($messages));

        $users = array();
        $userlist = array();
        for ($i=0; $i<sizeof($messages); $i++){
            if (!in_array($messages[$i]->username,$userlist)) {
                array_push($userlist, $messages[$i]->username);
                //$this->info('[' . date("Y-m-d H:i:s") . '] user['.$i.'] =  ' . $messages[$i]->username);

                $dlrlist = array();
                $userdlr = array(
                    'username' => $messages[$i]->username,
                    'password' => $messages[$i]->password,
                    'dlrlist' => $dlrlist
                );
                for ($j=0; $j<sizeof($messages); $j++) {

                    if ($messages[$j]->username === $messages[$i]->username) {
                        //$this->info('[' . date("Y-m-d H:i:s") . '] username['.$j.']  =  ' . $messages[$j]->username);
                        array_push($dlrlist, $messages[$j]);
                    }
                }

                array_push($userdlr['dlrlist'], $dlrlist);
                array_push($users, $userdlr);

            }
        }

        $this->sendbachdlr($users);
        $this->info('['.date("Y-m-d H:i:s").'] Sent All DLR : Ended');
    }


    public function getDLRTeleoss(SMSAPIServiceInterface $SMSAPIServiceInterface){

        //Fetch DLR on Teleoss
        $messages = MessageAPI::whereNotIn('dlrstatus', array('DELIVRD','UNDELIV'))
            //->where('created_at', '>=', Carbon::today()->subDays(2)->toDateTimeString())
            ->whereIn('messagetype', array('TELEOSS', 'DYNAMIC', 'ORANGE'))
            ->whereIn('network', array('MTNC', 'BANKAI'))
            ->latest()->get();


        if (!$messages->isEmpty()) {
            $this->info('['.date("Y-m-d H:i:s").'] Get DLRTeleoss : Started count = '. count($messages));
            foreach ($messages as $message) {
                $admin = User::find($message->user_id);
                //$this->info('['.date("Y-m-d H:i:s").'] MessageId = '. $message->smsclientid);
                $DLR = $SMSAPIServiceInterface->getDLR($admin->username, $admin->password, $message->smsclientid);
                $DLR_json = json_decode(json_encode($DLR, true));
                $this->info('[' . date("Y-m-d H:i:s") . '] DLR_json = ' . json_encode($DLR_json));

                if ($DLR_json->responsecode == 0) {
                    if (property_exists($DLR_json,'drlist')) {
                        if ($DLR_json->dlristcount = 1){
                            if (property_exists($DLR_json->drlist->dr, 'status')) {
                                if (($DLR_json->drlist->dr->status == 'DELIVRD') || ($DLR_json->drlist->dr->status == 'UNDELIV')) {
                                    $message->responsecode = ($DLR_json->drlist->dr->status == 'DELIVRD') ? 1 : 0;
                                    $message->responsedescription = ($message->responsecode == 1) ? $DLR_json->resposedescription : $DLR_json->drlist->dr->status;
                                    if (($message->dlrstatus == 'UNDELIV') && ($DLR_json->drlist->dr->status == 'DELIVRD')) {
                                        $message->dlrsent = 0;
                                    }
                                    $message->dlrstatus = $DLR_json->drlist->dr->status;
                                    $message->actualerrcode = $DLR_json->drlist->dr->actualerrcode;

                                    $message->submittime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($DLR_json->drlist->dr->submittime)));
                                    $message->senttime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($DLR_json->drlist->dr->senttime)));
                                    $message->deliverytime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($DLR_json->drlist->dr->deliverytime)));

                                    $message->undeliveredreason = $DLR_json->drlist->dr->undeliveredreason;
                                    $message->details = $DLR_json->drlist->dr->details;
                                    $message->save();
                                    if ($message->network == "MTNC") {
                                        $smsdlr = new SMSDLR([
                                            'requestId' => $message->smsclientid,
                                            'errorcode' => ($message->dlrstatus == "DELIVRD") ? "300" : "301",
                                            'provider' => "MTNC",
                                            'deliveryStatus' => $message->undeliveredreason,
                                            'mobileno' => "tel:" . $message->mobilno,
                                            'deliverytime' => $DLR_json->drlist->dr->deliverytime
                                        ]);
                                        $smsdlr->save();
                                    }

                                } else if ($DLR_json->drlist->dr->status == 'Unknown Status') {
                                    $message->responsecode = 0;
                                    $message->responsedescription = "Undelivered";
                                    $message->dlrstatus = 'UNDELIV';
                                    $message->actualerrcode = -1;

                                    $message->submittime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($DLR_json->drlist->dr->submittime)));
                                    $message->senttime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($DLR_json->drlist->dr->senttime)));
                                    $message->deliverytime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($DLR_json->drlist->dr->senttime)));
                                    $message->undeliveredreason = 'Fail';
                                    $message->details = 'Unknown Status';
                                    $message->save();
                                }

                            }
                        }else if($DLR_json->dlristcount > 1){
                            $dr = $DLR_json->drlist->dr;
                            for ($i=0;$i<count($dr);$i++){
                                $message = MessageAPI::where("smsclientid",$dr[$i]->messageid)->first();
                                if (($dr[$i]->details == 'DELIVRD') || ($dr[$i]->details == 'UNDELIV')) {
                                    $message->responsecode = ($dr[$i]->details == 'DELIVRD') ? 1 : 0;
                                    $message->responsedescription = ($message->responsecode == 1) ? "Success" : $dr[$i]->status;
                                    if (($message->dlrstatus == 'UNDELIV') && ($dr[$i]->details == 'DELIVRD')) {
                                        $message->dlrsent = 0;
                                    }
                                    $message->dlrstatus = $dr[$i]->details;
                                    $message->actualerrcode = $dr[$i]->actualerrcode;

                                    $message->submittime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($dr[$i]->submittime)));
                                    $message->senttime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($dr[$i]->senttime)));
                                    $message->deliverytime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($dr[$i]->deliverytime)));

                                    $message->undeliveredreason = $dr[$i]->undeliveredreason;
                                    $message->details = $dr[$i]->status;
                                    $message->save();
                                    if ($message->network == "MTNC") {
                                        $smsdr = SMSDLR::where("requestId",$message->smsclientid)->first();
                                        if ($smsdr == null) {
                                            $smsdlr = new SMSDLR([
                                                'requestId' => $message->smsclientid,
                                                'errorcode' => ($message->dlrstatus == "DELIVRD") ? "300" : "301",
                                                'provider' => "MTNC",
                                                'deliveryStatus' => $message->undeliveredreason,
                                                'mobileno' => "tel:" . $message->mobilno,
                                                'deliverytime' => $dr[$i]->deliverytime
                                            ]);
                                            $smsdlr->save();
                                        }
                                    }

                                } else if ($dr[$i]->status == 'Unknown Status') {
                                    $message->responsecode = 0;
                                    $message->responsedescription = "Undelivered";
                                    $message->dlrstatus = 'UNDELIV';
                                    $message->actualerrcode = -1;

                                    $message->submittime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($dr[$i]->submittime)));
                                    $message->senttime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($dr[$i]->senttime)));
                                    $message->deliverytime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($dr[$i]->senttime)));
                                    $message->undeliveredreason = 'Fail';
                                    $message->details = 'Unknown Status';
                                    $message->save();
                                }
                            }

                        }
                    }
                }

            }

            $this->info('['.date("Y-m-d H:i:s").'] Get DLRTeleoss : Ended');
        } else {
            $this->info('['.date("Y-m-d H:i:s").'] Get DLRTeleoss Failed with error =  No DLR to sent found');
        }

    }

    public function sendbachdlr($users){

        $this->info('['.date("Y-m-d H:i:s").'] usercount = ' .  sizeof($users));

        for ($i=0;$i<sizeof($users);$i++){
            //$user = User::where('username', $users[$i]['username'])->where('password', $users[$i]['password'])->first();
            $user = SubUser::where('username', $users[$i]['username'])->where('password', $users[$i]['password'])->first();
            //$this->info('['.date("Y-m-d H:i:s").'] user = '.$users[$i]['username'].' dlrlistcount  = ' .  sizeof($users[$i]['dlrlist'][0]));
            if ($user != null) {

                if ($user->batch_dlr_url != null) {
                    $dlrlist = '';
                    for ($j=0;$j<sizeof($users[$i]['dlrlist'][0]);$j++){
                        if ($j!=0)
                            $dlrlist = $dlrlist . ',';
                        $dlr = '{
                            "reponsecode": "'. $users[$i]['dlrlist'][0][$j]->responsecode.'",
                            "reponsedescription": "'. $users[$i]['dlrlist'][0][$j]->responsedescription.'",
                            "mobileno": "'. $users[$i]['dlrlist'][0][$j]->mobilno.'",
                            "messageid": "'. $users[$i]['dlrlist'][0][$j]->messageid.'",
                            "submittime": "'. $users[$i]['dlrlist'][0][$j]->submittime.'",
                            "senttime": "'. $users[$i]['dlrlist'][0][$j]->senttime.'",
                            "deliverytime": "'. $users[$i]['dlrlist'][0][$j]->deliverytime.'",
                            "status": "'. $users[$i]['dlrlist'][0][$j]->dlrstatus.'"
                        }';
                        $dlrlist = $dlrlist . $dlr;
                    }
                    $body = '{"dlrlist":['.$dlrlist.']}';

                    $ch = curl_init($user->batch_dlr_url);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_HTTPHEADER, [
                        'Accept:application/json',
                        'Content-Type:application/json']);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

                    //needed for https
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                    $response = curl_exec($ch);
                    if (curl_error($ch)) {
                        $this->info('['.date("Y-m-d H:i:s").'] DLR Sent Failed with error = ' . curl_error($ch) . ' & user = ' . $user->username);
                    }else {
                        $json = json_decode($response, true);
                        for ($l = 0; $l < sizeof($json['dlrlist']); $l++) {
                            $messageid = $json['dlrlist'][$l]['messageid'];
                            $message = MessageAPI::where('messageid','=',$messageid)->first();
                            $message->dlrsentstatus = "sent";
                            $message->dlrsent = $json['dlrlist'][$l]['status'];
                            $message->save();
                            $this->info('[' . date("Y-m-d H:i:s") . '] DLR Sent Success  response = ' . $message->dlrsent . ' & user = ' . $message->username . ' phone = ' . $message->mobilno . ' date = ' . $message->created_at);
                        }
                    }
                    curl_close($ch);
                }else{
                    for ($j=0;$j<sizeof($users[$i]['dlrlist'][0]);$j++){
                        $dlr = '{
                            "reponsecode": "'. $users[$i]['dlrlist'][0][$j]->responsecode.'",
                            "reponsedescription": "'. $users[$i]['dlrlist'][0][$j]->responsedescription.'",
                            "mobileno": "'. $users[$i]['dlrlist'][0][$j]->mobilno.'",
                            "messageid": "'. $users[$i]['dlrlist'][0][$j]->messageid.'",
                            "submittime": "'. $users[$i]['dlrlist'][0][$j]->submittime.'",
                            "senttime": "'. $users[$i]['dlrlist'][0][$j]->senttime.'",
                            "deliverytime": "'. $users[$i]['dlrlist'][0][$j]->deliverytime.'",
                            "status": "'. $users[$i]['dlrlist'][0][$j]->dlrstatus.'"
                        }';

                        $ch = curl_init($user->dlr_url);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                            'Accept:application/json',
                            'Content-Type:application/json']);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $dlr);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

                        //needed for https
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                        $response = curl_exec($ch);
                        $message = MessageAPI::where('messageid','=',$users[$i]['dlrlist'][0][$j]->messageid)->first();
                        $message->dlrsentstatus = "sent";
                        if (curl_error($ch)) {
                            $this->info('['.date("Y-m-d H:i:s").'] DLR Sent Failed with error = ' . curl_error($ch) . ' & user = ' . $message->username);
                        }
                        curl_close($ch);
                        $message->dlrsent = $response;
                        $message->save();
                        $this->info('['.date("Y-m-d H:i:s").'] DLR Sent Success  response = ' . $response . ' & user = ' . $message->username.' phone = ' . $message->mobilno.' date = ' . $message->id);
                    }
                }
            }
        }
    }

    /*public function updateDLR(){
        $messages =MessageAPI::whereIn('dlrstatus', array('Undelivered'))
            ->where('created_at', '>=', Carbon::today()->toDateTimeString())
            ->latest()->get();

        foreach ($messages as $message) {
            $time = new \DateTime($message->created_at);
            $diff = $time->diff(new \DateTime($message->deliverytime));
            $minutes = ($diff->days * 24 * 60) +
                ($diff->h * 60) + $diff->i;

            if ($minutes >= 15) {
                $message->responsecode = 1;
                $message->responsedescription = "Success";
                $message->dlrstatus = 'DELIVRD';
                $message->actualerrcode = -1;
                $message->undeliveredreason = 'Pending';
                $message->details = 'Undelivered';
                $message->dlrsent = 0;
                $message->save();
            }else{
                $message->responsecode = 0;
                $message->responsedescription = "Undelivered";
                $message->dlrstatus = 'Undelivered';
                $message->actualerrcode = -1;
                $message->undeliveredreason = 'Undelivered';
                $message->details = 'Undelivered';
                $message->dlrsent = 0;
                $message->save();
            }
        }
    }*/

    public function sendSMSMTN(){
        $msg = MessageAPI::where('created_at', '>=', Carbon::today()->toDateTimeString())
            ->where('username', 'skysoft')
            ->where('network', 'MTNC')
            ->where('undeliveredreason', 'Pending')
            ->latest()->get();

        $this->info('['.date("Y-m-d H:i:s").'] Resent SMS : Started count = '. count($msg));

        foreach ($msg as $message){
            $number = str_replace('+','',$message->mobilno);
            $sms_args = array(
                'phoneNumber' => (substr( $number, 0, 3 ) == '237')?$number:'237'.$number,
                'message' =>  $message->message ,
                'senderName' =>  $message->senderid
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://smsvas.com/operatorsmsapi/public/index.php/api/v1/sendocmsms');
            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sms_args, '', '&'));
            $response = curl_exec($ch);
            $result = json_decode($response, true);
            curl_close($ch);

            if ($result['code'] == "00") {
                $message->smsclientid = $result['messageId'];
                $message->dlrsent = 0;
                $message->save();
            }
            //$message->undeliveredreason = 'Pending1';
            //$message->save();
        }
    }

    public function resendUndelivMTNSMSWithMTN ($settings){
        $admin = User::find($settings ->admin_user_id);
        $msg = MessageAPI::where('created_at', '>=', Carbon::today()->toDateString())
            ->where('network', 'OCM')
            ->where('dlrstatus','=', 'UNDELIV')
            ->latest()->get();

        $this->info('['.date("Y-m-d H:i:s").'] MTN Resend Undeliv MTN SMS: Started count = '. count($msg));

        foreach ($msg as $message){
            //if ($message->dlrsentattempt < 5) {
            $number = str_replace('+', '', $message->mobilno);

            $client = new \GuzzleHttp\Client();
            $response = $client->post('https://smsvas.com/teleoss/sendsms.jsp', [
                'headers' => ['Accept' => 'application/xml'],
                'form_params' => [
                    'user' => $admin->username,
                    'password' => $admin->password,
                    'mobiles' => (substr($number, 0, 3) == '237') ? $number : '237' . $number,
                    'sms' => $message->message,
                    'senderid' => $message->senderid,
                    'isallowduplicatemobile' => 'no']
            ]);

            if ($response != null) {
                $xml = simplexml_load_string($response->getBody()->getContents());
                $sms = $xml->sms[0];
                if ($sms != null) {
                    $smsclientid = (string)intval($sms->{"messageid"});
                    $message->smsclientid = $smsclientid;
                    $message->responsecode = 2;
                    $message->responsedescription = "Waiting DLR";
                    $message->network = "MTNC";
                    $message->dlrsent = 0;
                    $message->dlrsentattempt = $message->dlrsentattempt + 1;
                    $message->save();
                }
            }
            //}
        }
    }

    public function resendUnknownDelivMTNSMSWithMTN (){
        $msg = MessageAPI::where('created_at', '>=', Carbon::today()->subDays(1)->toDateTimeString())
            ->where('network', 'OCM')
            ->where('responsecode','=', 2)
            ->whereNull('actualerrcode')
            ->latest()->get();

        $this->info('['.date("Y-m-d H:i:s").'] MTN Resend UnknownDeliv MTN SMS: Started count = '. count($msg));

        foreach ($msg as $message){
            $time = new \DateTime($message->created_at);
            $diff = $time->diff(new \DateTime());
            $minutes = ($diff->days * 24 * 60) +
                ($diff->h * 60) + $diff->i;

            if ($minutes >= 40) {

                $number = str_replace('+','',$message->mobilno);
                $sms_args = array(
                    'phoneNumber' => (substr( $number, 0, 3 ) == '237')?$number:'237'.$number,
                    'message' =>  $message->message ,
                    'senderName' =>  $message->senderid
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://smsvas.com/operatorsmsapi/public/index.php/api/v1/sendocmsms');
                curl_setopt($ch, CURLOPT_HTTPGET, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sms_args, '', '&'));
                $response = curl_exec($ch);
                $result = json_decode($response, true);
                curl_close($ch);

                if ($result['code'] == "00") {
                    $message->smsclientid = $result['messageId'];
                    $message->responsecode = 2;
                    $message->responsedescription = "Waiting DLR";
                    $message->network = "OCM";
                    $message->dlrsent = 0;
                    $message->save();
                }

            }

        }
    }


    public function resendUndelivMTNSMSWithOCM (){
        $msg = MessageAPI::where('created_at', '>=', Carbon::today()->toDateString())
            //->where('responsecode', '=', 2)
            //->where('dlrsentattempt', '>=', 5)
            ->where('network', 'MTNC')
            ->where('dlrstatus','=', 'UNDELIV')
            ->latest()->get();

        $this->info('['.date("Y-m-d H:i:s").'] OCM Resend Undeliv MTN SMS: Started count = '. count($msg));

        foreach ($msg as $message){
            $number = str_replace('+','',$message->mobilno);
            $sms_args = array(
                'phoneNumber' => (substr( $number, 0, 3 ) == '237')?$number:'237'.$number,
                'message' =>  $message->message ,
                'senderName' =>  $message->senderid
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://smsvas.com/operatorsmsapi/public/index.php/api/v1/sendocmsms');
            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sms_args, '', '&'));
            $response = curl_exec($ch);
            $result = json_decode($response, true);
            curl_close($ch);

            if ($result['code'] == "00") {
                $message->smsclientid = $result['messageId'];
                $message->responsecode = 2;
                $message->responsedescription = "Waiting DLR";
                $message->network = "OCM";
                $message->dlrsent = 0;
                $message->save();
            }
        }
    }

    public function resendUndelivOCMSMSWithOCM (){
        $msg = MessageAPI::where('created_at', '>=', Carbon::today()->toDateString())
            ->whereIn('messagetype', array('ORANGE'))
            ->where('responsecode', '=', 2)
            ->where('network', 'MTNC')
            ->where('dlrstatus','=', 'UNDELIV')
            ->where('senderid','=', 'UnitedXpres')
            ->latest()->get();

        $this->info('['.date("Y-m-d H:i:s").'] Resend Undeliv OCM SMS: Started count = '. count($msg));

        foreach ($msg as $message){
            $number = str_replace('+','',$message->mobilno);
            $sms_args = array(
                'phoneNumber' => (substr( $number, 0, 3 ) == '237')?$number:'237'.$number,
                'message' =>  $message->message ,
                'senderName' =>  $message->senderid
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://smsvas.com/operatorsmsapi/public/index.php/api/v1/sendocmsms');
            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sms_args, '', '&'));
            $response = curl_exec($ch);
            $result = json_decode($response, true);
            curl_close($ch);

            if ($result['code'] == "00") {
                $message->smsclientid = $result['messageId'];
                $message->responsecode = 2;
                $message->responsedescription = "Waiting DLR";
                $message->network = "OCM";
                $message->dlrsent = 0;
                $message->save();
            }
        }
    }

    public function resendUndelivOCMSMSWithOCM2 ($settings){
        //$admin = User::find($settings ->admin_user_id);
        $msg = MessageAPI::where('created_at', '>=', Carbon::today()->toDateString())
            ->where('network', 'OCM')
            ->where('dlrstatus','=', 'UNDELIV')
            //->where('details','=', 'DeliveryImpossible')
            //->where('username','=', 'stdadmin')
            ->latest()->get();

        $this->info('['.date("Y-m-d H:i:s").'] Resend Undeliv OCM SMS: Started count = '. count($msg));

        foreach ($msg as $message) {
            if ($message->dlrsentattempt < 1) {
                $number = str_replace('+', '', $message->mobilno);
                $sms_args = array(
                    'phoneNumber' => (substr($number, 0, 3) == '237') ? $number : '237' . $number,
                    'message' => $message->message,
                    'senderName' => $message->senderid
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://smsvas.com/operatorsmsapi/public/index.php/api/v1/sendocmsms');
                curl_setopt($ch, CURLOPT_HTTPGET, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sms_args, '', '&'));
                $response = curl_exec($ch);
                $result = json_decode($response, true);
                curl_close($ch);

                if ($result['code'] == "00") {
                    $message->smsclientid = $result['messageId'];
                    $message->responsecode = 2;
                    $message->responsedescription = "Waiting DLR";
                    $message->dlrsentattempt = $message->dlrsentattempt + 1;
                    $message->network = "OCM";
                    $message->dlrsent = 0;
                    $message->save();
                }
                /*$client = new \GuzzleHttp\Client();
                $response = $client->post('https://smsvas.com/teleoss/sendsms.jsp', [
                    'headers' => ['Accept' => 'application/xml'],
                    'form_params' => [
                        'user' => $admin->username,
                        'password' => $admin->password,
                        'mobiles' => (substr($number, 0, 3) == '237') ? $number : '237' . $number,
                        'sms' => $message->message,
                        'senderid' => $message->senderid,
                        'isallowduplicatemobile' => 'no']
                ]);

                if ($response != null) {
                    $xml = simplexml_load_string($response->getBody()->getContents());
                    $sms = $xml->sms[0];
                    if ($sms != null) {
                        $smsclientid = (string)intval($sms->{"messageid"});
                        $message->smsclientid = $smsclientid;
                        $message->responsecode = 2;
                        $message->responsedescription = "Waiting DLR";
                        $message->network = "MTNC";
                        $message->dlrsent = 0;
                        $message->dlrsentattempt = $message->dlrsentattempt + 1;
                        $message->save();
                    }
                }*/
            }
        }
    }

    /*public function ResendSMSMTN(){
        $msg = MessageAPI::where('created_at', '>=', Carbon::today()->toDateTimeString())
            ->where('username', 'skysoft')
            ->where('network', 'MTNC')
            ->where('responsecode','=', 2)
            ->latest()->get();

        $this->info('['.date("Y-m-d H:i:s").'] Resent SMS MTN: Started count = '. count($msg));

        foreach ($msg as $message){
            $time = new \DateTime($message->created_at);
            $diff = $time->diff(new \DateTime());
            $minutes = ($diff->days * 24 * 60) +
                ($diff->h * 60) + $diff->i;

            if ($minutes >= 60) {
                $number = str_replace('+', '', $message->mobilno);
                $sms_args = array(
                    'phoneNumber' => (substr($number, 0, 3) == '237') ? $number : '237' . $number,
                    'message' => $message->message,
                    'senderName' => $message->senderid
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://smsvas.com/operatorsmsapi/public/index.php/api/v1/sendocmsms');
                curl_setopt($ch, CURLOPT_HTTPGET, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sms_args, '', '&'));
                $response = curl_exec($ch);
                $result = json_decode($response, true);
                curl_close($ch);

                if ($result['code'] == "00") {
                    $message->smsclientid = $result['messageId'];
                    $message->network = 'OCM';
                    $message->save();
                }
            }
        }
    }*/




    /*public function sendSMSReport($settings, $SMSAPIServiceInterface){

        $day =  Carbon::today()->toDateString();
        $admin = SubUser::find($settings->admin_subuser_id);
        if ($settings->sms_report_date != $day){
            $this->info('['.date("Y-m-d H:i:s").'] Sending SMS report started ');

            $info = $this->report();
            $message = "DAILY SMS REPORT : " . Carbon::yesterday()->toDateString().
                "\nSent : ". $info['sent'] .
                "\nDelivered : " . $info['delivered'] . " -> " . $info['dlvr_rate'] .
                "\nUndelivered : ". $info['undelivered'] . " -> " . $info['undlvr_rate'].
                "\n\nActive Users : ". implode(',',$info['active_users']);
            $data = array(
                "message" => $message,
                "sent" => $info['sent'],
                "deliv" => $info['delivered'],
                "dlvr_rate" => $info['dlvr_rate'],
                "undeliv" => $info['undelivered'],
                "undlvr_rate" => $info['undlvr_rate'],
                "active_users" => $info['active_users']
            );

            Mail::to("eddy.tchousse@nexah.net")
                //->cc(array("joseph.njel@nexah.net","josias.wandji@nexah.net","ulrich.sinha@nexah.net"))
                ->sendNow(new NEXAHReports($data));

            $SMSAPIServiceInterface->sendsms($admin->username,$admin->password,$admin->phone,
                $message,$admin->senderid,null,null,null);
            $settings->sms_report_date = $day;
            $settings->save();
            $this->info('['.date("Y-m-d H:i:s").'] Sending SMS report ended ');
        }
    }*/


    /*public function report(){

        $from = Carbon::yesterday()->toDateString().' 00:00:00';
        $to = Carbon::yesterday()->toDateString().' 23:59:59';

        $smsarr = MessageAPI::whereBetween('created_at', [$from, $to])->get();
        $nbsent = count($smsarr);
        $this->info('count = ' . $nbsent);

        $info = array(
            'from' => $from,
            'to' => $to,
            'sent' => $nbsent,
            'delivered' => 0,
            'dlvr_rate' => "0%",
            'undelivered' => 0,
            'undlvr_rate' => "0%",
            'active_users' => array()
        );
        foreach ($smsarr as $sms){
            if ($sms->dlrstatus == 'DELIVRD'){
                $info['delivered'] =  $info['delivered'] + 1;
            }else if($sms->dlrstatus == 'UNDELIV'){
                $info['undelivered'] =  $info['undelivered'] + 1;
            }
            if (!in_array($sms->username, $info['active_users'])){
                array_push($info['active_users'], $sms->username);
            }
        }

        $info['dlvr_rate'] =  $nbsent==0?'0%':round(($info['delivered'] * 100)/ $nbsent, 2).'%';
        $info['undlvr_rate'] =  $nbsent==0?'0%':round(($info['undelivered'] * 100)/ $nbsent, 2).'%';
        return $info;

    }*/


    public function test(){
        //Rapport SMS de 3 mois dernier
        $firstDayofPreviousMonth = Carbon::now()->startOfMonth()->subMonths(3)->toDateTimeString();
        $lastDayofPreviousMonth = Carbon::now()->subMonth()->endOfMonth()->toDateString()." 23:59:59";
        $this->info("Rapport SMS du mois dernier\nFrom = ".$firstDayofPreviousMonth." To = ".$lastDayofPreviousMonth."\n");

        //Rapport SMS du mois dernier
        $firstDayofPreviousMonth = Carbon::now()->startOfMonth()->subMonth()->toDateTimeString();
        $lastDayofPreviousMonth = Carbon::now()->subMonth()->endOfMonth()->toDateString()." 23:59:59";
        $this->info("Rapport SMS du mois dernier\nFrom = ".$firstDayofPreviousMonth." To = ".$lastDayofPreviousMonth."\n");

        //Rapport SMS d'hier
        $dateFrom = Carbon::yesterday()->toDateTimeString();
        $dateTo =  Carbon::yesterday()->toDateString()." 23:59:59";
        $this->info("Rapport SMS d'hier\nFrom = ".$dateFrom." To = ".$dateTo."\n");

        //Rapport SMS de la semaine derniere
        $currentDate = Carbon::now();
        $firstDayofPreviousWeek = $currentDate->subDays($currentDate->dayOfWeek - 1)->subWeek()->toDateTimeString();
        $currentDate = Carbon::now();
        $lastDayofPreviousWeek = $currentDate->subDays($currentDate->dayOfWeek)->toDateString()." 23:59:59";
        $this->info("Rapport SMS de la semaine derniere\nFrom = ".$firstDayofPreviousWeek." To = ".$lastDayofPreviousWeek."\n");

    }

    public function isBlockedSender($sender){
        //$setting = Settings::find();
        $senders = "Orange,MTN,Camtel,nexttel,paul biya,RDPC,MRC,Kamto,ENEO,CDE,Jesus,DIEU,Allah,Jehovah,Diable,satan,demon,Esprit,Eglise,Mosquee,temple,Marrabout, Samuel etoo,MobileMoney,Orange Money,MTN Money";
        $blocked_senderid = explode(",", strtolower($senders));
        return in_array($sender,$blocked_senderid);
    }

    /*public function sendScheduledSMS(){
        $now =  date("Y-m-d H:i:s", strtotime('+1 hour', strtotime(Carbon::now()->toDateTimeString())));
        $msg = MessageAPI::where('scheduletime', '<=',$now)
            ->whereNull('schedulesenttime')
            ->where('responsecode','=', 3)
            ->latest()->get();

        $this->info('['.date("Y-m-d H:i:s").'] Send schedule SMS: Started count = '. count($msg));

        foreach ($msg as $message) {

            $user = SubUser::find($message->sub_user_id);
            //$admin = User::find($message->user_id);
            $sending_traffic_id = $user->sending_traffic_id;// !=0? $user->sending_traffic_id : $admin->sending_traffic_id;
            $sending_traffic = SendingTraffic::find($sending_traffic_id);
            $sendingservice = SendingService::find($sending_traffic->sending_service_id);
            $service_name = $sendingservice->name;

            $traffic_id = $sending_traffic->traffic_id;
            $traffic = Traffic::find($traffic_id);
            $number = str_replace('+', '', $message->mobilno);
            $core = new CoreSMSAPI();
            $core->sendRequest($message->id, $traffic,$message->username, $message->password, $number, $message->message, $message->senderid, $service_name, $message->stackid,$message->campaignid);

        }
    }*/


    /*public function createScheduleEventMessage($SMSAPIServiceInterface){
        $events = Event::where('status', true)
            ->where('is_deleted', false)
            ->latest()->get();
        $this->info('['.date("Y-m-d H:i:s").'] Schedule Event SMS: Started count = '. count($events));

        $todayDayOfWeek = Carbon::now()->day;
        $todayDayMonth = Carbon::now()->month;

        foreach ($events as $event) {
            $user = SubUser::find($event->user_id);
            $eventContacts = EventContact::where("event_id",$event->id)
                ->where("status",0)
                ->get();
            foreach ($eventContacts as $eventContact){
                $eventDate = $eventContact->date_event;
                $date = Carbon::createFromTimestamp(strtotime($eventDate));
                $eventDayOfWeek = $date->day;
                $eventMonth = $date->month;
                $week = $event->week;
                $hour = "10:00";
                $scheduletime = ($week == null)?($eventDate.$hour):$date->subWeeks($week)->toDateString().$hour;
                if (($eventDayOfWeek == $todayDayOfWeek) && ($eventMonth == $todayDayMonth)) {
                    $SMSAPIServiceInterface->sendsms($user->username, $user->password, $eventContact->mobileno,
                        $eventContact->message, $event->senderid, $scheduletime, $eventContact->id, $event->id);
                    $eventContact->status = 1;
                    $eventContact->save();
                }
            }
        }
    }*/


    /*public function sendEventMessage($SMSAPIServiceInterface){

        $events = Event::where('status', true)
            ->where('is_deleted', false)
            ->latest()->get();
        $this->info('['.date("Y-m-d H:i:s").'] Send Event SMS: Started count = '. count($events));

        $todayDayOfWeek = Carbon::now()->dayOfWeek;
        $todayTime = date('H:i:s', Carbon::now()->timestamp);
        $todayTime = date("H:i:s", strtotime('+1 hour', strtotime($todayTime)));
        $todayDate = Carbon::today()->toDateString();
        $this->info('['.date("Y-m-d H:i:s").'] Today Day hour = '. $todayTime);
        $this->info('['.date("Y-m-d H:i:s").'] Today Day ofWeek = '. $todayDayOfWeek);

        foreach ($events as $event) {
            $user = SubUser::find($event->user_id);
            $dayarr = explode(",",$event->day);
            $this->info('['.date("Y-m-d H:i:s").'] Days for event = '. $event->day);

            for ($i = 0; $i < sizeof($dayarr); $i++) {
                $this->info('['.date("Y-m-d H:i:s").'] Day for event = '. $dayarr[$i]);
                $this->info('['.date("Y-m-d H:i:s").'] is hour valid '. $todayTime >= $event->hour);

                if (($todayDayOfWeek == $dayarr[$i]) && ($todayTime >= $event->hour)) {
                    $this->info('['.date("Y-m-d H:i:s").'] Sending Event for day = '. $dayarr[$i]);

                    $dayEventStatus = $this->getEventStatusForToday($event, $dayarr[$i]);
                    if (sizeof($dayEventStatus) == 0){
                        $this->sendmessage($SMSAPIServiceInterface, $event, $user);
                        $this->updateEvent($event, $dayarr[$i], $todayDate);
                    }else{
                        if(!$this->isAlreadyExeToday($dayEventStatus, $todayDate)){
                            if ($event->is_repeat == 1) {
                                $this->sendmessage($SMSAPIServiceInterface, $event, $user);
                                $this->updateEvent($event, $dayarr[$i], $todayDate);
                            }
                        }
                    }
                }
            }
        }
    }

    public function getEventStatusForToday($event, $day){
        $result = array();
        if ($event->exe_status != null) {
            $status = $this->getExeStatus($event->exe_status);
            for ($i = 0; $i < sizeof($status); $i++) {
                if ($status[$i][0] == $day) {
                    return array_push($result, $status[$i]);
                }
            }
        }
        return $result;
    }

    public function isAlreadyExeToday($eventStatus, $todayDate){
        for ($i=0; $i<sizeof($eventStatus);$i++){
            if ($eventStatus[$i][1] == $todayDate){
                return true;
            }
        }
        return false;
    }

    public function updateEvent($event, $day, $todayDate){
        $before = $event->exe_status != null?$event->exe_status.';':'';
        $event->exe_status =  $before.$day.','.$todayDate;
        $event->save();
    }

    public function getExeStatus($exe){
        $exe_status = explode(';',$exe);
        $status = array();
        for($l = 0; $l < sizeof($exe_status); $l++){
            array_push($status, explode(',', $exe_status[$l]));
        }
        return $status;
    }

    public function sendmessage($SMSAPIServiceInterface, $event, $user){
        $eventContacts = EventContact::where('event_id',$event->id)->get();
        foreach ($eventContacts as $eventContact){
            $this->info('['.date("Y-m-d H:i:s").'] Send Message phone = ' . $eventContact->mobileno);
            $SMSAPIServiceInterface->sendsms($user->username, $user->password, $eventContact->mobileno,
                $eventContact->message, $event->senderid,null, null, null);
        }
    }*/

}

