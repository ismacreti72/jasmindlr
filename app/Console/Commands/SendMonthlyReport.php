<?php

namespace App\Console\Commands;

use App\Library\Services\Contracts\MailServiceInterface;
use App\Library\Services\Contracts\SMSAPIServiceInterface;
use App\Library\Services\CoreSMSAPI;
use App\Models\Sms\NexahEntreprise;
use App\Models\Sms\NexahUser;
use App\Models\Sms\Settings;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendMonthlyReport extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MonthlyReport:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Monthly SMS usage repport to administrator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(MailServiceInterface $mailService)
    {

        try {

            $this->info('['.date("Y-m-d H:i:s").'] Cron report started ');
            $this->MonthlyReport($mailService);
            $this->info('['.date("Y-m-d H:i:s").'] Cron report ended ');
        }catch (\Exception $e){
            $this->info('['.date("Y-m-d H:i:s").'] Cron report started  with error = ' . $e->getMessage());
        }

        return 'cron started';
    }

    public function MonthlyReport(MailServiceInterface $mailService){

        $today = getdate();
        //Number of day in month.
        $mon = $today["mon"];
        $year = $today["year"];

        $settings = Settings::find(1);
        $nb = $settings->nombre;
        $workingdays = $this->countDays($year, $mon, array(0, 5));
        $quota = ($workingdays * $nb)*100;

        $from = Carbon::now()->startOfMonth()->subMonth()->toDateTimeString();
        $fromDate = Carbon::now()->startOfMonth()->subMonth()->toDateString();
        $to = Carbon::now()->subMonth()->endOfMonth()->format('Y-m-d')." 23:59:59";
        $toDate = Carbon::now()->subMonth()->endOfMonth()->toDateString();

        $arrayuser =  NexahUser::all();
        $count = array();
        foreach ($arrayuser as $user) {
            if ($user->admin == 0) {
                $entreprise = NexahEntreprise::where('user_id', $user->id)->where('created_at', '>', $from)
                    ->where('created_at', '<', $to)->get();

                $percent = count($entreprise) / $quota;
                array_push($count, array("name" => $user->name, "entreprise" => count($entreprise), "percent" => round($percent, 2)));
            }
        }
        $data['title'] = 'Monthly Report Sales';
        $data['message'] = 'Report Monthly from '.$fromDate. " to ".$toDate;

        $mailService->sendReportMail($data,$count);
    }

    public function countDays($year, $month, $ignore) {
        $count = 0;
        $counter = mktime(0, 0, 0, $month, 1, $year);
        while (date("n", $counter) == $month) {
            if (in_array(date("w", $counter), $ignore) == false) {
                $count++;
            }
            $counter = strtotime("+1 day", $counter);
        }
        return $count;
    }

}

