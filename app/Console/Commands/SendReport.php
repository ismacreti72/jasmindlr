<?php

namespace App\Console\Commands;

use App\Library\Services\Contracts\SMSAPIServiceInterface;
use App\Models\ApiConfigs\Settings;
use App\Models\Sms\MessageAPI;
use App\Models\User\SubUser;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class SendReport extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Report:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily SMS usage repport to administrator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(SMSAPIServiceInterface $SMSAPIServiceInterface)
    {

        try {

            $this->info('['.date("Y-m-d H:i:s").'] Cron report started ');
            $settings = Settings::first();
            if ($settings->sendailyrepport == 1) {
                $this->sendSMSReport($settings,$SMSAPIServiceInterface);
            }else{
                $this->info('['.date("Y-m-d H:i:s").'] Cron report disabled ');
            }
            $this->info('['.date("Y-m-d H:i:s").'] Cron report ended ');
        }catch (\Exception $e){
            $this->info('['.date("Y-m-d H:i:s").'] Cron report started  with error = ' . $e->getMessage());
        }
        //Log::info("CRON REPORT FIRED");
        return 'Cron fired';
    }


    public function sendSMSReport($settings, SMSAPIServiceInterface $SMSAPIServiceInterface){

        $day =  Carbon::today()->toDateString();
        $admin = SubUser::find($settings->admin_subuser_id);
        if ($settings->sms_report_date != $day){
            $this->info('['.date("Y-m-d H:i:s").'] Sending SMS report started ');

            $info = $this->report();
            $info['title'] = $info['title'].' : '.Carbon::yesterday()->toDateString();
            $message = "DAILY SMS REPORT : " . Carbon::yesterday()->toDateString().
                "\nSent : ". $info['sent'] .
                "\nDelivered : " . $info['delivered'] . " -> " . $info['dlvr_rate'] .
                "\nUndelivered : ". $info['undelivered'] . " -> " . $info['undlvr_rate'].
                "\nPending : ". $info['pending'] . " -> " . $info['pending_rate'].
                "\n\nActive Users : ". implode(',',$info['active_users']);
            $SMSAPIServiceInterface->sendsms($admin->username,$admin->password,$admin->phone,
                $message,$admin->senderid,null,null,null);
            $this->send($info);
            $settings->sms_report_date = $day;
            $settings->save();
            $this->info('['.date("Y-m-d H:i:s").'] Sending SMS report ended ');
        }
    }


    public function report(){

        $from = Carbon::yesterday()->toDateString().' 00:00:00';
        $to = Carbon::yesterday()->toDateString().' 23:59:59';

        $smsarr = MessageAPI::whereBetween('created_at', [$from, $to])
            ->whereNotNull("messageid")
            ->where("mobilno","like",'+237%')
            ->get();
        $nbsent = count($smsarr);
        $this->info('count = ' . $nbsent);

        $info = array(
            'title' => "DAILY SMS REPORT",
            'content' => "Yesterday",
            'from' => $from,
            'to' => $to,
            'sent' => $nbsent,
            'delivered' => 0,
            'dlvr_rate' => "0%",
            'undelivered' => 0,
            'undlvr_rate' => "0%",
            'pending' => 0,
            'pending_rate' => "0%",
            'traffic' => $this->setTraffic($smsarr),
            'active_users' => array()
        );
        foreach ($smsarr as $sms){
            if ($sms->dlrstatus == 'DELIVRD'){
                $info['delivered'] =  $info['delivered'] + 1;
            }else if($sms->dlrstatus == 'UNDELIV'){
                $info['undelivered'] =  $info['undelivered'] + 1;
            }else{
                $info['pending'] =  $info['pending'] + 1;
            }
            if (!in_array($sms->username, $info['active_users'])){
                array_push($info['active_users'], $sms->username);
            }
        }

        $info['dlvr_rate'] =  $nbsent==0?'0%':round(($info['delivered'] * 100)/ $nbsent, 2).'%';
        $info['undlvr_rate'] =  $nbsent==0?'0%':round(($info['undelivered'] * 100)/ $nbsent, 2).'%';
        $info['pending_rate'] =  $nbsent==0?'0%':round(($info['pending'] * 100)/ $nbsent, 2).'%';
        return $info;

    }

    public function send($info){
        $client = new Client();
        $response = $client->post('https://sms.nexah.net/api/v1/report', [
            'headers' => ['Accept' => 'application/json'],
            'json' => $info
        ]);
        $result = $response->getBody()->getContents();
    }

    public function setTraffic($sms){

        $stat = array(
            "sent" => 0,
            "deliv" => 0,
            "undeliv" => 0,
            "delivRate" => "0%",
            "undelivRate" => "0%"
        );

        $sent = array();
        $deliv = array();
        $undeliv = array();
        $labels = ['0h', '1h', '2h', '3h', '4h', '5h', '6h', '7h','8h', '9h', '10h',
            '11h', '12h', '13h', '14h', '15h','16h', '17h', '18h', '19h', '20h',
            '21h','22h', '23h'];
        for($i=0; $i < sizeof($labels); $i++){
            $sent[$i] = 0;
            $deliv[$i] = 0;
            $undeliv[$i] = 0;
        }

        $stat["sent"] = sizeof($sms);
        foreach ($sms as $message){
            $date = date("H:i:s", strtotime($message->senttime));
            for ($i=0; $i < sizeof($labels); $i++){
                $dateFrom = $i<10?'0'.$i.":00:00":$i.":00:00";
                $dateTo = $i<10?'0'.$i.":59:59":$i.":59:59";
                if (($date >= $dateFrom) && ($date <= $dateTo)){
                    $sent[$i] = $sent[$i] + 1;
                    if ($message->dlrstatus == "DELIVRD"){
                        $stat["deliv"] = $stat["deliv"] + 1;
                        $deliv[$i] = $deliv[$i] + 1;
                    }else{
                        $stat["undeliv"] = $stat["undeliv"] + 1;
                        $undeliv[$i] = $undeliv[$i] + 1;
                    }
                }
            }

        }
        $stat['delivRate'] =   $stat["sent"]==0?'0%':round(($stat['deliv'] * 100)/  $stat["sent"], 2).'%';
        $stat['undelivRate'] =   $stat["sent"]==0?'0%':round(($stat['undeliv'] * 100)/  $stat["sent"], 2).'%';

        return array("sent" => $sent, "deliv" => $deliv, "undeliv" => $undeliv, "labels" => $labels, "stat" => $stat);
    }

}

#Christ is my reword and my devotion
