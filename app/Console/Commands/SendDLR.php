<?php

namespace App\Console\Commands;

use App\Library\Services\Contracts\MailServiceInterface;
use App\Library\Services\Contracts\SMSAPIServiceInterface;
use App\Mail\NEXAHReports;
use App\Models\ApiConfigs\Settings;
use App\Models\Sms\MessageAPI;
use App\Models\Sms\SMSDLR;
use App\Models\User\SubUser;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SendDLR extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DLR:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Delivery Receipt on registred Customer account URL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(SMSAPIServiceInterface $SMSAPIServiceInterface, MailServiceInterface $mailService)
    {

        try {

            //$this->reportTest();

            $this->info('['.date("Y-m-d H:i:s").'] Cron started ');
            $settings = Settings::first();
            if ($settings->dlrservicestatus == 1) {
                $this->getDLRTeleoss($SMSAPIServiceInterface);
                $this->sendAllDLR();
            }

        }catch (\Exception $e){
            $this->info('['.date("Y-m-d H:i:s").'] Cron Send DLR  with error = ' . $e->getMessage());
        }

        return 'Cron fired';
    }

    public function reportTest(){

        $data = array(
            'title' => "DAILY SMS REPORT: 2019-11-05",
            'file_url' => "/var/www/html/bulk/public/reports/Daily/Daily2019_11_05.png",
            'type' => "Daily",
            'file_name' => "2019_11_05",
            'email' => "eddy.tchousse@nexah.net"
        );

        Mail::to(explode(",",$data["email"]))
            ->sendNow(new NEXAHReports($data));

    }

    public function sendAllDLR(){

        /*$users =  DB::table('api_users')
            ->select('username')
            ->where('has_dlr_url','=',true)
            ->get();*/
        $users =  DB::table('api_sub_users')
            ->select('username')
            //->where('has_dlr_url','=',true)
            ->whereNotNull('batch_dlr_url')
            ->latest()
            ->get();
        $usernames = collect($users)->map(function($x){ return (array)$x; })->toArray();

        $messages = DB::table('api_messages')
            ->select((DB::raw('*')))
            ->whereIn('dlrstatus', array('DELIVRD','UNDELIV'))
            ->where('dlrsent','=', false)
            ->where('created_at', '>=', Carbon::today()->subDays(1)->toDateTimeString())
            ->whereIn('username', $usernames)
            ->latest()->get();

        $this->info('['.date("Y-m-d H:i:s").'] Sent All DLR : Started count = '. sizeof($messages));

        $users = array();
        $userlist = array();
        for ($i=0; $i<sizeof($messages); $i++){
            if (!in_array($messages[$i]->username,$userlist)) {
                array_push($userlist, $messages[$i]->username);
                //$this->info('[' . date("Y-m-d H:i:s") . '] user['.$i.'] =  ' . $messages[$i]->username);

                $dlrlist = array();
                $userdlr = array(
                    'username' => $messages[$i]->username,
                    'password' => $messages[$i]->password,
                    'dlrlist' => $dlrlist
                );
                for ($j=0; $j<sizeof($messages); $j++) {

                    if ($messages[$j]->username === $messages[$i]->username) {
                        //$this->info('[' . date("Y-m-d H:i:s") . '] username['.$j.']  =  ' . $messages[$j]->username);
                        array_push($dlrlist, $messages[$j]);
                    }
                }

                array_push($userdlr['dlrlist'], $dlrlist);
                array_push($users, $userdlr);

            }
        }

        $this->sendbachdlr($users);
        $this->info('['.date("Y-m-d H:i:s").'] Sent All DLR : Ended');
    }


    public function getDLRTeleoss(SMSAPIServiceInterface $SMSAPIServiceInterface){

        //Fetch DLR on Teleoss
        $messages = MessageAPI::whereNotIn('dlrstatus', array('DELIVRD','UNDELIV'))
            //->where('created_at', '>=', Carbon::today()->subDays(2)->toDateTimeString())
            ->whereIn('messagetype', array('TELEOSS', 'DYNAMIC', 'ORANGE'))
            ->whereIn('network', array('MTNC', 'BANKAI'))
            ->whereNotNull('smsclientid')
            ->latest()->get();


        if (!$messages->isEmpty()) {
            $this->info('['.date("Y-m-d H:i:s").'] Get DLRTeleoss : Started count = '. count($messages));
            foreach ($messages as $message) {
                $admin = User::find($message->user_id);
                $this->info('[' . date("Y-m-d H:i:s") . '] MessageId = ' . $message->smsclientid);
                if (strlen($message->smsclientid) > 5) {
                    $DLR = $SMSAPIServiceInterface->getDLR($admin->username, $admin->password, $message->smsclientid);
                    $DLR_json = json_decode(json_encode($DLR, true));
                    $this->info('[' . date("Y-m-d H:i:s") . '] DLR_json = ' . json_encode($DLR_json));

                    if ($DLR_json->responsecode == 0) {
                        if (property_exists($DLR_json, 'drlist')) {
                            if ($DLR_json->dlristcount = 1) {
                                if (property_exists($DLR_json->drlist->dr, 'status')) {
                                    if (($DLR_json->drlist->dr->status == 'DELIVRD') || ($DLR_json->drlist->dr->status == 'UNDELIV')) {
                                        $message->responsecode = ($DLR_json->drlist->dr->status == 'DELIVRD') ? 1 : 0;
                                        $message->responsedescription = ($message->responsecode == 1) ? $DLR_json->resposedescription : $DLR_json->drlist->dr->status;
                                        if (($message->dlrstatus == 'UNDELIV') && ($DLR_json->drlist->dr->status == 'DELIVRD')) {
                                            $message->dlrsent = 0;
                                        }
                                        $message->dlrstatus = $DLR_json->drlist->dr->status;
                                        $message->actualerrcode = $DLR_json->drlist->dr->actualerrcode;

                                        $message->submittime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($DLR_json->drlist->dr->submittime)));
                                        $message->senttime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($DLR_json->drlist->dr->senttime)));
                                        $message->deliverytime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($DLR_json->drlist->dr->deliverytime)));

                                        $message->undeliveredreason = $DLR_json->drlist->dr->undeliveredreason;
                                        $message->details = $DLR_json->drlist->dr->details;
                                        $message->save();
                                        if ($message->network == "MTNC") {
                                            $smsdlr = new SMSDLR([
                                                'requestId' => $message->smsclientid,
                                                'errorcode' => ($message->dlrstatus == "DELIVRD") ? "300" : "301",
                                                'provider' => "MTNC",
                                                'deliveryStatus' => $message->undeliveredreason,
                                                'mobileno' => "tel:" . $message->mobilno,
                                                'deliverytime' => $DLR_json->drlist->dr->deliverytime
                                            ]);
                                            $smsdlr->save();
                                        }

                                    } else if ($DLR_json->drlist->dr->status == 'Unknown Status') {
                                        $message->responsecode = 0;
                                        $message->responsedescription = "Undelivered";
                                        $message->dlrstatus = 'UNDELIV';
                                        $message->actualerrcode = -1;

                                        $message->submittime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($DLR_json->drlist->dr->submittime)));
                                        $message->senttime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($DLR_json->drlist->dr->senttime)));
                                        $message->deliverytime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($DLR_json->drlist->dr->senttime)));
                                        $message->undeliveredreason = 'Fail';
                                        $message->details = 'Unknown Status';
                                        $message->save();
                                    }

                                }
                            } else if ($DLR_json->dlristcount > 1) {
                                $dr = $DLR_json->drlist->dr;
                                for ($i = 0; $i < sizeof($dr); $i++) {
                                    $this->info('[' . date("Y-m-d H:i:s") . '] DR = ' . json_encode($dr[$i]));

                                    $message = MessageAPI::where("smsclientid", $dr[$i]->messageid)->first();
                                    if (($dr[$i]->details == 'DELIVRD') || ($dr[$i]->details == 'UNDELIV')) {
                                        $message->responsecode = ($dr[$i]->details == 'DELIVRD') ? 1 : 0;
                                        $message->responsedescription = ($message->responsecode == 1) ? "Success" : $dr[$i]->status;
                                        if (($message->dlrstatus == 'UNDELIV') && ($dr[$i]->details == 'DELIVRD')) {
                                            $message->dlrsent = 0;
                                        }
                                        $message->dlrstatus = $dr[$i]->details;
                                        $message->actualerrcode = $dr[$i]->actualerrcode;

                                        $message->submittime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($dr[$i]->submittime)));
                                        $message->senttime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($dr[$i]->senttime)));
                                        $message->deliverytime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($dr[$i]->deliverytime)));

                                        $message->undeliveredreason = $dr[$i]->undeliveredreason;
                                        $message->details = $dr[$i]->status;
                                        $message->save();
                                        if ($message->network == "MTNC") {
                                            $smsdr = SMSDLR::where("requestId", $message->smsclientid)->first();
                                            if ($smsdr == null) {
                                                $smsdlr = new SMSDLR([
                                                    'requestId' => $message->smsclientid,
                                                    'errorcode' => ($message->dlrstatus == "DELIVRD") ? "300" : "301",
                                                    'provider' => "MTNC",
                                                    'deliveryStatus' => $message->undeliveredreason,
                                                    'mobileno' => "tel:" . $message->mobilno,
                                                    'deliverytime' => $dr[$i]->deliverytime
                                                ]);
                                                $smsdlr->save();
                                            }
                                        }

                                    } else if ($dr[$i]->status == 'Unknown Status') {
                                        $message->responsecode = 0;
                                        $message->responsedescription = "Undelivered";
                                        $message->dlrstatus = 'UNDELIV';
                                        $message->actualerrcode = -1;

                                        $message->submittime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($dr[$i]->submittime)));
                                        $message->senttime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($dr[$i]->senttime)));
                                        $message->deliverytime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($dr[$i]->senttime)));
                                        $message->undeliveredreason = 'Fail';
                                        $message->details = 'Unknown Status';
                                        $message->save();
                                    }
                                }

                            }
                        }

                    }

                }
            }

            $this->info('['.date("Y-m-d H:i:s").'] Get DLRTeleoss : Ended');
        } else {
            $this->info('['.date("Y-m-d H:i:s").'] Get DLRTeleoss Failed with error =  No DLR to sent found');
        }

    }

    public function sendbachdlr($users){

        $this->info('['.date("Y-m-d H:i:s").'] usercount = ' .  sizeof($users));

        for ($i=0;$i<sizeof($users);$i++){
            //$user = User::where('username', $users[$i]['username'])->where('password', $users[$i]['password'])->first();
            $user = SubUser::where('username', $users[$i]['username'])->where('password', $users[$i]['password'])->first();
            //$this->info('['.date("Y-m-d H:i:s").'] user = '.$users[$i]['username'].' dlrlistcount  = ' .  sizeof($users[$i]['dlrlist'][0]));
            if ($user != null) {

                if ($user->batch_dlr_url != null) {
                    $dlrlist = '';
                    for ($j=0;$j<sizeof($users[$i]['dlrlist'][0]);$j++){
                        if ($j!=0)
                            $dlrlist = $dlrlist . ',';
                        $dlr = '{
                            "reponsecode": "'. $users[$i]['dlrlist'][0][$j]->responsecode.'",
                            "reponsedescription": "'. $users[$i]['dlrlist'][0][$j]->responsedescription.'",
                            "mobileno": "'. $users[$i]['dlrlist'][0][$j]->mobilno.'",
                            "messageid": "'. $users[$i]['dlrlist'][0][$j]->messageid.'",
                            "submittime": "'. $users[$i]['dlrlist'][0][$j]->submittime.'",
                            "senttime": "'. $users[$i]['dlrlist'][0][$j]->senttime.'",
                            "deliverytime": "'. $users[$i]['dlrlist'][0][$j]->deliverytime.'",
                            "status": "'. $users[$i]['dlrlist'][0][$j]->dlrstatus.'"
                        }';
                        $dlrlist = $dlrlist . $dlr;
                    }
                    $body = '{"dlrlist":['.$dlrlist.']}';

                    $ch = curl_init($user->batch_dlr_url);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_HTTPHEADER, [
                        'Accept:application/json',
                        'Content-Type:application/json']);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

                    //needed for https
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                    $response = curl_exec($ch);
                    if (curl_error($ch)) {
                        $this->info('['.date("Y-m-d H:i:s").'] DLR Sent Failed with error = ' . curl_error($ch) . ' & user = ' . $user->username);
                    }else {
                        $json = json_decode($response, true);
                        for ($l = 0; $l < sizeof($json['dlrlist']); $l++) {
                            $messageid = $json['dlrlist'][$l]['messageid'];
                            $message = MessageAPI::where('messageid','=',$messageid)->first();
                            $message->dlrsentstatus = "sent";
                            $message->dlrsent = $json['dlrlist'][$l]['status'];
                            $message->save();
                            $this->info('[' . date("Y-m-d H:i:s") . '] DLR Sent Success  response = ' . $message->dlrsent . ' & user = ' . $message->username . ' phone = ' . $message->mobilno . ' date = ' . $message->created_at);
                        }
                    }
                    curl_close($ch);
                }else{
                    for ($j=0;$j<sizeof($users[$i]['dlrlist'][0]);$j++){
                        $dlr = '{
                            "reponsecode": "'. $users[$i]['dlrlist'][0][$j]->responsecode.'",
                            "reponsedescription": "'. $users[$i]['dlrlist'][0][$j]->responsedescription.'",
                            "mobileno": "'. $users[$i]['dlrlist'][0][$j]->mobilno.'",
                            "messageid": "'. $users[$i]['dlrlist'][0][$j]->messageid.'",
                            "submittime": "'. $users[$i]['dlrlist'][0][$j]->submittime.'",
                            "senttime": "'. $users[$i]['dlrlist'][0][$j]->senttime.'",
                            "deliverytime": "'. $users[$i]['dlrlist'][0][$j]->deliverytime.'",
                            "status": "'. $users[$i]['dlrlist'][0][$j]->dlrstatus.'"
                        }';

                        $ch = curl_init($user->dlr_url);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                            'Accept:application/json',
                            'Content-Type:application/json']);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $dlr);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

                        //needed for https
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                        $response = curl_exec($ch);
                        $message = MessageAPI::where('messageid','=',$users[$i]['dlrlist'][0][$j]->messageid)->first();
                        $message->dlrsentstatus = "sent";
                        if (curl_error($ch)) {
                            $this->info('['.date("Y-m-d H:i:s").'] DLR Sent Failed with error = ' . curl_error($ch) . ' & user = ' . $message->username);
                        }
                        curl_close($ch);
                        $message->dlrsent = $response;
                        $message->save();
                        $this->info('['.date("Y-m-d H:i:s").'] DLR Sent Success  response = ' . $response . ' & user = ' . $message->username.' phone = ' . $message->mobilno.' date = ' . $message->id);
                    }
                }
            }
        }
    }


    public function isBlockedSender($sender){
        //$setting = Settings::find();
        $senders = "Orange,MTN,Camtel,nexttel,paul biya,RDPC,MRC,Kamto,ENEO,CDE,Jesus,DIEU,Allah,Jehovah,Diable,satan,demon,Esprit,Eglise,Mosquee,temple,Marrabout, Samuel etoo,MobileMoney,Orange Money,MTN Money";
        $blocked_senderid = explode(",", strtolower($senders));
        return in_array($sender,$blocked_senderid);
    }

}

