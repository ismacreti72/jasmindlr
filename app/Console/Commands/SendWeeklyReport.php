<?php

namespace App\Console\Commands;

use App\Library\Services\Contracts\MailServiceInterface;
use App\Library\Services\Contracts\SMSAPIServiceInterface;
use App\Library\Services\CoreSMSAPI;
use App\Models\Sms\NexahEntreprise;
use App\Models\Sms\NexahUser;
use App\Models\Sms\Settings;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendWeeklyReport extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'WeeklyReport:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send weekly SMS usage repport to administrator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(MailServiceInterface $mailService)
    {

        try {

            $this->info('['.date("Y-m-d H:i:s").'] Cron Weekly Report started ');
            $this->WeeklyReport($mailService);
            $this->info('['.date("Y-m-d H:i:s").'] Cron Weekly Report ended ');
        }catch (\Exception $e){
            Log::info('['.date("Y-m-d H:i:s").'] Cron Weekly Report started  with error = ' . $e->getMessage());
        }

        return false;
    }

    public function WeeklyReport(MailServiceInterface $mailService){

        $currentDate = Carbon::now();
        $from = $currentDate->subDays($currentDate->dayOfWeek - 1)->subWeek()->format('Y-m-d'). " 00:00:00";
        $currentDate = Carbon::now();
        $fromDate = $currentDate->subDays($currentDate->dayOfWeek - 1)->subWeek()->toDateString();
        $currentDate = Carbon::now();
        $to = $currentDate->subDays($currentDate->dayOfWeek)->format('Y-m-d'). " 23:59:59";
        $currentDate = Carbon::now();
        $toDate = $currentDate->subDays($currentDate->dayOfWeek)->toDateString();

        $settings = Settings::find(1);
        $nbsetting = $settings->semaine * $settings->nombre;
        $arrayuser =  NexahUser::all();
        $count = array();
        foreach ($arrayuser as $user) {
            if ($user->admin == 0) {
                $entreprise = NexahEntreprise::where('user_id', $user->id)->where('created_at', '>', $from)
                    ->where('created_at', '<', $to)->get();

                $percent = (count($entreprise) / $nbsetting) * 100;
                array_push($count, array("name" => $user->name, "entreprise" => count($entreprise), "percent" => round($percent, 2)));
            }
        }
        $data['title'] = 'Weekly Report Sales';
        $data['message'] = 'Report Weekly from '.$fromDate. " to ".$toDate;
        $mailService->sendReportMail($data,$count);
    }

}

