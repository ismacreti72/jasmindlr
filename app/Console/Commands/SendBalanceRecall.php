<?php

namespace App\Console\Commands;

use App\Library\Services\Contracts\SMSAPIServiceInterface;
use App\Models\ApiConfigs\Settings;
use App\Models\User\SubUser;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendBalanceRecall extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'BalanceRecall:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send balance recall to user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(SMSAPIServiceInterface $SMSAPIServiceInterface)
    {

        try {

            $this->info('['.date("Y-m-d H:i:s").'] Cron Balance Recall started ');
            $settings = Settings::first();
            $this->sendRecallSMS($settings, $SMSAPIServiceInterface);
            $this->info('['.date("Y-m-d H:i:s").'] Cron Balance Recall ended ');
        }catch (\Exception $e){
            Log::info('['.date("Y-m-d H:i:s").'] Cron Cron Balance Recall started  with error = ' . $e->getMessage());
        }

        return false;
    }


    public function sendRecallSMS($settings, SMSAPIServiceInterface $SMSAPIServiceInterface){
       $users = SubUser::where("is_admin",true)->get();
       foreach ($users as $user){
           if ($user->notif_report_enabled == 1) {
               if ($user->credit_rate == 100) {
                   //send SMS
                   $message = $settings->no_credit_message;
                   $number = str_replace('+', '', $user->phone);

                   $SMSAPIServiceInterface->sendsms('nexahusr', 'nex@hu5r', $number, $message,
                       'NEXAH', null, null, null);
               }
           }
       }
    }

}

