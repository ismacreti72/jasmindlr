<?php

namespace App\Console\Commands;

use App\Library\Services\Contracts\MailServiceInterface;
use App\Library\Services\Contracts\SMSAPIServiceInterface;
use App\Library\Services\CoreSMSAPI;
use App\Models\Sms\NexahEntreprise;
use App\Models\Sms\NexahUser;
use App\Models\Sms\Settings;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendDailyReport extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DailyReport:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily SMS usage repport to administrator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(MailServiceInterface $mailService)
    {

        try {

                $this->info('['.date("Y-m-d H:i:s").'] Cron daily report started ');
                $this->DailyReport($mailService);
                $this->info('['.date("Y-m-d H:i:s").'] Cron daily report ended ');
            }catch (\Exception $e){
                $this->info('['.date("Y-m-d H:i:s").'] Cron report daily started  with error = ' . $e->getMessage());
            }
            return 'Cron fired';
        }


    public function DailyReport(MailServiceInterface $mailService){

        $date = Carbon::yesterday()->format('Y-m-d');
        $from = $date.' 00:00:00';
        $to = $date.' 23:59:59';

        $data['message'] = 'Report Daily in '.$date;
        $setting = Settings::find(1);
        $arrayuser =  NexahUser::all();
        $count = array();
        foreach ($arrayuser as $user) {
            if ($user->admin == 0) {
                $entreprise = NexahEntreprise::where('user_id', $user->id)->where('created_at', '>', $from)
                    ->where('created_at', '<', $to)->get();

                $percent = (count($entreprise) / $setting->nombre) * 100;
                array_push($count, array("name" => $user->name, "entreprise" => count($entreprise), "percent" => round($percent, 2)));
            }
        }
        $data['title'] = 'Daily Report Sales';
        $mailService->sendReportMail($data,$count);
    }

}

