<?php

namespace App\Console\Commands;

use App\Library\Services\Contracts\SMSAPIServiceInterface;

use App\Models\Sms\NexahEntreprise;
use App\Models\Sms\NexahUser;
use App\Models\Sms\Settings;
use App\Models\Sms\SettingsCRM;
use App\Models\User\Roles;
use App\Models\User\UserRoles;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;


class SendCRMSMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CRMSMS:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(SMSAPIServiceInterface $SMSAPIService)
    {
        //info start and end cron
        try {

            $this->info('['.date("Y-m-d H:i:s").'] Cron crm sms started ');
            $this->DailySmsTwice($SMSAPIService,11);
            $this->DailySmsTwice($SMSAPIService,16);
            $this->info('['.date("Y-m-d H:i:s").'] Cron crm sms ended ');
        }catch (\Exception $e){
            $this->info('['.date("Y-m-d H:i:s").'] Cron report daily started  with error = ' . $e->getMessage());
        }
        return 'Cron fired';
    }

    public function DailySmsTwice(SMSAPIServiceInterface $SMSAPIService,$hour)
    {

        $date = Carbon::now()->startOfHour()->toDateTimeString();
        $dayOfWeek = date("l", strtotime($date));
        $settings = SettingsCRM::find(1);
        $dayOfWeekCrm = explode(",",$settings->dayOfWeek);
        $excepted = Carbon::now()->setTime($hour, 0, 0)->toDateTimeString();
        $settings = Settings::find(1);
        $nb = $settings->nombre;
        $switch = $settings->enable;
        $this->info('daily sms '.$date.' expected '.$excepted. ' day of week '.$dayOfWeek);
        if ($date == $excepted && $switch == 1) {
            if (!in_array($dayOfWeek,$dayOfWeekCrm)) {
                $currentDate1 = Carbon::now()->setTime(0, 0, 0)->toDateTimeString();
                $currenDate2 = Carbon::now()->setTime($hour, 0, 0)->toDateTimeString();
                $sales = UserRoles::where('role_id', 3)->get()->count();

                $users = NexahUser::where('admin', 0)->get();
                $allSchool = 0;
                $allAssurance = 0;
                $allOther = 0;
                $realHour = $hour + 1;
                foreach ($users as $user) {
                    $school = NexahEntreprise::where("categorie", "Education")->where("user_id", $user->id)->whereBetween('created_at', [$currentDate1, $currenDate2])->get()->count();
                    $assurance = NexahEntreprise::where("categorie", "Assurances")->where("user_id", $user->id)->whereBetween('created_at', [$currentDate1, $currenDate2])->get()->count();
                    $all = NexahEntreprise::where("user_id", $user->id)->whereBetween('created_at', [$currentDate1, $currenDate2])->get()->count();

                    $other = $all - ($school + $assurance);
                    $allSchool += $school;
                    $allAssurance += $assurance;
                    $allOther += $other;

                    $smsSales = "Hello " . $user->name
                        . ",\nVos prospections de la journée de " . $realHour . "h"
                        . "\nÉcoles: " . $school . "/2"
                        . "\nAssurances: " . $assurance . "/1"
                        . "\nAutres: " . $other . "/1"
                        . " \nTotal: " . $all . "/" . $nb;
                    $this->info('sms sales ' . $smsSales);
                    $data['user'] = 'crm@nexah.net';
                    $data['password'] = 'nex@hcrm';
                    $data['senderid'] = 'NEXAH CRM';
                    $data['sms'] = $smsSales;
                    $data['mobiles'] = $user->number;

                    $SMSAPIService->sendrealsms($data);
                }
                $total = $allSchool + $allAssurance + $allOther;

                $rolesUsers = UserRoles::where('role_id', 1)->orWhere('role_id', 2)->get();

                $nbschool = 2 * $sales;
                $nbassur = 1 * $sales;
                $nbother = 1 * $sales;
                $nball = $nbschool + $nbassur + $nbother;
                foreach ($rolesUsers as $admin) {

                    $smsAdmin = "Hello " . $admin->Users->name
                        . ",\nRecapitulatifs des prospections de " . $realHour . "h"
                        . "\nÉcoles: " . $allSchool . "/" . $nbschool
                        . "\nAssurances: " . $allAssurance . "/" . $nbassur
                        . "\nAutres: " . $allOther . "/" . $nbother
                        . " \nTotal: " . $total . "/" . $nball;


                    $this->info('sms admin ' . $smsAdmin);

                    $data['user'] = 'crm@nexah.net';
                    $data['password'] = 'nex@hcrm';
                    $data['senderid'] = 'NEXAH CRM';
                    $data['sms'] = $smsAdmin;
                    $data['mobiles'] = $admin->Users->number;

                    $SMSAPIService->sendrealsms($data);
                }
            }else{
                $this->info('dayOfWeek ' .$dayOfWeek );
            }

        }else{
            $this->info('daily sms invalid date expected or switch off');
        }
    }
}
