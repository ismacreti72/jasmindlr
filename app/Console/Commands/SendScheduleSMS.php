<?php

namespace App\Console\Commands;

use App\Library\Services\CoreSMSAPI;
use App\Library\Services\SMSAPIService;
use App\Models\ApiConfigs\SendingService;
use App\Models\ApiConfigs\SendingTraffic;
use App\Models\ApiConfigs\Traffic;
use App\Models\Sms\MessageAPI;
use App\Models\User\SubUser;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendScheduleSMS extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ScheduleSMS:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send scheduled message';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {

        try {
            $this->info('['.date("Y-m-d H:i:s").'] Cron scheduled message started ');
            $this->sendScheduledSMS();
            $this->info('['.date("Y-m-d H:i:s").'] Cron scheduled message ended ');
        }catch (\Exception $e){
            $this->info('['.date("Y-m-d H:i:s").'] Cron scheduled message started  with error = ' . $e->getMessage());
        }
        //Log::info("CRON SCHEDULE SMS FIRED");
        return 'Cron fired';
    }

    public function sendScheduledSMS(){

        $smsService = new SMSAPIService();
        $now =  date("Y-m-d H:i:s", strtotime('+1 hour', strtotime(Carbon::now()->toDateTimeString())));
        $msg = MessageAPI::where('scheduletime', '<=',$now)
            ->whereNull('schedulesenttime')
            ->where('responsecode','=', 3)
            ->latest()->get();

        $this->info('['.date("Y-m-d H:i:s").'] Send schedule SMS: Started count = '. count($msg));
        //Log::info('['.date("Y-m-d H:i:s").'] Send schedule SMS: Started count = '. count($msg));
        foreach ($msg as $message) {

            $user = SubUser::find($message->sub_user_id);
            $sending_traffic_id = $user->sending_traffic_id;
            $sending_traffic = SendingTraffic::find($sending_traffic_id);
            $sendingservice = SendingService::find($sending_traffic->sending_service_id);
            $service_name = $sendingservice->name;

            $traffic_id = $sending_traffic->traffic_id;
            $traffic = Traffic::find($traffic_id);
            $number = str_replace('+', '', $message->mobilno);
            $core = new CoreSMSAPI();

            $response = $core->sendRequest($message->id, $traffic->url != null ? $traffic : $core->checktraffic($number),
                $message->username,
                $message->password, $number, $message->message, $message->senderid,
                $service_name, $message->stackid,$message->campaignid);

            /*$response = $smsService->sendsms($message->username, $message->password, $number, $message->message,
                $message->senderid,null,$message->stackid,$message->campaignid);*/

            $this->info('['.date("Y-m-d H:i:s").'] Send schedule SMS: user = '. $message->username . ' response = '. json_encode($response));

            //Log::info('['.date("Y-m-d H:i:s").'] Send schedule SMS: user = '. $message->username . ' response = '. json_encode($response));

        }
    }

}

