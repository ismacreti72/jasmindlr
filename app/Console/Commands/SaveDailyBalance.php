<?php

namespace App\Console\Commands;

use App\Library\Services\Contracts\SMSAPIServiceInterface;
use App\Models\Sms\BalanceHistory;
use App\Models\Sms\CreditStoreHistory;
use App\Models\Sms\MessageAPI;
use App\Models\User\SubUser;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SaveDailyBalance extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DailyBalance:save';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save daily user balance ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(SMSAPIServiceInterface $SMSAPIServiceInterface)
    {

        try {
            $this->saveOCMBalance($SMSAPIServiceInterface);
            $this->saveUserBalance();
        }catch (\Exception $e){
            Log::info("cron daily balance fired with error = ".$e->getMessage());
        }
        //Log::info("CRON DAILY BALANCE FIRED");
        return 'Cron fired';
    }


    public function saveOCMBalance(SMSAPIServiceInterface $SMSAPIServiceInterface){

        $sms = MessageAPI::where('created_at','>=',Carbon::today()->toDateTimeString())->get();
        $todayMorningOcmBalance = json_decode($SMSAPIServiceInterface->getOcmBalance());
        //Log::info("OCM BALANCE = ".json_encode($todayMorningOcmBalance));
        $yesterdayEveningOcmBalance = $todayMorningOcmBalance->availableUnits + count($sms);
        $today = Carbon::today()->toDateString();
        $todayId = date("dmY",strtotime($today));
        $yesterday = Carbon::yesterday()->toDateString();
        $yesterdayId = date("dmY",strtotime($yesterday));

        $balanceHistoryYesterday = CreditStoreHistory::find($yesterdayId);
        if ($balanceHistoryYesterday != null) {
            $balanceHistoryYesterday->finalCredit = $yesterdayEveningOcmBalance;
            $balanceHistoryYesterday->save();
        }
        $creditStoreHistory = new CreditStoreHistory([
            "id" => $todayId,
            "day" => $today,
            "provider" => "OCM",
            "initialCredit" => $todayMorningOcmBalance->availableUnits
        ]);
        $creditStoreHistory->save();

    }

    public function saveUserBalance(){
        $users = SubUser::get();
        Log::info("users = ". count($users));

        foreach ($users as $user){
            $sms = MessageAPI::where('created_at','>=',Carbon::today()->toDateTimeString())
                ->where("username",$user->username)
                ->get();

            $todayMorningBalance = $user->credit;
            $yesterdayEveningBalance = $todayMorningBalance + count($sms);
            $today = Carbon::today()->toDateString();
            $todayId = date("dmY",strtotime($today));
            $yesterday = Carbon::yesterday()->toDateString();
            $yesterdayId = date("dmY",strtotime($yesterday));

            $balanceHistoryYesterday = BalanceHistory::find($yesterdayId.$user->id);
            if ($balanceHistoryYesterday != null) {
                $balanceHistoryYesterday->finalCredit = $yesterdayEveningBalance;
                $balanceHistoryYesterday->save();
            }

            $creditStoreHistory = new BalanceHistory([
                "id" => $todayId.$user->id,
                "user_id" => $user->id,
                "day" => $today,
                "initialCredit" => $todayMorningBalance,
            ]);
            $creditStoreHistory->save();
        }
    }
}

