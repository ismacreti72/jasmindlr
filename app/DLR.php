<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DLR extends Model
{
    protected $table = "d_l_r_s";

    protected $fillable = ["uid", "connector", "message_status", "done_date", "sub", "level", "message", "error", "id_smsc", "delivred", "submit_date"];
}
