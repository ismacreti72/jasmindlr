<?php


namespace App\Models\User;


use Illuminate\Database\Eloquent\Model;

class UserRoles extends Model
{

    //table
    protected $table = 'NexahUserRole';

    //fillable
    protected $fillable = ['role_id','user_id'];

    public function Roles(){
        return $this->belongsTo('App\Models\User\Roles','role_id');
    }

    public function Users(){
        return $this->belongsTo('App\Models\Sms\NexahUser','user_id');
    }
}
