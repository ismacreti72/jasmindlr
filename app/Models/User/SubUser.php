<?php
/**
 * Created by PhpStorm.
 * User: eddy
 * Date: 25/10/18
 * Time: 09:24
 */

namespace App\Models\User;
use Illuminate\Database\Eloquent\Model;


class SubUser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'api_sub_users';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'master_user_id','master_subuser_id','username', 'password',
        'is_admin', 'phone', 'credit', 'balance', 'credit_rate', 'senderid', 'company','company_sender',
        'notification_enable', 'notif_report_enabled', 'accountexpdate',
        'balanceexpdate','activated','has_dlr_url', 'dlr_url', 'batch_dlr_url', 'sending_traffic_id'];
}
