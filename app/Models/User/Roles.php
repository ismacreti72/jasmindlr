<?php


namespace App\Models\User;


use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{

    //table
    protected $table = 'Roles';

    //fillable
    protected $fillable = ['roles'];
}
