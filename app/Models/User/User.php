<?php
/**
 * Created by PhpStorm.
 * User: eddy
 * Date: 25/10/18
 * Time: 09:24
 */

namespace App\Models\User;
use Illuminate\Database\Eloquent\Model;


class User extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'api_users';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'username', 'password', 'phone', 'has_dlr_url', 'dlr_url', 'batch_dlr_url', 'sending_traffic_id', 'credit', 'company', 'accountexpdate', 'balanceexpdate'];
}
