<?php

namespace App\Providers;

use App\Library\Services\SMSAPIService;
use Illuminate\Support\ServiceProvider;


class SMSAPIServiceProvider extends ServiceProvider
{


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\Contracts\SMSAPIServiceInterface', function ($app) {
            return new SMSAPIService();
        });
    }

}
