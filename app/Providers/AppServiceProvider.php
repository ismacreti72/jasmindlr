<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->register(SMSAPIServiceProvider::class);
        $this->app->register(ContactAPIServiceProvider::class);
        $this->app->register(MailServiceProvider::class);
        $this->app->register(SSSMSAPIServiceProvider::class);
        $this->app->register(EventAPIServiceProvider::class);
        $this->app->register(SMSAccountAPIServiceProvider::class);


    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
