<?php

namespace App\Providers;

use App\Library\Services\EventAPIService;
use Illuminate\Support\ServiceProvider;


class EventAPIServiceProvider extends ServiceProvider
{


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\Contracts\EventAPIServiceInterface', function ($app) {
            return new EventAPIService();
        });
    }

}
