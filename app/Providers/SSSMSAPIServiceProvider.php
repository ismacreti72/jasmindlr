<?php

namespace App\Providers;

use App\Library\Services\SSSMSAPIService;
use Illuminate\Support\ServiceProvider;


class SSSMSAPIServiceProvider extends ServiceProvider
{


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\Contracts\SSSMSAPIServiceInterface', function ($app) {
            return new SSSMSAPIService();
        });
    }

}
