<?php


namespace App\Providers;


use App\Library\Services\SmsAccountAPIService;
use Illuminate\Support\ServiceProvider;

class SMSAccountAPIServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\Contracts\SmsAccountAPIServiceInterface', function ($app) {
            return new SmsAccountAPIService();
        });
    }
}
