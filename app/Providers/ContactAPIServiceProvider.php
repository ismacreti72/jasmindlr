<?php

namespace App\Providers;

use App\Library\Services\ContactAPIService;
use Illuminate\Support\ServiceProvider;


class ContactAPIServiceProvider extends ServiceProvider
{


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\Contracts\ContactAPIServiceInterface', function ($app) {
            return new ContactAPIService();
        });
    }

}
