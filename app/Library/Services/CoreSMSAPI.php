<?php
/**
 * Created by PhpStorm.
 * User: eddy
 * Date: 13/12/18
 * Time: 10:47
 */

namespace App\Library\Services;

use App\Library\Services\Contracts\SMSAPIServiceInterface;
use App\Mail\NEXAHAlert;
use App\Mail\NEXAHReports;
use App\Models\ApiConfigs\Providers;
use App\Models\ApiConfigs\Settings;
use App\Models\ApiConfigs\Traffic;
use App\Models\Sms\MessageAPI;
use App\Models\User\SubUser;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CoreSMSAPI
{


    public function checktraffic($phone){
        $phone = str_replace("+","",$phone);
        $traffics = Traffic::get();
        foreach ($traffics as $traffic){
            if ($traffic->phone_regex != null) {
                if (preg_match_all($traffic->phone_regex, $phone, $out)) {
                    return $traffic;
                }
            }
        }
        return null;
    }

    public function sendRequest($smsid, $traffic, $username, $password,
                                $mobil, $message, $sender, $sendingservice, $stackid, $campaignid){

        $sms_args = array(
            'user' => $username,
            'password' => $password,
            'mobileno' => $mobil,
            'smsid' => $smsid,
            'message' => $message,
            'stackid' => $stackid,
            'campaignid' => $campaignid,
            'senderid' => $sender,
            'sendingservice' => $sendingservice,
            'traffic_id' => $traffic->id,
            'traffic_id1_count' => $traffic->traffic_id1_count,
            'traffic_id2_count' => $traffic->traffic_id2_count,
            'traffic_name' => $traffic->name
        );
        Log::info('OCM API Agrs' . json_encode($sms_args));
        Log::info('URL ' . $traffic->url);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $traffic->url);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sms_args, '', '&'));
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;

    }

    public function isBlockedSender($sender){
        $setting = Settings::first();
        $blocked_senderid = explode(",", strtolower($setting->blocked_senderid));
        return in_array(strtolower($sender),$blocked_senderid);
    }

    public function teleossapi($user, $number, $message, $sender){
        try {
            //Log::info("Teleoss API Call param : user = " . $username . ' Phone = ' . $number);
            $from_params = [
                'user' => $user->username,
                'password' => $user->password,
                'mobiles' => $number,
                'sms' => $message,
                'senderid' => $sender,
                'isallowduplicatemobile' => 'no'
            ];
            if (strlen($message) != strlen(utf8_decode($message))) {
                $from_params['unicode'] = 1;
            }
            $client = new \GuzzleHttp\Client();
            $result = $client->post('https://smsvas.com/teleoss/sendsms.jsp', [
                'headers' => ['Accept' => 'application/xml'],
                'form_params' => $from_params
            ]);
            return $result;
        }catch (\Exception $e){
            return null;
        }
    }

    public function ocmapi($number, $message, $sender){

        $sms_args = array(
            'phoneNumber' => (substr( $number, 0, 3 ) == '237')?$number:'237'.$number,
            'message' => $message,
            'senderName' => $sender
        );
        //Log::info('OCM API Agrs' . json_encode($sms_args));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://smsvas.com/operatorsmsapi/public/index.php/api/v1/sendocmsms');
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sms_args, '', '&'));
        $response = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($response, true);
        return $result;
    }

    public function mtnapi($number, $message, $sender){

        $sms_args = array(
            'phoneNumber' => (substr( $number, 0, 3 ) == '237')?$number:'237'.$number,
            'message' => $message,
            'senderName' => $sender
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://smsvas.com/operatorsmsapi/public/index.php/api/v1/sendmtnsms');
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sms_args, '', '&'));
        $response = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($response, true);
        return $result;
    }

    public function teleoss_schedulesmsapi($user, $number, $message, $sender, $scheduletime){
        //Log::info("Teleoss API Call schedule param : user = " . $username .' Phone = ' . $number);
        $client = new \GuzzleHttp\Client();
        return  $client->post('https://smsvas.com/teleoss/sendsms.jsp', [
            'headers' => ['Accept' => 'application/xml'],
            'form_params' => [
                'user' => $user->username,
                'password' => $user->password,
                'mobiles' => $number,
                'sms' => $message,
                'senderid' => $sender,
                'scheduletime' => $scheduletime,
                'isallowduplicatemobile' => 'no']
        ]);
    }

    public function debitcredit($user, $message, $setting){
        $total_sms_unit = ceil(strlen($message)/$setting->sms_size);
        if ($user != null){
            $oldcredit = $user->credit;
            $user->credit = $oldcredit - $total_sms_unit;
            $user->save();
            return $total_sms_unit;
        }
        return 0;
    }

    public function checkRemainingBalance($user, $settings){

        if ($user->notif_report_enabled == 1) {
            $smsService = new SMSAPIService();
            $rates = explode(",", $settings->balance_rates);
            if ($user->balance > $user->credit) {
                $creditRate = (integer)round(((($user->balance - $user->credit) / $user->balance) * 100) / 10, PHP_ROUND_HALF_DOWN) * 10;
                if (in_array($creditRate, $rates)) {
                    if ($user->credit_rate != $creditRate) {
                        $RATE = $creditRate;
                        $CREDIT = $user->credit;
                        $user->credit_rate = $creditRate;
                        $user->save();

                        //send Message

                        /*
                        * Cher Client, vous  avez déjà consommé $RATE% de votre volume SMS. Votre solde est de $CREDIT SMS.
                        * Nous vous remercions pour votre confiance.
                        */

                        $message = str_replace(array('$RATE', '$CREDIT'), array($RATE, $CREDIT), ($RATE == 100) ? $settings->no_credit_message : $settings->rate_usage_message);
                        $number = str_replace('+', '', $user->phone);

                        $smsService->sendsms('nexahusr', 'nex@hu5r', $number, $message,
                            'NEXAH', null, null, null);

                        /*$this->sendRequest(null, $traffic,'nexahusr',
                            'nex@hu5r', $number, $message, 'NEXAH',
                            $service_name, null,null);*/
                    }
                }
            }
        }
    }


    public function getProvider($number){
        $providers = Providers::all();
        foreach ($providers as $provider){
            if (preg_match_all($provider->regex, $number, $out)){
                return $provider;
            }
        }
        return null;
    }

    public function signup($username, $password, $firstname, $lastname, $mobileno, $email, $address,
                           $ccname, $ccemail, $ccmobileno, $tcname, $tcemail, $tcmobileno, $city)
    {

        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://smsvas.com/teleoss/api/signupUser.jsp?', [
            'headers' => ['Accept' => 'application/xml'],
            'form_params' => [
                'apikey' => 'A18CA5DC0B329940',
                'user-id' => $username,
                'password' => $password,
                'password-hint' => $password,
                'first-name' => $firstname,
                'last-name' => $lastname,
                'mobile-number' => $mobileno,
                'email-id' => $email,
                'address' => $address,
                'cc-name' => $ccname,
                'cc-email-id' => $ccemail,
                'cc-mobile-number' => $ccmobileno,
                'tc-name' => $tcname,
                'tc-email-id' => $tcemail,
                'tc-mobile-number' => $tcmobileno,
                'city' => $city,
                'auto-credit' => 'yes']
        ]);
        $result = simplexml_load_string($response->getBody()->getContents());
        return $result;
    }

    public function smscredit($username, $password)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://smsvas.com/teleoss/smscredit.jsp', [
            'headers' => ['Accept' => 'application/xml'],
            'form_params' => [
                'user' => $username,
                'password' => $password]
        ]);
        $xml = simplexml_load_string($response->getBody()->getContents());
        return $xml;
    }

    public function getDLR($username, $password, $messageid)
    {
        $form_params = [
            'userid' => $username,
            'password' => $password,
            'messageid' => $messageid,
            'redownload'  => 'yes',
            'responcetype' => 'xml'];


        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://smsvas.com/teleoss/getDLR.jsp?', [
            'headers' => ['Accept' => 'application/xml'],
            'form_params' => $form_params
        ]);

        $result = simplexml_load_string($response->getBody()->getContents(),null,LIBXML_NOCDATA);
        return $result;
    }

    public function telerecharge($admin,$username,$validity,$credit){
        $client = new Client();
        $response = $client->post('https://smsvas.com/teleoss/api/recharge/recharge.jsp', [
            'headers' => ['Accept' => 'application/xml'],
            'form_params' => [
                'user' => $admin->username,
                'password' => $admin->password,
                'validitydays' => $validity,
                'balance' => $credit,
                'pintype' => 'both',
                'creditableuser' => $username,
                'action' => 'recharge',
                'schedule' => 'now']
        ]);
        $result = simplexml_load_string($response->getBody()->getContents());
        return $result;
    }

    public function setDailyTraffic($sms){

        $stat = array(
            "sent" => 0,
            "delivered" => 0,
            "undelivered" => 0,
            "pending" => 0,
            "mtn" => 0,
            "ocm" => 0,
            "bankai" => 0,
            "total_sms_unit" => 0,

            //MTN DETAILS
            "sentMTN" => 0,
            "deliveredMTN" => 0,
            "undeliveredMTN" => 0,
            "pendingMTN" => 0,

            //OCM DETAILS
            "sentOCM" => 0,
            "deliveredOCM" => 0,
            "undeliveredOCM" => 0,
            "pendingOCM" => 0,

            //BANKAI DETAILS
            "sentBANKAI" => 0,
            "deliveredBANKAI" => 0,
            "undeliveredBANKAI" => 0,
            "pendingBANKAI" => 0,

            //GENERAL RATE
            "delivRate" => "0%",
            "undelivRate" => "0%",
            "pendingRate" => "0%",

            //MTN
            "sentRateMTN" => "0%",
            "delivRateMTN" => "0%",
            "undelivRateMTN" => "0%",
            "pendingRateMTN" => "0%",

            //OCM RATE
            "sentRateOCM" => "0%",
            "delivRateOCM" => "0%",
            "undelivRateOCM" => "0%",
            "pendingRateOCM" => "0%",

            //BANKAI RATE
            "sentRateBANKAI" => "0%",
            "delivRateBANKAI" => "0%",
            "undelivRateBANKAI" => "0%",
            "pendingRateBANKAI" => "0%"
        );

        $sent = array();
        $deliv = array();
        $undeliv = array();
        $pending = array();

        $labels = ["0h", "1h", "2h", "3h", "4h", "5h", "6h", "7h","8h", "9h", "10h",
            "11h", "12h", "13h", "14h", "15h","16h", "17h", "18h", "19h", "20h",
            "21h","22h", "23h"];
        for($i=0; $i < sizeof($labels); $i++){
            $sent[$i] = 0;
            $deliv[$i] = 0;
            $undeliv[$i] = 0;
            $pending[$i] = 0;
        }

        $stat["sent"] = sizeof($sms);
        foreach ($sms as $message){
            $date = $this->hourFromUTCToLocal(date("H:i:s", strtotime($message->created_at)));
            if ($message->network == "MTNC"){
                $stat["sentMTN"] = $stat["sentMTN"] + 1;
            }elseif ($message->network == "OCM"){
                $stat["sentOCM"] = $stat["sentOCM"] + 1;
            }elseif ($message->network == "BANKAI"){
                $stat["sentBANKAI"] = $stat["sentBANKAI"] + 1;
            }
            for ($i=0; $i < sizeof($labels); $i++){
                $dateFrom = $i<10?'0'.$i.":00:00":$i.":00:00";
                $dateTo = $i<10?'0'.$i.":59:59":$i.":59:59";
                if (($date >= $dateFrom) && ($date <= $dateTo)){
                    $sent[$i] = $sent[$i] + 1;
                    $stat["total_sms_unit"] = $stat["total_sms_unit"] + $message->total_sms_unit;
                    if ($message->dlrstatus == "DELIVRD"){
                        $stat["delivered"] = $stat["delivered"] + 1;
                        $deliv[$i] = $deliv[$i] + 1;
                    }else if ($message->dlrstatus == "UNDELIV"){
                        $stat["undelivered"] = $stat["undelivered"] + 1;
                        $undeliv[$i] = $undeliv[$i] + 1;
                    }else{
                        $stat["pending"] = $stat["pending"] + 1;
                        $pending[$i] = $pending[$i] + 1;
                    }
                    if ($message->network == "MTNC"){
                        $stat["mtn"] = $stat["mtn"] + $message->total_sms_unit;
                    }elseif ($message->network == "OCM"){
                        $stat["ocm"] = $stat["ocm"] + $message->total_sms_unit;
                    }elseif ($message->network == "BANKAI"){
                        $stat["bankai"] = $stat["bankai"] + $message->total_sms_unit;
                    }

                    //Operator details
                    if ($message->dlrstatus == "DELIVRD" && $message->network == "MTNC"){
                        $stat["deliveredMTN"] = $stat["deliveredMTN"] + 1;
                    }else if ($message->dlrstatus == "UNDELIV" && $message->network == "MTNC"){
                        $stat["undeliveredMTN"] = $stat["undeliveredMTN"] + 1;
                    }else if ($message->network == "MTNC"){
                        $stat["pendingMTN"] = $stat["pendingMTN"] + 1;
                    }

                    if ($message->dlrstatus == "DELIVRD" && $message->network == "OCM"){
                        $stat["deliveredOCM"] = $stat["deliveredOCM"] + 1;
                    }else if ($message->dlrstatus == "UNDELIV" && $message->network == "OCM"){
                        $stat["undeliveredOCM"] = $stat["undeliveredOCM"] + 1;
                    }else if ($message->network == "OCM"){
                        $stat["pendingOCM"] = $stat["pendingOCM"] + 1;
                    }

                    if ($message->dlrstatus == "DELIVRD" && $message->network == "BANKAI"){
                        $stat["deliveredBANKAI"] = $stat["deliveredBANKAI"] + 1;
                    }else if ($message->dlrstatus == "UNDELIV" && $message->network == "BANKAI"){
                        $stat["undeliveredBANKAI"] = $stat["undeliveredBANKAI"] + 1;
                    }else if ($message->network == "BANKAI"){
                        $stat["pendingBANKAI"] = $stat["pendingBANKAI"] + 1;
                    }
                }
            }

        }

        //GENERAL RATE
        $stat['delivRate'] =   $stat["sent"]==0?'0%':round(($stat['delivered'] * 100)/  $stat["sent"], 2).'%';
        $stat['undelivRate'] =   $stat["sent"]==0?'0%':round(($stat['undelivered'] * 100)/  $stat["sent"], 2).'%';
        $stat['pendingRate'] =   $stat["sent"]==0?'0%':round(($stat['pending'] * 100)/  $stat["sent"], 2).'%';

        //MTN RATE
        $stat['sentRateMTN'] =   $stat["sent"]==0?'0%':round(($stat['sentMTN'] * 100)/  $stat["sent"], 2).'%';
        $stat['delivRateMTN'] =   $stat["sent"]==0?'0%':round(($stat['deliveredMTN'] * 100)/  $stat["sent"], 2).'%';
        $stat['undelivRateMTN'] =   $stat["sent"]==0?'0%':round(($stat['undeliveredMTN'] * 100)/  $stat["sent"], 2).'%';
        $stat['pendingRateMTN'] =   $stat["sent"]==0?'0%':round(($stat['pendingMTN'] * 100)/  $stat["sent"], 2).'%';

        //OCM RATE
        $stat['sentRateOCM'] =   $stat["sent"]==0?'0%':round(($stat['sentOCM'] * 100)/  $stat["sent"], 2).'%';
        $stat['delivRateOCM'] =   $stat["sent"]==0?'0%':round(($stat['deliveredOCM'] * 100)/  $stat["sent"], 2).'%';
        $stat['undelivRateOCM'] =   $stat["sent"]==0?'0%':round(($stat['undeliveredOCM'] * 100)/  $stat["sent"], 2).'%';
        $stat['pendingRateOCM'] =   $stat["sent"]==0?'0%':round(($stat['pendingOCM'] * 100)/  $stat["sent"], 2).'%';

        //BANKAI RATE
        $stat['sentRateBANKAI'] =   $stat["sent"]==0?'0%':round(($stat['sentBANKAI'] * 100)/  $stat["sent"], 2).'%';
        $stat['delivRateBANKAI'] =   $stat["sent"]==0?'0%':round(($stat['deliveredBANKAI'] * 100)/  $stat["sent"], 2).'%';
        $stat['undelivRateBANKAI'] =   $stat["sent"]==0?'0%':round(($stat['undeliveredBANKAI'] * 100)/  $stat["sent"], 2).'%';
        $stat['pendingRateBANKAI'] =   $stat["sent"]==0?'0%':round(($stat['pendingBANKAI'] * 100)/  $stat["sent"], 2).'%';

        return array("sent" => $sent, "delivered" => $deliv, "undelivered" => $undeliv, "pending" => $pending, "labels" => $labels, "stat" => $stat);
    }

    public function setTraffic($sms, $from, $to){

        $stat = array(
            "sent" => 0,
            "delivered" => 0,
            "undelivered" => 0,
            "pending" => 0,
            "mtn" => 0,
            "ocm" => 0,
            "bankai" => 0,
            "total_sms_unit" => 0,

            //MTN DETAILS
            "sentMTN" => 0,
            "deliveredMTN" => 0,
            "undeliveredMTN" => 0,
            "pendingMTN" => 0,

            //OCM DETAILS
            "sentOCM" => 0,
            "deliveredOCM" => 0,
            "undeliveredOCM" => 0,
            "pendingOCM" => 0,

            //BANKAI DETAILS
            "sentBANKAI" => 0,
            "deliveredBANKAI" => 0,
            "undeliveredBANKAI" => 0,
            "pendingBANKAI" => 0,

            //GENERAL RATE
            "delivRate" => "0%",
            "undelivRate" => "0%",
            "pendingRate" => "0%",

            //MTN
            "sentRateMTN" => "0%",
            "delivRateMTN" => "0%",
            "undelivRateMTN" => "0%",
            "pendingRateMTN" => "0%",

            //OCM RATE
            "sentRateOCM" => "0%",
            "delivRateOCM" => "0%",
            "undelivRateOCM" => "0%",
            "pendingRateOCM" => "0%",

            //BANKAI RATE
            "sentRateBANKAI" => "0%",
            "delivRateBANKAI" => "0%",
            "undelivRateBANKAI" => "0%",
            "pendingRateBANKAI" => "0%"
        );

        $sent = array();
        $deliv = array();
        $undeliv = array();
        $pending = array();

        $labels = $this->dateListBetweenTwoDates($from, $to);
        for($i=0; $i < sizeof($labels); $i++){
            $sent[$i] = 0;
            $deliv[$i] = 0;
            $undeliv[$i] = 0;
            $pending[$i] = 0;
        }
        $stat["sent"] = sizeof($sms);
        foreach ($sms as $message){
            if ($message->network == "MTNC"){
                $stat["sentMTN"] = $stat["sentMTN"] + 1;
            }elseif ($message->network == "OCM"){
                $stat["sentOCM"] = $stat["sentOCM"] + 1;
            }elseif ($message->network == "BANKAI"){
                $stat["sentBANKAI"] = $stat["sentBANKAI"] + 1;
            }
            $date = Carbon::createFromTimestamp(strtotime($this->dateFromUTCToLocal($message->created_at)));
            $day = $date->toDateString();
            for ($i=0; $i < sizeof($labels); $i++){
                if ($day == $labels[$i]){
                    $sent[$i] = $sent[$i] + 1;
                    $stat["total_sms_unit"] = $stat["total_sms_unit"] + $message->total_sms_unit;
                    if ($message->dlrstatus == "DELIVRD"){
                        $stat["delivered"] = $stat["delivered"] + 1;
                        $deliv[$i] = $deliv[$i] + 1;
                    }else if ($message->dlrstatus == "UNDELIV"){
                        $stat["undelivered"] = $stat["undelivered"] + 1;
                        $undeliv[$i] = $undeliv[$i] + 1;
                    }else{
                        $stat["pending"] = $stat["pending"] + 1;
                        $pending[$i] = $pending[$i] + 1;
                    }
                    if ($message->network == "MTNC"){
                        $stat["mtn"] = $stat["mtn"] + $message->total_sms_unit;
                    }elseif ($message->network == "OCM"){
                        $stat["ocm"] = $stat["ocm"] + $message->total_sms_unit;
                    }elseif ($message->network == "BANKAI"){
                        $stat["bankai"] = $stat["bankai"] + $message->total_sms_unit;
                    }

                    //Operator details
                    if ($message->dlrstatus == "DELIVRD" && $message->network == "MTNC"){
                        $stat["deliveredMTN"] = $stat["deliveredMTN"] + 1;
                    }else if ($message->dlrstatus == "UNDELIV" && $message->network == "MTNC"){
                        $stat["undeliveredMTN"] = $stat["undeliveredMTN"] + 1;
                    }else if ($message->network == "MTNC"){
                        $stat["pendingMTN"] = $stat["pendingMTN"] + 1;
                    }

                    if ($message->dlrstatus == "DELIVRD" && $message->network == "OCM"){
                        $stat["deliveredOCM"] = $stat["deliveredOCM"] + 1;
                    }else if ($message->dlrstatus == "UNDELIV" && $message->network == "OCM"){
                        $stat["undeliveredOCM"] = $stat["undeliveredOCM"] + 1;
                    }else if ($message->network == "OCM"){
                        $stat["pendingOCM"] = $stat["pendingOCM"] + 1;
                    }

                    if ($message->dlrstatus == "DELIVRD" && $message->network == "BANKAI"){
                        $stat["deliveredBANKAI"] = $stat["deliveredBANKAI"] + 1;
                    }else if ($message->dlrstatus == "UNDELIV" && $message->network == "BANKAI"){
                        $stat["undeliveredBANKAI"] = $stat["undeliveredBANKAI"] + 1;
                    }else if ($message->network == "BANKAI"){
                        $stat["pendingBANKAI"] = $stat["pendingBANKAI"] + 1;
                    }
                }
            }
        }

        //GENERAL RATES
        $stat['delivRate'] =   $stat["sent"]==0?'0%':round(($stat['delivered'] * 100)/  $stat["sent"], 2).'%';
        $stat['undelivRate'] =   $stat["sent"]==0?'0%':round(($stat['undelivered'] * 100)/  $stat["sent"], 2).'%';
        $stat['pendingRate'] =   $stat["sent"]==0?'0%':round(($stat['pending'] * 100)/  $stat["sent"], 2).'%';

        //MTN RATE
        $stat['sentRateMTN'] =   $stat["sent"]==0?'0%':round(($stat['sentMTN'] * 100)/  $stat["sent"], 2).'%';
        $stat['delivRateMTN'] =   $stat["sent"]==0?'0%':round(($stat['deliveredMTN'] * 100)/  $stat["sent"], 2).'%';
        $stat['undelivRateMTN'] =   $stat["sent"]==0?'0%':round(($stat['undeliveredMTN'] * 100)/  $stat["sent"], 2).'%';
        $stat['pendingRateMTN'] =   $stat["sent"]==0?'0%':round(($stat['pendingMTN'] * 100)/  $stat["sent"], 2).'%';

        //OCM RATE
        $stat['sentRateOCM'] =   $stat["sent"]==0?'0%':round(($stat['sentOCM'] * 100)/  $stat["sent"], 2).'%';
        $stat['delivRateOCM'] =   $stat["sent"]==0?'0%':round(($stat['deliveredOCM'] * 100)/  $stat["sent"], 2).'%';
        $stat['undelivRateOCM'] =   $stat["sent"]==0?'0%':round(($stat['undeliveredOCM'] * 100)/  $stat["sent"], 2).'%';
        $stat['pendingRateOCM'] =   $stat["sent"]==0?'0%':round(($stat['pendingOCM'] * 100)/  $stat["sent"], 2).'%';

        //BANKAI RATE
        $stat['sentRateBANKAI'] =   $stat["sent"]==0?'0%':round(($stat['sentBANKAI'] * 100)/  $stat["sent"], 2).'%';
        $stat['delivRateBANKAI'] =   $stat["sent"]==0?'0%':round(($stat['deliveredBANKAI'] * 100)/  $stat["sent"], 2).'%';
        $stat['undelivRateBANKAI'] =   $stat["sent"]==0?'0%':round(($stat['undeliveredBANKAI'] * 100)/  $stat["sent"], 2).'%';
        $stat['pendingRateBANKAI'] =   $stat["sent"]==0?'0%':round(($stat['pendingBANKAI'] * 100)/  $stat["sent"], 2).'%';

        return array("sent" => $sent, "delivered" => $deliv, "undelivered" => $undeliv, "pending" => $pending, "labels" => $labels, "stat" => $stat);
    }

    public function dateListBetweenTwoDates($from, $to){
        $period = CarbonPeriod::create($from, $to);
        $dates = array();
        foreach ($period as $date) {
            array_push($dates, $date->format('Y-m-d'));
        }
        return $dates;
    }

    public function getUserStatus2(){

        $settings = Settings::first();
        $active_users = array();
        $dormant_users = array();
        $inactive_users = array();
        $total_users = array();

        $status = array(
            'active_users' => array(),
            'dormant_users' => array(),
            'inactive_users' => array(),
            'total_users' => array()
        );

        $users =  DB::table('api_sub_users')
            ->select('username')
            ->get();
        $usernames = collect($users)->map(function($x){ return (array)$x; })->toArray();

        for ($i=0; $i < sizeof($usernames); $i++){
            if (!in_array($usernames[$i], $total_users)){
                $user = array("username" => $usernames[$i]["username"], "nb7DaysSMS" => 0);
                array_push($total_users, $user);
            }
        }

        for($i=0; $i<sizeof($total_users); $i++){
            $date = Carbon::today()->subDays($settings->day_sms_user_status)->toDateString();
            $messages = MessageAPI::where("username",$total_users[$i]['username'])
                ->orWhere("username",$total_users[$i]['username'])
                ->where('created_at', '>=', $date)->get();
            $total_users[$i]['nb7DaysSMS'] = count($messages);
            if ($total_users[$i]['nb7DaysSMS'] > $settings->max_sms_user_status){
                array_push($active_users, $total_users[$i]);
            }elseif (($total_users[$i]['nb7DaysSMS'] >= $settings->min_sms_user_status) &&
                ($total_users[$i]['nb7DaysSMS'] <= $settings->max_sms_user_status)){
                array_push($dormant_users, $total_users[$i]);
            }else{
                array_push($inactive_users, $total_users[$i]);
            }
        }


        $status['total_users'] =  $total_users;
        $status['active_users'] =  $active_users;
        $status['dormant_users'] =  $dormant_users;
        $status['inactive_users'] =  $inactive_users;

        return $status;
    }

    public function getUserStatus(){

        $settings = Settings::first();
        $active_users = array();
        $dormant_users = array();
        $inactive_users = array();
        $total_users = array();

        $status = array(
            'active_users' => array(),
            'dormant_users' => array(),
            'inactive_users' => array(),
            'total_users' => array()
        );


        $users = SubUser::where("is_admin", 1)->get();
        foreach ($users as $user){
            $user = array("username" => $user->username, "userid" => $user->id, "nb7DaysSMS" => 0);
            array_push($total_users, $user);
        }

        for($i=0; $i<sizeof($total_users); $i++){
            $date = Carbon::today()->subDays($settings->day_sms_user_status + 1)->toDateString(). ' 23:00:00';
            $to = Carbon::today()->toDateString().' 22:59:59';
            $messages = MessageAPI::where("master_subuser_id",$total_users[$i]['userid'])
                ->whereNotNull("messageid")
                ->whereBetween('created_at', [$date,$to])
                ->orWhere("sub_user_id",$total_users[$i]['userid'])
                ->whereNotNull("messageid")
                ->whereBetween('created_at', [$date,$to])
                ->get();
            $total_users[$i]['nb7DaysSMS'] = count($messages);
            if ($total_users[$i]['nb7DaysSMS'] > $settings->max_sms_user_status){
                array_push($active_users, $total_users[$i]);
            }elseif (($total_users[$i]['nb7DaysSMS'] >= $settings->min_sms_user_status) &&
                ($total_users[$i]['nb7DaysSMS'] <= $settings->max_sms_user_status)){
                array_push($dormant_users, $total_users[$i]);
            }else{
                array_push($inactive_users, $total_users[$i]);
            }
        }


        $status['total_users'] =  $total_users;
        $status['active_users'] =  $active_users;
        $status['dormant_users'] =  $dormant_users;
        $status['inactive_users'] =  $inactive_users;

        return $status;
    }

    public function generate_report($info){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://smsvas.com/bulk/public/index.php/api/v1/generate_report');
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($info, '', '&'));
        $response = curl_exec($ch);
        $result = json_decode($response, true);
        curl_close($ch);
        return $result;
    }


    public function sendReport($report, $settings, SMSAPIServiceInterface $SMSAPIServiceInterface){
        $admin = SubUser::find($settings->admin_subuser_id);
        $active_users = ($report['active_users'] != null)?count(explode(';',$report['active_users'])):0;
        $dormant_users = ($report['dormant_users'] != null)?count(explode(';',$report['dormant_users'])):0;
        $inactive_users =  ($report['inactive_users'] != null)?count(explode(';',$report['inactive_users'])):0;
        $total_users = ($report['total_users'] != null)?count(explode(';',$report['total_users'])):0;

        $message = $report['title'].
            "\n\nCrédit Consommé ".
            "\nMTN : ". $report['mtn'].
            "\nOCM : ".$report['ocm'].
            "\nBANKAI : ".$report['bankai'].
            "\nCrédit Total : ".$report['total_sms_unit'].
            "\n\nSMS Envoyé ".
            "\nMTN : " . $report['sent_mtn'] . " -> " . $report['sent_rate_mtn'] .
            "\nOCM : " . $report['sent_ocm'] . " -> " . $report['sent_rate_ocm'] .
            "\nBANKAI : " . $report['sent_bankai'] . " -> " . $report['sent_rate_bankai'] .
            "\nTotal : " . $report['sent']  . " -> 100%" .
            "\n\nSMS Délivré ".
            "\nMTN : " . $report['delivered_mtn'] . " -> " . $report['dlvr_rate_mtn'] .
            "\nOCM : " . $report['delivered_ocm'] . " -> " . $report['dlvr_rate_ocm'] .
            "\nBANKAI : " . $report['delivered_bankai'] . " -> " . $report['dlvr_rate_bankai'] .
            "\nTotal : " . $report['delivered']  . " -> " . $report['dlvr_rate'] .
            "\n\nSMS Non Délivré : ".
            "\nMTN : " . $report['undelivered_mtn'] . " -> " . $report['undlvr_rate_mtn'] .
            "\nOCM : " . $report['undelivered_ocm'] . " -> " . $report['undlvr_rate_ocm'] .
            "\nBANKAI : " . $report['undelivered_bankai'] . " -> " . $report['undlvr_rate_bankai'] .
            "\nTotal : " . $report['undelivered']  . " -> " . $report['undlvr_rate'].
            "\n\nSMS En cours : ".
            "\nMTN : " . $report['pending_mtn'] . " -> " . $report['pending_rate_mtn'] .
            "\nOCM : " . $report['pending_ocm'] . " -> " . $report['pending_rate_ocm'] .
            "\nBANKAI : " . $report['pending_bankai'] . " -> " . $report['pending_rate_bankai'] .
            "\nTotal : " . $report['pending'] . " -> " . $report['pending_rate'].
            "\n\nSolde Actuel OCM : ". $report['current_ocm_balance'].
            "\nSolde Actuel MTN : ---".
            "\nSolde Actuel BANKAI : ---".
            "\n\nUtilisateur Actif : ". $active_users.
            "\nUtilisateur Dormant : ". $dormant_users.
            "\nUtilisateur Inactif: ".$inactive_users.
            "\nTotal Utilisateur : ". $total_users;

        $SMSAPIServiceInterface->sendsms($admin->username,$admin->password,$settings->sms_report_phone,
            $message,$admin->senderid,null,null,null);

        $data = [
            'title' => $report['title'],
            'type' => $report['type'],
            'file_url' => $report['file_url'],
            'email' => $settings->sms_report_email
        ];
        $this->sendMailReport($data);
    }

    public function sendMailReport($data){
        Mail::to(explode(",",$data["email"]))
            ->sendNow(new NEXAHReports($data));
    }

    public function sendMailAlert($data){
        Mail::to(explode(",",$data["email"]))
            ->sendNow(new NEXAHAlert($data));
    }

    public function dateToUTC($date){
        return date("Y-m-d H:i:s", strtotime('-1 hour', strtotime($date)));
    }
    public function dateFromUTCToLocal($date){
        return date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($date)));
    }
    public function hourFromUTCToLocal($hour){
        return date("H:i:s", strtotime('+1 hour', strtotime($hour)));
    }

    public function checkIfExist($stackid, $campaignid){

        if ($stackid != null && $campaignid != null){
            $uuid_sms_id = "nxh"."-".$stackid.'-'.$campaignid;
            $message = MessageAPI::where("uuid_sms_id",$uuid_sms_id)->first();
            if ($message != null){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}
