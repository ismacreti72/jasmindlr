<?php
/**
 * Created by PhpStorm.
 * User: eddy
 * Date: 13/12/18
 * Time: 10:47
 */

namespace App\Library\Services;

use App\Library\Services\Contracts\SSSMSAPIServiceInterface;
use App\Models\ApiConfigs\Settings;
use App\Models\ApiConfigs\Traffic;
use App\Models\Sms\MessageAPI;
use App\Models\User\SubUser;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class SSSMSAPIService extends CoreSMSAPI implements SSSMSAPIServiceInterface
{

    public function sendsmsteleoss($smsid, $username, $password, $mobil, $message, $sender,
                                   $sendingservice, $traffic_id1_count, $traffic_id2_count,
                                   $traffic_name, $traffic_id, $stackid, $campaignid){

        $setting = Settings::first();
        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        $traffic = Traffic::find($traffic_id);
        $admin = User::find($traffic->account_id);

            try {
                $smssave = ($smsid == null)?$this->saveMessage($admin,$user,$username, $password,null,
                    null, $mobil, $sender, $message, $sendingservice, $traffic_name,
                    0, $stackid, $campaignid):MessageAPI::find($smsid);

                $smselt = $this->teleossRequest($admin, $mobil, $message, $sender, $smsid, $user, $setting, $smssave, $sendingservice,
                    $traffic_name, $traffic_id1_count, $traffic_id2_count);

            } catch (\Exception $exception) {
                if ($stackid != null && $campaignid == null){
                    $sms = MessageAPI::where("uuid_sms_id", $stackid)->first();
                    if ($sms != null){
                        $smselt['status'] = 'success';
                        $smselt['smsclientid'] = $sms->smsclientid;
                        $smselt['messageid'] = $sms->messageid;
                        $smselt['mobileno'] = $sms->mobilno;
                        $smselt['errorcode'] = "NXH202";
                        $smselt['errordescription'] = "Message already exist";
                    }else{
                        $smselt['status'] = 'error';
                        $smselt['smsclientid'] = null;
                        $smselt['messageid'] = null;
                        $smselt['mobileno'] = $this->getNumber($mobil);
                        $smselt['errorcode'] = "NXH500";
                        $smselt['errordescription'] = "Internal error";
                    }
                }else{
                    $smselt['status'] = 'error';
                    $smselt['smsclientid'] = null;
                    $smselt['messageid'] = null;
                    $smselt['mobileno'] = $this->getNumber($mobil);
                    $smselt['errorcode'] = "NXH500";
                    $smselt['errordescription'] = "Internal error";
                }
            }
        return $smselt;

    }


    public function teleossRequest($admin, $mobil, $message, $sender, $smsid, $user, $setting, $smssave, $sendingservice,
                                   $traffic_name, $traffic_id1_count, $traffic_id2_count){

        $response = $this->teleossapi($admin, $mobil, $message, $sender);
        if ($response != null) {
            $xml = simplexml_load_string($response->getBody()->getContents());
            $sms = $xml->sms[0];
            if ($sms != null) {

                //Success
                $smselt['status'] = 'success';
                $smsclientid = (string)intval($sms->{"messageid"});
                $smselt['smsclientid'] = (string)intval($sms->{"messageid"});
                $messageid = md5(intval($sms->{"messageid"}));
                $smselt['messageid'] = $messageid;
                $mobilno = (string)$sms->{"mobile-no"};
                $smselt['mobileno'] = $mobilno;
                $smselt['errorcode'] = null;
                $smselt['errordescription'] = null;
                //Update user credit

                if ($smsid == null) {
                    $total_sms_unit = $this->debitcredit($user, $message, $setting);

                    $sms = MessageAPI::find($smssave->id);
                    $sms->messageid = $messageid;
                    $sms->smsclientid = $smsclientid;
                    $sms->mobilno = $mobilno;
                    $sms->total_sms_unit = $total_sms_unit;
                    $sms->save();
                } else {
                    $smssave = MessageAPI::find($smsid);
                    $this->saveExecutedScheduledSMS($smssave, $smsclientid,$sendingservice, $traffic_name);
                }
                $this->checkRemainingBalance($user, $setting);

            } else {
                //Error AND send with Orange

                if ($traffic_id1_count == $traffic_id2_count) {

                    $smselt['status'] = 'error';
                    $smsclientid = intval($xml->error[0]->smsclientid);
                    $smselt['smsclientid'] = $smsclientid;
                    $smselt['messageid'] = null;
                    $smselt['mobileno'] = $this->getNumber($mobil);
                    $error_code = intval($xml->error[0]->{"error-code"});
                    $smselt['errorcode'] = $error_code;
                    $error_description = (string)$xml->error[0]->{"error-description"};
                    $smselt['errordescription'] = $error_description;

                } else {
                    //Send with Orange
                    $smselt = $this->ocmRequest($admin,$mobil, $message, $sender, $user, $setting, $smsid, $smssave,
                        $sendingservice,"OCM", $traffic_id1_count,1);
                }

            }
        } else {
            //Send with Orange
            $smselt = $this->ocmRequest($admin,$mobil, $message, $sender, $user, $setting, $smsid, $smssave,$sendingservice,
                "OCM", $traffic_id1_count,1);
        }
        return $smselt;
    }


    public function sendsmsocm($smsid, $username, $password, $mobil, $message, $sender,
                               $sendingservice, $traffic_id1_count, $traffic_id2_count,
                               $traffic_name, $traffic_id, $stackid, $campaignid){

        $setting = Settings::first();
        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        $traffic = Traffic::find($traffic_id);
        $admin = User::find($traffic->account_id);
        Log::info('OCM api field phone = ' . $mobil . 'message = '.$message. ' sender = '.$sender);
            try {
                $smssave = ($smsid == null)?$this->saveMessage($admin, $user, $username, $password, null,
                    null, $mobil, $sender, $message, $sendingservice, $traffic_name,
                    0, $stackid, $campaignid):MessageAPI::find($smsid);

                $smselt =  $this->ocmRequest($admin,$mobil, $message, $sender, $user, $setting, $smsid, $smssave,$sendingservice,$traffic_name,
                    $traffic_id1_count,$traffic_id2_count);

            } catch (\Exception $exception) {
                if ($stackid != null && $campaignid == null){
                    $sms = MessageAPI::where("uuid_sms_id", $stackid)->first();
                    if ($sms != null){
                        $smselt['status'] = 'success';
                        $smselt['smsclientid'] = $sms->smsclientid;
                        $smselt['messageid'] = $sms->messageid;
                        $smselt['mobileno'] = $sms->mobilno;
                        $smselt['errorcode'] = "NXH202";
                        $smselt['errordescription'] = "Message already exist";
                    }else{
                        $smselt['status'] = 'error';
                        $smselt['smsclientid'] = null;
                        $smselt['messageid'] = null;
                        $smselt['mobileno'] = $this->getNumber($mobil);
                        $smselt['errorcode'] = "NXH500";
                        $smselt['errordescription'] = "Internal error";
                    }
                }else{
                    $smselt['status'] = 'error';
                    $smselt['smsclientid'] = null;
                    $smselt['messageid'] = null;
                    $smselt['mobileno'] = $this->getNumber($mobil);
                    $smselt['errorcode'] = "NXH500";
                    $smselt['errordescription'] = "Internal error";
                }
            }
        return $smselt;
    }


    public function ocmRequest($admin,$mobil, $message, $sender, $user, $setting, $smsid, $smssave,$sendingservice,$traffic_name,
                               $traffic_id1_count,$traffic_id2_count){

        $reponse = $this->ocmapi($mobil, $message, $sender);
        Log::info('OCM api response ' . json_encode($reponse));
        if (array_key_exists('code', $reponse) && $reponse['code'] == "00") {

            //Success
            $smselt['status'] = 'success';
            $smselt['smsclientid'] = $reponse['messageId'];
            $smselt['messageid'] = md5($reponse['messageId']);
            $smselt['mobileno'] = '+' . $reponse['mobileno'];
            $smselt['errorcode'] = null;
            $smselt['errordescription'] = null;
            //Update user credit

            if ($smsid == null) {
                $total_sms_unit = $this->debitcredit($user, $message, $setting);

                $sms = MessageAPI::find($smssave->id);
                $sms->messageid = $smselt['messageid'];
                $sms->smsclientid = $smselt['smsclientid'];
                $sms->mobilno = $smselt['mobileno'];
                $sms->total_sms_unit = $total_sms_unit;
                $sms->save();
            } else {
                $smssave = MessageAPI::find($smsid);
                $this->saveExecutedScheduledSMS($smssave, $smselt['smsclientid'],$sendingservice,$traffic_name);
            }
            $this->checkRemainingBalance($user, $setting);

        } else {

            if ($traffic_id1_count == $traffic_id2_count) {
                $smselt['status'] = 'error';
                $smselt['smsclientid'] = null;
                $smselt['messageid'] = null;
                $smselt['mobileno'] = $this->getNumber($mobil);
                $smselt['errorcode'] = $reponse['code'];
                $smselt['errordescription'] = $reponse['responsedescription'];
            } else {
                $smselt = $this->teleossRequest($admin, $mobil, $message, $sender, $smsid, $user, $setting, $smssave, $sendingservice,
                    "MTNC", 1, $traffic_id2_count);
            }
        }
        return $smselt;
    }


    public function sendsmsbankai($smsid, $username, $password, $mobil, $message, $sender,
                                  $sendingservice, $traffic_name, $traffic_id, $stackid, $campaignid){

        $provider = $this->getProvider(str_replace('+','',$mobil));
        if ($provider->location == "LOCAL"){
            return $this->sendsmsocm($smsid, $username, $password, $mobil, $message, $sender,
                $sendingservice, 0, 1,
                "OCM", $traffic_id, $stackid, $campaignid);
        }else {
            $setting = Settings::first();
            $user = SubUser::where('username', $username)
                ->where('password', $password)
                ->first();
            $traffic = Traffic::find($traffic_id);
            $admin = User::find($traffic->account_id);
            try {
                $smssave = ($smsid == null)?$this->saveMessage($admin, $user, $username, $password, null,
                    null, $mobil, $sender, $message, $sendingservice, $traffic_name,
                    0, $stackid, $campaignid):MessageAPI::find($smsid);

                $response = $this->teleossapi($admin, $mobil, $message, $sender);
                if ($response != null) {
                    $xml = simplexml_load_string($response->getBody()->getContents());
                    $sms = $xml->sms[0];
                    if ($sms != null) {

                        //Success
                        $smselt['status'] = 'success';
                        $smsclientid = (string)intval($sms->{"messageid"});
                        $smselt['smsclientid'] = (string)intval($sms->{"messageid"});
                        $messageid = md5(intval($sms->{"messageid"}));
                        $smselt['messageid'] = $messageid;
                        $mobilno = (string)$sms->{"mobile-no"};
                        $smselt['mobileno'] = $mobilno;
                        $smselt['errorcode'] = null;
                        $smselt['errordescription'] = null;
                        //Update user credit

                        if ($smsid == null) {
                            $total_sms_unit = $this->debitcredit($user, $message, $setting);

                            /*$this->saveMessage($admin, $user, $username, $password, $messageid,
                                $smsclientid, $mobilno, $sender, $message, $sendingservice, $traffic_name,
                                $total_sms_unit, $stackid, $campaignid);*/

                            $sms = MessageAPI::find($smssave->id);
                            $sms->messageid = $messageid;
                            $sms->smsclientid = $smsclientid;
                            $sms->mobilno = $mobilno;
                            $sms->total_sms_unit = $total_sms_unit;
                            $sms->save();
                        }else{
                            $smssave = MessageAPI::find($smsid);
                            $this->saveExecutedScheduledSMS($smssave, $smsclientid, $sendingservice, $traffic_name);
                        }
                        $this->checkRemainingBalance($user, $setting);

                    } else {
                        //Error AND send with Orange
                        $smselt['status'] = 'error';
                        $smsclientid = intval($xml->error[0]->smsclientid);
                        $smselt['smsclientid'] = $smsclientid;
                        $smselt['messageid'] = null;
                        $smselt['mobileno'] = $this->getNumber($mobil);
                        $error_code = intval($xml->error[0]->{"error-code"});
                        $smselt['errorcode'] = $error_code;
                        $error_description = (string)$xml->error[0]->{"error-description"};
                        $smselt['errordescription'] = $error_description;
                    }
                }
            }catch (\Exception $exception){
                if ($stackid != null && $campaignid == null){
                    $sms = MessageAPI::where("uuid_sms_id", $stackid)->first();
                    if ($sms != null){
                        $smselt['status'] = 'success';
                        $smselt['smsclientid'] = $sms->smsclientid;
                        $smselt['messageid'] = $sms->messageid;
                        $smselt['mobileno'] = $sms->mobilno;
                        $smselt['errorcode'] = "NXH202";
                        $smselt['errordescription'] = "Message already exist";
                    }else{
                        $smselt['status'] = 'error';
                        $smselt['smsclientid'] = null;
                        $smselt['messageid'] = null;
                        $smselt['mobileno'] = $this->getNumber($mobil);
                        $smselt['errorcode'] = "NXH500";
                        $smselt['errordescription'] = "Internal error";
                    }
                }else{
                    $smselt['status'] = 'error';
                    $smselt['smsclientid'] = null;
                    $smselt['messageid'] = null;
                    $smselt['mobileno'] = $this->getNumber($mobil);
                    $smselt['errorcode'] = "NXH500";
                    $smselt['errordescription'] = "Internal error";
                }
            }
            return $smselt;
        }

    }

    public function getNumber($mobilno){
        $provider = $this->getProvider(str_replace('+','',$mobilno));
        $number = (substr( $mobilno, 0, 3 ) == $provider->prefix)?'+'.$mobilno:'+'.$provider->prefix.$mobilno;
        return $number;
    }

    public function saveMessage($admin,$user,$username, $password,$messageid,
                                $smsclientid, $mobilno, $senderid, $message, $sendingservice,
                                $traffic_name,$total_sms_unit, $stackid, $campaignid)
    {
        $provider = $this->getProvider(str_replace('+', '', $mobilno));
        $uuid_sms_id = $campaignid!=null?("nxh"."-".$stackid.'-'.$campaignid):$stackid;
        $message = new MessageAPI([
            'messageid' => $messageid,
            'smsclientid' => $smsclientid,
            'mobilno' => $mobilno,
            'user_id' => $admin->id,
            'sub_user_id' => $user->id,
            'master_subuser_id' => $user->master_subuser_id,
            'username' => $username,
            'password' => $password,
            'senderid' => $senderid,
            'message' => $message,
            'provider_id' => $provider!=null?$provider->id:0,
            'total_sms_unit' => $total_sms_unit,
            'messagetype' => $sendingservice,
            'network' => $traffic_name,
            'dlrstatus' => '',
            'responsecode' => 2,
            'responsedescription' => 'Waiting DLR',
            'stackid' => $stackid,
            'campaignid' => $campaignid,
            'uuid_sms_id' => $uuid_sms_id
        ]);
        $message->save();
        return $message;
    }

    public function saveExecutedScheduledSMS($sms, $smsclientid, $sendingservice,$traffic_name ){
        $sms->smsclientid = $smsclientid;
        $sms->schedulesenttime = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime(Carbon::now()->toDateTimeString())));
        $sms->dlrstatus = '';
        //$sms->uuid_sms_id = $uuid_sms_id;
        $sms->responsecode = 2;
        $sms->responsedescription = 'Waiting DLR';
        $sms->network = $traffic_name;
        $sms->messagetype = $sendingservice;
        $sms->dlrsent = 0;
        $sms->save();
    }

    public function sendsmsmtn($smsid, $username, $password, $mobil, $message, $sender,
                               $sendingservice, $traffic_id1_count, $traffic_id2_count,
                               $traffic_name, $traffic_id, $stackid, $campaignid)
    {
        $setting = Settings::first();
        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        $traffic = Traffic::find($traffic_id);
        $admin = User::find($traffic->account_id);
        Log::info('MTN api field phone = ' . $mobil . 'message = '.$message. ' sender = '.$sender);

        $reponse = $this->mtnapi($mobil, $message, $sender);
        Log::info('MTN api response ' . json_encode($reponse));

        if (array_key_exists('code',$reponse) && $reponse['code'] == "00") {

            //Success
            $smselt['status'] = 'success';
            $smselt['smsclientid'] = $reponse['smsClientId'];
            $smselt['messageid'] = md5($reponse['messageId']);
            $smselt['mobileno'] = '+'.$reponse['mobileno'];
            $smselt['errorcode'] = null;
            $smselt['errordescription'] = null;

            if ($smsid == null) {
                $total_sms_unit = $this->debitcredit($user, $message, $setting);
            }else{
                $total_sms_unit = null;
            }
            $this->saveMessage($smsid, $admin,$user,$username, $password,$smselt['messageid'],
                $smselt['smsclientid'], $smselt['mobileno'], $sender, $message, $sendingservice,
                $traffic_name,$total_sms_unit, $stackid, $campaignid);
            $this->checkRemainingBalance($user, $setting);

        } else {

            if ($traffic_id1_count == $traffic_id2_count) {
                $smselt['status'] = 'error';
                $smselt['smsclientid'] = null;
                $smselt['messageid'] = null;
                $smselt['mobileno'] = $this->getNumber($mobil);
                $smselt['errorcode'] = $reponse['code'];
                $smselt['errordescription'] = $reponse['faultstring'];
            }else{
                $smselt = $this->sendsmsocm($smsid, $username, $password, $mobil, $message,
                    $sender, $sendingservice, $traffic_id1_count, $traffic_id2_count,
                    $traffic_name,$traffic_id, $stackid, $campaignid);
            }
        }
        return $smselt;

    }
}
