<?php
/**
 * Created by PhpStorm.
 * User: eddy
 * Date: 13/12/18
 * Time: 10:47
 */

namespace App\Library\Services;

use App\Library\Services\Contracts\MailServiceInterface;
use App\Mail\NexahReport;
use Illuminate\Support\Facades\Mail;


class MailService implements MailServiceInterface
{

    public function sendSimpleMail($data){

        try {
            Mail::send($data['view'], array('data' => $data), function ($message) use ($data) {
                $message->from($data['fromEmail'], $data['fromName']);
                $message->to($data['toEmail'], $data['toName'])->subject($data['subject']);
            });
            return "Email Sent Success";
        }catch (\Exception $e){
            return "Email Sent Failed - " . $e->getMessage();
        }
    }

    public function sendReportMail($data, $user)
    {
        Mail::to(['ulrich.sinha@nexah.net','joseph.njel@nexah.net','hermann.songwa@nexah.net'])
            ->cc(['yannick.belias@nexah.net','yolande.efila@nexah.net','joseph.monny@nexah.net'])
            ->sendNow(new NexahReport($data,$user));
        /*Mail::to(['ulrich.sinha@nexah.net','hermann.songwa@nexah.net'])
           ->sendNow(new NexahReport($data,$user));*/
    }
}
