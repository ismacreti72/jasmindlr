<?php
/**
 * Created by PhpStorm.
 * User: eddy
 * Date: 13/12/18
 * Time: 10:47
 */

namespace App\Library\Services;

use App\Library\Services\Contracts\EventAPIServiceInterface;
use App\Models\ApiConfigs\Settings;
use App\Models\EventManagement\Event;
use App\Models\EventManagement\EventContact;
use App\Models\Sms\MessageAPI;
use App\Models\User\SubUser;

class EventAPIService implements EventAPIServiceInterface
{

    public function createEvent($username, $password, $name, $message, $senderid,
                                $details, $status, $week, $file)
    {
       $user = $this->checkuser($username, $password);
        $settings = Settings::first();
       if (is_array($user)){
           return $user;
       }
        try{
            $event = new Event([
                'user_id' => $user->id,
                'user_admin_id' => $user->master_subuser_id,
                'name' => $name,
                'message' => $message,
                'senderid' => $senderid,
                'status' => $status,
                'week' => $week,
                'file' => $file
            ]);
            $event->save();
            for ($i=0; $i < sizeof($details); $i++){
                $total_sms_unit = ceil(strlen($details[$i]['message'])/$settings->sms_size);
                if ($user->credit >= $total_sms_unit) {
                    $eventContact = new EventContact([
                        'event_id' => $event->id,
                        'mobileno' => $details[$i]['mobileno'],
                        'message' => $details[$i]['message'],
                        'date_event' => date("Y-m-d", strtotime($details[$i]['date_event']))
                    ]);
                    $eventContact->save();
                    $oldcredit = $user->credit;
                    $user->credit = $oldcredit - $total_sms_unit;
                    $user->save();
                }
            }

            $response = $this->getEventResponse($event);
            return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200, "event" => $response);

        }catch (\Exception $e){
            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 101, "message" => $e->getMessage());
        }

    }


    public function updateEvent($username, $password, $eventid, $name, $message,
                                $senderid, $details, $status,$week, $file)
    {
        $user = $this->checkuser($username, $password);
        $settings = Settings::first();
        if (is_array($user)){
            return $user;
        }
        try{
            $event = $this->getUserEvent($eventid, $user->id);
            if (is_array($event)){
                return $event;
            }
            $event->name = $name;
            $event->message = $message;
            $event->file = $file;
            $event->senderid = $senderid;
            $event->status = $status;
            $event->week = $week;
            $event->save();

            for ($i=0; $i < sizeof($details); $i++){
                $eventContact = EventContact::where('event_id', $eventid)
                    ->where('mobileno', $details[$i]['mobileno'])
                    ->first();
                if ($eventContact == null){
                    $total_sms_unit = ceil(strlen($details[$i]['message'])/$settings->sms_size);
                    if ($user->credit >= $total_sms_unit) {
                        $eventContact = new EventContact([
                            'event_id' => $eventid,
                            'mobileno' => $details[$i]['mobileno'],
                            'message' => $details[$i]['message'],
                            'date_event' => date("Y-m-d", strtotime($details[$i]['date_event']))
                        ]);
                        $eventContact->save();
                        $oldcredit = $user->credit;
                        $user->credit = $oldcredit - $total_sms_unit;
                        $user->save();
                    }
                }
            }

            $response = $this->getEventResponse($event);
            return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200, "event" => $response);

        }catch (\Exception $e){
            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 101, "message" => $e->getMessage());
        }
    }

    public function deleteEvent($username, $password, $eventid)
    {
        $user = $this->checkuser($username, $password);
        if (is_array($user)){
            return $user;
        }
        $event = $this->getUserEvent($eventid, $user->id);
        if (is_array($event)){
            return $event;
        }
        $event->is_deleted = 1;
        $event->save();
        $response = $this->getEventResponse($event);
        return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200, "event" => $response);
    }


    public function enable($username, $password, $eventid, $status)
    {
        $user = $this->checkuser($username, $password);
        if (is_array($user)){
            return $user;
        }
        $event = $this->getUserEvent($eventid, $user->id);
        if (is_array($event)){
            return $event;
        }

        if ($event->is_deleted==1){
            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 101,"message_fr" => "Cet évènement n'existe pas","message_en" => "Event no exist");
        }
        $event->status = $status;
        $event->save();
        $response = $this->getEventResponse($event);
        return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200, "event" => $response);

    }

    public function getEventResponse($event){

        $eventContacts = EventContact::where('event_id','=',$event->id)->get();
        $response = array(
            'id' => $event->id,
            'user_id' => $event->user_id,
            'user_admin_id' => $event->user_admin_id,
            'message' => $event->message,
            'name' => $event->name,
            'senderid' => $event->senderid,
            'status' => $event->status,
            'week' => $event->week,
            'exe_status' => $event->exe_status,
            'file' => $event->file,
            'is_deleted' => $event->is_deleted,
            'created_at' => $this->time($event->created_at),
            'updated_at' => $this->time($event->updated_at),
            'mobiles' => $eventContacts
        );
        return $response;
    }

    public function time($date){
        return date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($date)));
    }

    public function checkuser($username, $password){
        $user = SubUser::where('username', $username)
            ->where('password', $password)->first();
        if ($user == null){
            return array("responsecode" => 0, "responsedescription" => "error", "errcode" => 100, "message_fr" => "username ou mot de passe invalide", "message_en" => "Invalid user or password");
        }
        return $user;
    }

    public function getUserEvent($eventid, $userid){
        $event = Event::where('id',$eventid)->where('user_id',$userid)->first();
        if ($event == null){
            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 101,"message_fr" => "Evenement n'existe pas","message_en" => "Event Not Found");
        }
        return $event;
    }

    public function listEvent($username, $password)
    {
        $user = SubUser::where("username",$username)
            ->where("password",$password)
            ->first();
        if ($user != null){
            if ($user->is_admin == 1){
                $events = Event::where('user_admin_id',$user->id)
                    ->orWhere('user_id',$user->id)
                    ->latest()
                    ->get();
            }else{
                $events = Event::where('user_id',$user->id)
                    ->latest()
                    ->get();
            }
            return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,"events" => $events);
        }else{
            return array("responsecode" => 0, "responsedescription" => "error", "errcode" => 100, "message_fr" => "username ou mot de passe invalide", "message_en" => "Invalid user or password");
        }
    }

    public function listEventContact($username, $password, $eventid)
    {
        $user = SubUser::where("username",$username)
            ->where("password",$password)
            ->first();
        if ($user != null){
            $event = Event::find($eventid);
            $eventContacts = EventContact::where('event_id',$eventid)
                ->latest()
                ->get();
            return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,
                "event" => $event, "eventContacts" => $eventContacts);
        }else{
            return array("responsecode" => 0, "responsedescription" => "error", "errcode" => 100, "message_fr" => "username ou mot de passe invalide", "message_en" => "Invalid user or password");
        }
    }

    public function listEventMessages($username, $password, $eventid)
    {
        $user = SubUser::where("username",$username)
            ->where("password",$password)
            ->first();
        if ($user != null){
            $event = Event::find($eventid);
            $eventsms = MessageAPI::where('campaignid',$eventid)
                ->where('username',$username)
                ->where('password',$password)
                ->get();
            return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,
                "event" => $event,"eventsms" => $eventsms);
        }else{
            return array("responsecode" => 0, "responsedescription" => "error", "errcode" => 100, "message_fr" => "username ou mot de passe invalide", "message_en" => "Invalid user or password");
        }
    }
}
