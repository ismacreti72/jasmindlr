<?php


namespace App\Library\Services;


use App\Library\Services\Contracts\SmsAccountAPIServiceInterface;

class SmsAccountAPIService implements SmsAccountAPIServiceInterface
{

    public function createAcount($data)
    {

        $urlcreate = "https://sms.nexah.net/api/v1/createaccount";

        $ch = curl_init( $urlcreate );
        $payload = json_encode( $data );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
