<?php
/**
 * Created by PhpStorm.
 * User: eddy
 * Date: 13/12/18
 * Time: 10:47
 */

namespace App\Library\Services;

use App\Library\Services\Contracts\ContactAPIServiceInterface;
use App\Models\Sms\FilteredMessageAPI;
use Illuminate\Support\Facades\DB;

class ContactAPIService implements ContactAPIServiceInterface
{


    public function send_filtered_sms($id, $username, $password, $mobilesarr, $mobilesstr, $message, $sender, $filters)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://smsvas.com/teleoss/sendsms.jsp', [
            'headers' => ['Accept' => 'application/xml'],
            'form_params' => [
                'user' => $username,
                'password' => $password,
                'mobiles' => $mobilesstr,
                'sms' => $message,
                'senderid' => $sender,
                'isallowduplicatemobile' => 'no']
        ]);
        $xml = simplexml_load_string($response->getBody()->getContents());
        if ($xml->count() > 1 || ($xml->count() == 1 && $xml->sms[0] != null )){

            //SMS sent
            return $this->response($id, $xml, $username, $password, $sender, $message, $mobilesarr, $filters);
        }else{

            //SMS not sent
            $smsclientid = intval($xml->error[0]->smsclientid);
            $error_code = intval($xml->error[0]->{"error-code"});
            $error_description = (string)$xml->error[0]->{"error-description"};

            $codes = array(-10002,-10017,-10019,-10016, -10015, -10005, -10011, -10006, -10007, -10021, -10022, -10029);
            if (in_array($error_code, $codes)){
                return array("responsecode" => 0, "responsedescription" => "error", "responsemessage" => $error_description, "sms" => array());
            }else {
                $errsms = array();
                $errsms['status'] = 'error';
                $errsms['smsclientid'] = $smsclientid;
                $errsms['messageid'] = '';
                $errsms['mobileno'] = "+237" . str_replace("237", "",$mobilesarr[0]);
                $errsms['errorcode'] = $error_code;
                $errsms['errordescription'] = $error_description;
                return array("responsecode" => 0, "responsedescription" => "error", "responsemessage" => "error", "sms" => array("sms" => $errsms));
            }
        }
    }

    public function send_filtered_scheduledsms($id, $username, $password, $mobilesarr, $mobilesstr, $message, $sender, $scheduletime, $filters)
    {
        // TODO: Implement send_filtered_scheduledsms() method.
    }

    public function response($id, $xml, $username, $password, $sender, $message, $mobilesarr, $filters){
        $returnarr = array();
        for ($i=0; $i<$xml->count();$i++){
            $sms = $xml->sms[$i];

            $responsearr = array(
                'messageid' => '',
                'mobileno' => ''
            );

            if ($sms != null) {
                // SMS sent
                $messageid = intval($sms->{"messageid"});
                $responsearr['messageid'] = $messageid;
                $mobilno = (string)$sms->{"mobile-no"};
                $responsearr['mobileno'] = $mobilno;
                $this->saveMessage($id, $messageid, $mobilesarr, $filters, $username, $password, $sender, $message);
            }else{
                //SMS not sent
                $responsearr['status'] = 'error';
                $smsclientid = intval($xml->error[0]->smsclientid);
                $responsearr['smsclientid'] = $smsclientid;
                $error_code = intval($xml->error[0]->{"error-code"});
                $responsearr['errorcode'] = $error_code;
                $error_description = (string)$xml->error[0]->{"error-description"};
                $responsearr['errordescription'] = $error_description;
                $responsearr['mobileno'] = "+237" . str_replace("237", "",$mobilesarr[$i]);
            }
            $returnarr[$i] = $responsearr;
        }

        return array("responsecode"  => 1, "responsedescription"  => "success" , "responsemessage"  => "success", "sms"  => $returnarr);
    }


    public function saveMessage($id, $messageid, $phones, $filters, $username, $password, $senderid, $sms)
    {
        $message = FilteredMessageAPI::findOrFail($id);
        if ($message == null){
            $message = new FilteredMessageAPI([
                'requestid' => $id,
                'username' => $username,
                'password' => $password,
                'senderid' => $senderid,
                'message' => $sms,
                'contacts' => sizeof($phones),
                'filters' => $filters,
                'messagesid'=> array($messageid),
                'sent' => $messageid!=null?1:0
            ]);
        }else{
            //update the message
        }

        $message->save();
    }


    public function filter($ville, $quartier, $sexe, $annee_naiss)
    {
        return DB::table('contacts')
            ->select('phone')
            ->whereIn('sexe', [$sexe[0], $sexe[1]])
            ->where('ville', $ville)
            ->where('quartier', $quartier)
            ->whereBetween('annee_naiss', [$annee_naiss[0], $annee_naiss[1]])
            ->get();
    }


}