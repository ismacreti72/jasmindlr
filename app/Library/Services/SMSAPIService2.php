<?php
/**
 * Created by PhpStorm.
 * User: eddy
 * Date: 13/12/18
 * Time: 10:47
 */

namespace App\Library\Services;

use App\Library\Services\Contracts\SMSAPIServiceInterface;
use App\Models\ApiConfigs\SendingService;
use App\Models\ApiConfigs\SendingTraffic;
use App\Models\ApiConfigs\Settings;
use App\Models\ApiConfigs\Traffic;
use App\Models\Sms\MessageAPI;
use App\Models\Sms\Recharge;
use App\Models\User\SubUser;
use App\Models\User\User;
use Illuminate\Support\Facades\DB;

class SMSAPIService2 extends CoreSMSAPI implements SMSAPIServiceInterface
{


    public function checkoperator($phone){
        $traffics = Traffic::get();
        foreach ($traffics as $traffic){
            if (preg_match_all($traffic->phone_regex, $phone, $out)) {
                return $traffic;
            }
        }
    }


    public function sendbulksms($username, $password, $smsarr)
    {
        $admin = User::where('username', $username)->where('password', $password)->first();
        if ($admin == null){
            $user = SubUser::where('username', $username)->where('password', $password)->first();
            $admin = User::find($user->master_user_id);
        }
        $settings = Settings::first();
        $smsresultarr = array();
        for ($i=0; $i<sizeof($smsarr); $i++) {
            $smscredit = $this->balancecheck($username, $password);
            $smselt = array();
            if ($smscredit["credit"] > 0) {
                if (preg_match_all($settings->phone_regex, $smsarr[$i]['mobileno'], $out)) {

                    $sending_traffic_id = $admin->sending_traffic_id;
                    $sending_traffic = SendingTraffic::find($sending_traffic_id);
                    $sendingservice = SendingService::find($sending_traffic->sending_service_id);
                    $service_name = $sendingservice->name;

                    $traffic_id = $sending_traffic->traffic_id;
                    $traffic = Traffic::find($traffic_id);

                    //send request to traffic url and get response
                    $smselt = json_decode($this->sendRequest($traffic->url != null?$traffic:$this->checkoperator($smsarr[$i]['mobileno']), $username, $password, $smsarr[$i]['mobileno'],  $smsarr[$i]['message'], $smsarr[$i]['senderid'], $service_name));
                    $smselt->senderid = $smsarr[$i]['senderid'];
                    $smselt->idsmsskysoft = $smsarr[$i]['idsmsskysoft'];
                    $smselt->message = $smsarr[$i]['message'];

                }else{
                    $smselt['status'] = 'error';
                    $smselt['smsclientid'] = null;
                    $smselt['messageid'] = null;
                    $smselt['mobileno'] = "+237" . str_replace("237", "", $smsarr[$i]['mobileno']);
                    $smselt['errorcode'] = -10003;
                    $smselt['errordescription'] = "Invalid mobile number";
                    $smselt['senderid'] = $smsarr[$i]['senderid'];
                    $smselt['idsmsskysoft'] = $smsarr[$i]['idsmsskysoft'];
                    $smselt['message'] = $smsarr[$i]['message'];
                }
            }else{
                $smselt['status'] = 'error';
                $smselt['smsclientid'] = null;
                $smselt['messageid'] = null;
                $smselt['mobileno'] = "+237" . str_replace("237", "", $smsarr[$i]['mobileno']);
                $smselt['errorcode'] = -10008;
                $smselt['errordescription'] = "Balance not enough";
                $smselt['senderid'] = $smsarr[$i]['senderid'];
                $smselt['idsmsskysoft'] = $smsarr[$i]['idsmsskysoft'];
                $smselt['message'] = $smsarr[$i]['message'];
            }
            $smsresultarr[$i] = $smselt;
        }
        return array("user"  => $username, "password"  => $password ,"responsecode"  => 1, "responsedescription"  => "success" , "responsemessage"  => "success", "sms"  => $smsresultarr);

    }

    public function sendsms($username, $password, $numbers, $message, $sender, $scheduletime){
        $numbers = str_replace(" ", "", $numbers);
        $mobilnos = explode(',', $numbers);
        $subuser = SubUser::where('username', $username)->where('password', $password)->first();
        if ($subuser == null){
            $user = User::where('username', $username)->where('password', $password)->first();
        }else{
            $user = User::find($subuser->master_user_id);
        }
        $settings = Settings::first();

        $smsarr = array();
        for ($i=0; $i<sizeof($mobilnos); $i++) {
            $smscredit = $this->balancecheck($username, $password);
            $smselt = array();
            if ($smscredit["credit"] > 0) {
                if (preg_match_all($settings->phone_regex, $mobilnos[$i], $out)) {

                    $sending_traffic_id = $user->sending_traffic_id;
                    $sending_traffic = SendingTraffic::find($sending_traffic_id);
                    $sendingservice = SendingService::find($sending_traffic->sending_service_id);
                    $service_name = $sendingservice->name;

                    $traffic_id = $sending_traffic->traffic_id;
                    $traffic = Traffic::find($traffic_id);

                    if ($scheduletime == null) {
                        //send request to traffic url and get response
                        $smselt = json_decode($this->sendRequest($traffic->url != null ? $traffic : $this->checkoperator($mobilnos[$i]), $username, $password, $mobilnos[$i], $message, $sender, $service_name));
                    }else{
                        //Save schedule message
                        $total_sms_unit = $this->debitcredit($subuser!=null?$subuser:$user,$message,$settings);
                        $smselt = $this->saveScheduleMessage($user,$subuser,$mobilnos[$i],$sender,$message,$service_name,$total_sms_unit);
                    }
                }else{
                    $smselt['status'] = 'error';
                    $smselt['smsclientid'] = null;
                    $smselt['messageid'] = null;
                    $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                    $smselt['errorcode'] = -10003;
                    $smselt['errordescription'] = "Invalid mobile number";
                }
            }else{
                $smselt['status'] = 'error';
                $smselt['smsclientid'] = null;
                $smselt['messageid'] = null;
                $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                $smselt['errorcode'] = -10008;
                $smselt['errordescription'] = "Balance not enough";
            }
            $smsarr[$i] = $smselt;
        }
        return array("responsecode"  => 1, "responsedescription"  => "success" , "responsemessage"  => "success", "sms"  => $smsarr);

    }

    /*public function sendsms2($username, $password, $numbers, $message, $sender){
        $numbers = str_replace(" ", "", $numbers);
        $mobilnos = explode(',', $numbers);
        $subuser = SubUser::where('username', $username)->where('password', $password)->first();
        if ($subuser == null){
            $user = User::where('username', $username)->where('password', $password)->first();
        }else{
            $user = User::find($subuser->master_user_id);
        }
        $settings = Settings::first();

        $smsarr = array();
        for ($i=0; $i<sizeof($mobilnos); $i++) {
            $smscredit = $this->balancecheck($username, $password);
            $smselt = array();
            if ($smscredit["credit"] > 0) {
                if (preg_match_all($settings->phone_regex, $mobilnos[$i], $out)) {

                    $sending_traffic_id = $user->sending_traffic_id;
                    $sending_traffic = SendingTraffic::find($sending_traffic_id);
                    $sendingservice = SendingService::find($sending_traffic->sending_service_id);
                    $service_name = $sendingservice->name;

                    $traffic_id = $sending_traffic->traffic_id;
                    $traffic = Traffic::find($traffic_id);

                    //send request to traffic url and get response
                    $smselt = json_decode($this->sendRequest($traffic->url != null?$traffic:$this->checkoperator($mobilnos[$i]), $username, $password, $mobilnos[$i], $message, $sender, $service_name));

                }else{
                    $smselt['status'] = 'error';
                    $smselt['smsclientid'] = null;
                    $smselt['messageid'] = null;
                    $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                    $smselt['errorcode'] = -10003;
                    $smselt['errordescription'] = "Invalid mobile number";
                }
            }else{
                $smselt['status'] = 'error';
                $smselt['smsclientid'] = null;
                $smselt['messageid'] = null;
                $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                $smselt['errorcode'] = -10008;
                $smselt['errordescription'] = "Balance not enough";
            }
            $smsarr[$i] = $smselt;
        }
        return array("responsecode"  => 1, "responsedescription"  => "success" , "responsemessage"  => "success", "sms"  => $smsarr);

    }*/


    public function sendRequest($traffic,$username, $password, $mobil, $message, $sender, $sendingservice){
        //$message = preg_replace('/\r\n|\r|\n/','\n', addslashes($message));

        $sms_args = array(
            'user' => $username,
            'password' => $password,
            'mobileno' => $mobil,
            'message' => $message,
            'senderid' => $sender,
            'sendingservice' => $sendingservice,
            'traffic_id1_count' => $traffic->traffic_id1_count,
            'traffic_id2_count' => $traffic->traffic_id2_count,
            'traffic_name' => $traffic->name
        );
        //Log::info('OCM API Agrs' . json_encode($sms_args));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $traffic->url);
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sms_args, '', '&'));
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;

    }


    /*public function sendscheduledsms($username, $password, $numbers, $message, $sender, $scheduletime)
    {
        $numbers = str_replace(" ", "", $numbers);
        $mobilnos = explode(',', $numbers);
        $user = User::where('username', $username)->where('password', $password)->first();
        $settings = Settings::first();

        $smsarr = array();
        for ($i=0; $i<sizeof($mobilnos); $i++) {
            $smscredit = $this->balancecheck($username, $password);
            $smselt = array();
            if ($smscredit["credit"] > 0) {
                if (preg_match_all($settings->phone_regex, $mobilnos[$i], $out)) {
                    $settings = Settings::first();
                    $sendingservice = $settings->sendingservice;
                    $response = $this->teleoss_schedulesmsapi($username, $password, $mobilnos[$i], $message, $sender, $scheduletime);
                    $xml = simplexml_load_string($response->getBody()->getContents());
                    $sms = $xml->sms[0];
                    if ($sms != null) {
                        //Success
                        $smselt['status'] = 'success';
                        $smsclientid = intval($sms->{"smsclientid"});
                        $smselt['smsclientid'] = $smsclientid;
                        $messageid = intval($sms->{"messageid"});
                        $smselt['messageid'] = $messageid;
                        $mobilno = (string)$sms->{"mobile-no"};
                        $smselt['mobileno'] = $mobilno;
                        $smselt['errorcode'] = '';
                        $smselt['errordescription'] = '';
                        $this->saveMessage($messageid, $smsclientid, $mobilno, $username, $password, $sender, $message, $sendingservice, 'MTNC');
                        //Update user credit
                        $oldcredit = $user->credit;
                        $user->credit = $oldcredit - 1;
                        $user->save();
                    } else {
                        //Error
                        $smselt['status'] = 'error';
                        $smsclientid = intval($xml->error[0]->smsclientid);
                        $smselt['smsclientid'] = $smsclientid;
                        $smselt['messageid'] = '';
                        $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                        $error_code = intval($xml->error[0]->{"error-code"});
                        $smselt['errorcode'] = $error_code;
                        $error_description = (string)$xml->error[0]->{"error-description"};
                        $smselt['errordescription'] = $error_description;
                    }
                }else{
                    $smselt['status'] = 'error';
                    $smselt['smsclientid'] = '';
                    $smselt['messageid'] = '';
                    $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                    $smselt['errorcode'] = -10003;
                    $smselt['errordescription'] = "Invalid mobile number";
                }
            }else{
                $smselt['status'] = 'error';
                $smselt['smsclientid'] = '';
                $smselt['messageid'] = '';
                $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                $smselt['errorcode'] = -10008;
                $smselt['errordescription'] = "Balance not enough";
            }
            $smsarr[$i] = $smselt;
        }
        return array("responsecode"  => 1, "responsedescription"  => "success" , "responsemessage"  => "success", "sms"  => $smsarr);
    }


    public function saveMessage($messageid, $smsclientid, $mobilno, $username, $password, $senderid, $message, $sendingservice, $network)
    {
        $message = new MessageAPI([
            'messageid' => $messageid,
            'smsclientid' => $smsclientid,
            'mobilno' => $mobilno,
            'username' => $username,
            'password' => $password,
            'senderid' => $senderid,
            'message' => $message,
            'messagetype' => $sendingservice,
            'network' => $network,
            'dlrstatus' => '',
            'responsecode' => 2,
            'responsedescription' => 'waiting DLR'
        ]);
        $message->save();
    }*/


    public function saveScheduleMessage($user,$subuser, $mobilno, $senderid, $message, $sendingservice,$total_sms_unit)
    {
        $message = new MessageAPI([
            'mobilno' => $mobilno,
            'user_id' => $user->id,
            'sub_user_id' => $subuser!=null?$subuser->id:null,
            'master_subuser_id' => ($subuser!=null && $subuser->master_subuser_id!=null)?$subuser->master_subuser_id:null,
            'username' => $user->username,
            'password' => $user->password,
            'senderid' => $senderid,
            'message' => $message,
            'provider_id' => $this->getProvider(str_replace('+','',$mobilno)),
            'total_sms_unit' => $total_sms_unit,
            'messagetype' => $sendingservice,
            'dlrstatus' => '',
            'responsecode' => 3,
            'responsedescription' => 'Waiting scheduletime'
        ]);
        $message->save();
    }


    public function balancecheck($username, $password)
    {
        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        if ($user == null){
            $user = User::where('username', $username)
                ->where('password', $password)
                ->first();
            if ($user != null){
                return array(
                    "error" => null,
                    "accountexpdate"  => $user->accountexpdate,
                    "balanceexpdate"  => $user->balanceexpdate,
                    "credit"  => $user->credit
                );
            }else{
                return array("error" => "Invalid username or password");
            }
        }else{
            return array(
                "error" => null,
                "accountexpdate"  => $user->accountexpdate,
                "balanceexpdate"  => $user->balanceexpdate,
                "credit"  => $user->credit
            );
        }


    }

    public function rechargesubuser($username, $password, $subuserid, $credit)
    {
        $settings = Settings::first();

        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        $subuser = SubUser::find($subuserid);

        if ($user != null && $subuser != null && $subuser->master_subuser_id = $user->id){
            $recharge = $subuser->credit + $credit;
            /*if ($recharge >= 0){
            if ($credit > 0){
                //Credit
                if ($user->credit >= $credit) {
                    $recharge = new Recharge([
                        'user_id' => $user->master_user_id,
                        'master_subuser_id' => $user->id,
                        'sub_user_id' => $subuser->id,
                        'credit' => $credit,
                        'oldcredit' => $subuser->credit,
                        'newcredit' => $subuser->credit + $credit
                    ]);
                    $recharge->save();

                    $subuser->credit = $subuser->credit + $credit;
                    $subuser->save();

                    $user->credit = $user->credit - $credit;
                    $user->save();
                }
            }else if ($credit < 0){
                //Debit
                $debit = $subuser->credit + $credit;
                if ($debit >= 0){

                }else{
                    //Erreur
                }
            }*/
            if (($user->credit >= $credit && $recharge >= 0) || ($user->credit == 0 && $credit < 0 && $recharge >= 0)) {
                try {
                    $recharge = new Recharge([
                        'user_id' => $user->master_user_id,
                        'master_subuser_id' => $user->id,
                        'sub_user_id' => $subuser->id,
                        'credit' => $credit,
                        'oldcredit' => $subuser->credit,
                        'newcredit' => $subuser->credit + $credit
                    ]);
                    $recharge->save();

                    $subuser->credit = $subuser->credit + $credit;
                    $subuser->save();

                    $user->credit = $user->credit - $credit;
                    $user->save();

                    if ($subuser->phone != null && $user->phone != null) {
                        if ($credit > 0){
                            if ($user->notification_enable == 1) {
                                $message_user = str_replace(array('$USER','$CREDIT','$BALANCE'),array($subuser->username,$credit,$subuser->credit),$settings->recharge_message_subuser);
                                $this->sendsms($user->username, $user->password, $subuser->phone, $message_user, $user->company_sender,null);
                            }
                            return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,"message_fr" => "Compte crédité","message_en" => "Account credited");
                        }else{
                            if ($user->notification_enable == 1) {
                                $message_subuser = str_replace(array('$USER', '$CREDIT', '$BALANCE'), array($subuser->username, abs($credit), $subuser->credit), $settings->debit_message_subuser);
                                $message_user = str_replace(array('$USER', '$CREDIT', '$BALANCE'), array($user->username, abs($credit), $user->credit), $settings->recharge_message_subuser);
                                $this->sendsms($user->username, $user->password, $subuser->phone, $message_subuser, $user->company_sender,null);
                                $this->sendsms($user->username, $user->password, $user->phone, $message_user, $user->company_sender,null);
                            }
                            return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,"message_fr" => "Compte débité","message_en" => "Account debited");
                        }
                    }

                } catch (\Exception $e) {
                    return array("responsecode" => 0,"responsedescription" => "error","errcode" => -100,"message_en" => $e->getMessage());
                }

            }else{
                return array("responsecode" => 0,"responsedescription" => "error","errcode" => 100,"message_fr" => "Solde insuffisant","message_en" => "Balance not enough");
            }

        }else{
            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 101,"message_fr" => "Utilisateur invalide","message_en" => "Invalid Sub user");
        }
    }

    public function recharge($username, $password, $credit)
    {
        $settings = Settings::first();
        $admin = User::find($settings ->admin_user_id);

        $user = User::where('username', $username)
            ->where('password', $password)
            ->first();
        if ($user != null){
            try {
                $recharge = new Recharge([
                    'user_id' => $user->id,
                    'master_subuser_id' => null,
                    'sub_user_id' => null,
                    'credit' => $credit,
                    'oldcredit' => $user->credit,
                    'newcredit' => $user->credit + $credit
                ]);
                $recharge->save();

                $oldCredit = $user->credit;
                $newCredit = $oldCredit + $credit;
                $user->credit = $newCredit;
                $user->save();
                if ($user->phone != null) {
                    $message_user = str_replace(array('$CREDIT','$BALANCE'),array($credit,$user->credit),$settings->recharge_message_user);
                    $this->sendsms($admin->username, $admin->password, $user->phone, $message_user, $admin->senderid, null);
                }
                $message_admin = str_replace(array('$USER','$CREDIT','$BALANCE'),array($user->username,$credit,$user->credit),$settings->recharge_message_admin);
                $this->sendsms($admin->username, $admin->password, $admin->phone, $message_admin, $admin->senderid, null);

                return array("status" => "Account credited");
            }catch (\Exception $e){
                return array("error" => $e->getMessage());
            }
        }else{
            $subuser = SubUser::where('username', $username)
                ->where('password', $password)
                ->first();

            if ($subuser != null){
                try {
                    if ($subuser->master_subuser_id != null){
                        $masteruser = SubUser::find($subuser->master_subuser_id);
                    }else{
                        $masteruser = User::find($subuser->master_user_id);
                    }
                    if ($masteruser->credit >= $credit) {
                        $recharge = new Recharge([
                            'user_id' => $subuser->master_user_id,
                            'master_subuser_id' => $subuser->master_subuser_id,
                            'sub_user_id' => $subuser->id,
                            'credit' => $credit,
                            'oldcredit' => $subuser->credit,
                            'newcredit' => $subuser->credit + $credit
                        ]);
                        $recharge->save();

                        $oldCredit = $subuser->credit;
                        $newCredit = $oldCredit + $credit;
                        $subuser->credit = $newCredit;
                        $subuser->save();

                        $masteruser->credit = $masteruser->credit - $credit;
                        $masteruser->save();

                        if ($subuser->phone != null) {
                            $message_subuser = str_replace(array('$CREDIT','$BALANCE'),array($credit,$subuser->credit),$settings->recharge_message_user);
                            $this->sendsms($admin->username, $admin->password, $subuser->phone, $message_subuser, $admin->senderid, null);
                        }
                        $message_admin = str_replace(array('$USER','$CREDIT','$BALANCE'),array($subuser->username,$credit,$subuser->credit),$settings->recharge_message_admin);
                        $this->sendsms($admin->username, $admin->password, $admin->phone, $message_admin, $admin->senderid, null);

                        return array("status" => "Account credited");
                    }
                }catch (\Exception $e){
                    return array("error" => $e->getMessage());
                }
            }else {
                return array("error" => "Invalid username or password");
            }
        }
    }


    public function getDLRLocal($username, $password, $messageid)
    {

        $result = array();
        $user = User::where('username', $username)->where('password', $password)->first();
        if ($user != null){
            $message = MessageAPI::where('messageid',$messageid)
                ->where('user_id',$user->id)
                ->first();
            if ($message != null){
                $result["reponsecode"] = $message->responsecode;
                $result["reponsedescription"] = $message->responsedescription;
                $result["mobileno"] = $message->mobilno;
                $result["messageid"] = $message->messageid;
                $result["submittime"] = $message->submittime;
                $result["senttime"] = $message->senttime;
                $result["deliverytime"] = $message->deliverytime;
                $result["status"] = $message->dlrstatus;
            }else{
                $result["reponsecode"] = 0;
                $result["reponsedescription"] = "Message not found";
            }
        }else{
            $result["reponsecode"] = 0;
            $result["reponsedescription"] = "Invalid Username or Password";
        }

        return $result;
    }


    public function createapiuser($username, $password, $phone,
                                  $credit, $has_dlr_url, $dlr_url,
                                  $batch_dlr_url,$accountexpdate,
                                  $balanceexpdate, $company,$sending_traffic_id,$senderid)
    {
        try {
            $user = new User([
                'username' => $username,
                'password' => $password,
                'phone' => $phone,
                'credit' => $credit,
                'company' => $company,
                'sending_traffic_id' => $sending_traffic_id,
                'has_dlr_url' => $has_dlr_url,
                'dlr_url' => $dlr_url,
                'batch_dlr_url' => $batch_dlr_url,
                'senderid' => $senderid,
                'accountexpdate' => $accountexpdate,
                'balanceexpdate' => $balanceexpdate
            ]);
            $user->save();
            return array("responsecode" => 1, "responsedescription" => "Success", "message" => "User successfully created");
        }catch (\Exception $e){
            return array("responsecode" => 0, "responsedescription" => "error", "message" => $e->getMessage());
        }
    }

    public function addsubuser($username,$password,$subusername,$subuserpassword, $phone,$senderid,$credit,$company)
    {
        try {
            $user = SubUser::where('username',$subusername)->first();
            $settings = Settings::first();
            $master = User::find($settings->customer_user_id);
            if ($user == null) {
                $submaster = SubUser::where('username', $username)
                    ->where('password', $password)
                    ->first();
                if ($submaster != null) {
                    if ($submaster->credit >= $credit) {
                        $user = new SubUser([
                            'username' => $subusername,
                            'password' => $subuserpassword,
                            'phone' => $phone,
                            'credit' => $credit,
                            'company' => $company,
                            'master_user_id' => $master->id,
                            'master_subuser_id' => $submaster->id,
                            'senderid' => $senderid,
                            'accountexpdate' => $master->accountexpdate,
                            'balanceexpdate' => $master->balanceexpdate
                        ]);
                        $user->save();
                        $submaster->credit = $submaster->credit - $credit;
                        $submaster->save();
                        if ($user->phone != null && $submaster->phone != null) {
                            if ($submaster->notification_enable == 1) {
                                $message_user = str_replace(array('$USER','$PWD','$CREDIT'),array($user->username,$user->password,$user->credit),$settings->create_message_subuser);
                                $this->sendsms($submaster->username, $submaster->password, $user->phone, $message_user, $submaster->company_sender, null);
                            }
                        }
                    } else {
                        return array("responsecode" => 0, "responsedescription" => "error", "errcode" => 100, "message_fr" => "Solde insuffisant", "message_en" => "Balance not enough");
                    }
                }else{
                    return array("responsecode" => 0, "responsedescription" => "error", "errcode" => 102, "message_fr" => "user admin invalide", "message_en" => "Invalid admin user");
                }
            }else{
                return array("responsecode" => 0, "responsedescription" => "error",  "errcode" => 101,"message_fr" => "Login existant","message_en" => "User already exist");
            }

            return array("responsecode" => 1, "responsedescription" => "Success", "message_fr" => "Utilisateur enregistré","message_en" => "User successfully created");
        }catch (\Exception $e){
            return array("responsecode" => 0, "responsedescription" => "error", "errcode" => -100,"message_en" => $e->getMessage());
        }
    }


    public function getapisms($username, $password, $from, $to, $limit)
    {
        $user = User::where('username', $username)
            ->where('password', $password)
            ->first();
        if ($user != null){
            $smsarr = MessageAPI::where('user_id', $user->id)
                //->where('password', $password)
                ->whereBetween('created_at', [$from, $to])
                ->offset(0)
                ->limit($limit)
                ->latest()
                ->get();

            return array("responsecode" => 1, "responsedescription" => "Success", "sms" => $smsarr);
        }else{
            $user = SubUser::where('username', $username)
                ->where('password', $password)
                ->first();
            if ($user != null){
                $smsarr = MessageAPI::where('sub_user_id', $user->id)
                    ->OrWhere('master_subuser_id', $user->id)
                    ->whereBetween('created_at', [$from, $to])
                    ->offset(0)
                    ->limit($limit)
                    ->latest()
                    ->get();

                return array("responsecode" => 1, "responsedescription" => "Success", "sms" => $smsarr);
            }else{
                return array("responsecode" => 0, "responsedescription" => "Error", "message" => "Invalid username or password");
            }
        }
    }

    public function getusers()
    {
        $users = User::get();
        if ($users != null){
            return array("responsecode" => 1, "responsedescription" => "Success", "users" => $users);
        }else{
            return array("responsecode" => 0, "responsedescription" => "Error", "message" => "Users not found");
        }

    }

    public function getsubusers()
    {
        $users = SubUser::get();
        if ($users != null){
            return array("responsecode" => 1, "responsedescription" => "Success", "users" => $users);
        }else{
            return array("responsecode" => 0, "responsedescription" => "Error", "message" => "Users not found");
        }

    }

    public function deliveryinfos($username, $password, $from, $to)
    {
        $infos = array();
        $from = $from. ' 00:00:00';
        $to = $to. ' 23:59:59';

        if ($username != null && $password != null){
            $user = User::where('username', $username)
                ->where('password', $password)
                ->first();

            if ($user != null){

                $smsarr = MessageAPI::where('user_id', $user->id)
                    ->whereBetween('created_at', [$from, $to])
                    ->latest()
                    ->get();

                $nbsent = count($smsarr);
                $info = array(
                    'user' => $user->username,
                    'credit' => $user->credit,
                    'from' => $from,
                    'to' => $to,
                    'sent' => $nbsent,
                    'dlrsentcount' => 0,
                    'delivered' => 0,
                    'dlvr_rate' => "0%",
                    'undelivered' => 0,
                    'undlvr_rate' => "0%",
                );
                foreach ($smsarr as $sms){
                    if ($sms->dlrstatus == 'DELIVRD'){
                        $info['delivered'] =  $info['delivered'] + 1;
                    }else if($sms->dlrstatus == 'Undelivered'){
                        $info['undelivered'] =  $info['undelivered'] + 1;
                    }
                    if ($sms->dlrsent == 1){
                        $info['dlrsentcount'] =  $info['dlrsentcount'] + 1;
                    }
                }

                $info['dlvr_rate'] =  $nbsent==0?'0%':round(($info['delivered'] * 100)/ $nbsent, 2).'%';
                $info['undlvr_rate'] =  $nbsent==0?'0%':round(($info['undelivered'] * 100)/ $nbsent, 2).'%';
                array_push($infos, $info);
                return array("responsecode" => 1, "responsedescription" => "Success", "infos" => $infos);

            }else{
                $user = SubUser::where('username', $username)
                    ->where('password', $password)
                    ->first();
                if ($user != null){

                    if ($user->is_admin == 1 ){
                        $smsarr = MessageAPI::where('master_subuser_id', $user->id)
                            ->whereBetween('created_at', [$from, $to])
                            ->latest()
                            ->get();
                    }else{
                        $smsarr = MessageAPI::where('sub_user_id', $user->id)
                            ->whereBetween('created_at', [$from, $to])
                            ->latest()
                            ->get();
                    }

                    $nbsent = count($smsarr);
                    $info = array(
                        'user' => $user->username,
                        'credit' => $user->credit,
                        'from' => $from,
                        'to' => $to,
                        'sent' => $nbsent,
                        'dlrsentcount' => 0,
                        'delivered' => 0,
                        'dlvr_rate' => "0%",
                        'undelivered' => 0,
                        'undlvr_rate' => "0%",
                    );
                    foreach ($smsarr as $sms){
                        if ($sms->dlrstatus == 'DELIVRD'){
                            $info['delivered'] =  $info['delivered'] + 1;
                        }else if($sms->dlrstatus == 'Undelivered'){
                            $info['undelivered'] =  $info['undelivered'] + 1;
                        }
                        if ($sms->dlrsent == 1){
                            $info['dlrsentcount'] =  $info['dlrsentcount'] + 1;
                        }
                    }

                    $info['dlvr_rate'] =  $nbsent==0?'0%':round(($info['delivered'] * 100)/ $nbsent, 2).'%';
                    $info['undlvr_rate'] =  $nbsent==0?'0%':round(($info['undelivered'] * 100)/ $nbsent, 2).'%';
                    array_push($infos, $info);
                    return array("responsecode" => 1, "responsedescription" => "Success", "infos" => $infos);

                }else {
                    return array("responsecode" => 0, "responsedescription" => "Error", "message" => "Invalid username or password");
                }
            }

        }else{
            $users = User::all();
            foreach ($users as $user){
                $smsarr = MessageAPI::where('user_id', $user->id)
                    ->whereBetween('created_at', [$from, $to])
                    ->latest()
                    ->get();

                $nbsent = count($smsarr);
                $info = array(
                    'user' => $user->username,
                    'credit' => $user->credit,
                    'from' => $from,
                    'to' => $to,
                    'sent' => $nbsent,
                    'dlrsentcount' => 0,
                    'delivered' => 0,
                    'dlvr_rate' => "0%",
                    'undelivered' => 0,
                    'undlvr_rate' => "0%"
                );
                foreach ($smsarr as $sms){
                    if ($sms->dlrstatus == 'DELIVRD'){
                        $info['delivered'] =  $info['delivered'] + 1;
                    }else if($sms->dlrstatus == 'Undelivered'){
                        $info['undelivered'] =  $info['undelivered'] + 1;
                    }
                    if ($sms->dlrsent == 1){
                        $info['dlrsentcount'] =  $info['dlrsentcount'] + 1;
                    }
                }

                $info['dlvr_rate'] =  $nbsent==0?'0%':round(($info['delivered'] * 100)/ $info['sent'], 2).'%';
                $info['undlvr_rate'] =  $nbsent==0?'0%':round(($info['undelivered'] * 100)/ $info['sent'], 2).'%';
                array_push($infos, $info);
            }
            return array("responsecode" => 1, "responsedescription" => "Success", "infos" => $infos);
        }
    }

    public function updatesettings($dlrservicestatus,$sendailyrepport,$phone_regex,$sms_size, $admin_user_id){
        try {
            $settings = Settings::first();
            if ($dlrservicestatus != null)
                $settings->dlrservicestatus = $dlrservicestatus;
            if ($sendailyrepport != null)
                $settings->sendailyrepport = $sendailyrepport;
            if ($phone_regex != null)
                $settings->phone_regex = $phone_regex;
            if ($sms_size != null)
                $settings->sms_size = $sms_size;
            if ($admin_user_id != null)
                $settings->admin_user_id = $admin_user_id;
            $settings->save();
            return array("responsecode" => 1, "responsedescription" => "success", "message" => "settings updated successfully");
        }catch (\Exception $e){
            return array("responsecode" => 0, "responsedescription" => "error", "message" => $e->getMessage());
        }
    }

    public function checkuser($username, $password)
    {
        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        if ($user == null){
            $user = User::where('username', $username)
                ->where('password', $password)
                ->first();
        }

        if ($user != null){
            return array("responsecode" => 1, "responsedescription" => "success", "user" => $user);
        }else{
            return array("responsecode" => 0, "responsedescription" => "error", "message" => "user not found");
        }
    }


    public function subusers($username, $password)
    {
        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        if ($user != null){
            $subusers = SubUser::where('master_subuser_id', $user->id)
                ->where('id', '<>', $user->id)
                ->get();
            return array("responsecode" => 1, "responsedescription" => "Success", "subusers" => $subusers);
        }else{
            return array("responsecode" => 0, "responsedescription" => "Error", "message_fr" => "Login ou mot de passe invalide","message_en" => "Invalid username or password");
        }

    }



    public function deletesubuser($username, $password, $subuserid)
    {
        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        $subuser = SubUser::find($subuserid);

        if ($user != null && $subuser != null && $subuser->master_subuser_id = $user->id){
            $subuser->delete();
            return array("responsecode" => 1,"responsedescription" => "error","errcode" => 200,"message_fr" => "Compte supprimé","message_en" => "User deleted");
        }else{
            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 100,"message_fr" => "Utilisateur invalide","message_en" => "Invalid Sub user");
        }

    }

    public function getsubuser($username, $password, $subuserid)
    {
        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        $subuser = SubUser::find($subuserid);

        if ($user != null && $subuser != null && $subuser->master_subuser_id = $user->id){
            return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,"subuser" => $subuser);
        }else{
            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 100,"message_fr" => "Utilisateur Invalide","message_en" => "Invalid Sub user");
        }
    }

    public function updatesubuser($admin_user, $admin_password, $subuserid, $username,
                                  $password, $phone, $senderid, $company)
    {
        try {
                $master = SubUser::where('username', $admin_user)
                    ->where('password', $admin_password)
                    ->first();
                if ($master != null) {
                    $subuser = SubUser::find($subuserid);
                    if ($subuser != null){
                        if ($subuser->master_subuser_id == $master->id) {
                            $subuser->username = $username;
                            $subuser->password = $password;
                            $subuser->phone = $phone;
                            $subuser->senderid = $senderid;
                            $subuser->company = $company;
                            $subuser->save();
                            return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,"message_fr" => "Compte modifié","message_en" => "Account updated");
                        }else {
                            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 100,"message_fr" => "Utilisateur Invalide","message_en" => "Invalid Sub user");
                        }
                    }else{
                        return array("responsecode" => 0,"responsedescription" => "error","errcode" => 102,"message_fr" => "Sous compte introuvable","message_en" => "Account not found");
                    }
                }else {
                    return array("responsecode" => 0, "responsedescription" => "error", "errcode" => 101,"message_fr" => "Compte introuvable","message_en" => "Invalid Username or password");
                }
        }catch (\Exception $e){
            return array("responsecode" => 0, "responsedescription" => "error", "errcode" => -100,"message_fr" => $e->getMessage(),"message_en" => $e->getMessage());
        }
    }

    public function reportsubuser($user, $password, $from, $to)
    {
        $subuser = SubUser::where('username', $user)
            ->where('password', $password)
            ->first();
        $smsarr = array();
        $from = $from.' 00:00:00';
        $to = $to.' 23:59:59';
        if ($subuser != null){
            if ($subuser->is_admin == 1){
                $messages = DB::table('api_messages')
                    ->select((DB::raw('messageid,message,senderid,mobilno,created_at,dlrstatus,deliverytime')))
                    ->whereBetween('created_at', [$from,$to])
                    ->where('master_subuser_id', '=',$subuser->id)
                    ->latest()->get();
            }else{
                $messages = DB::table('api_messages')
                    ->select((DB::raw('messageid,message,senderid,mobilno,created_at,dlrstatus,deliverytime')))
                    ->whereBetween('created_at', [$from,$to])
                    ->where('sub_user_id', '=',$subuser->id)
                    ->latest()->get();
            }

            for ($i=0; $i<sizeof($messages); $i++){
                $smselt = array();
                $smselt['id'] = $messages[$i]->messageid;
                $smselt['message'] = $messages[$i]->message;
                $smselt['sender'] = $messages[$i]->senderid;
                $smselt['number'] = $messages[$i]->mobilno;
                $smselt['status'] = $messages[$i]->dlrstatus=='DELIVRD'?'DELIVERED':'UNDELIVERED';
                $smselt['senttime'] = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($messages[$i]->created_at)));;
                $smselt['deliverytime'] = $messages[$i]->deliverytime;
                array_push($smsarr,$smselt);
            }
            return  array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,"messages" => $smsarr);
        }else{
            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 102,"message_fr" => "Sous compte introuvable","message_en" => "Account not found");
        }
    }

    public function reportuser($user, $password, $subusername, $from, $to)
    {
        $user = SubUser::where('username', $user)
            ->where('password', $password)
            ->first();
        $smsarr = array();
        $from = $from.' 00:00:00';
        $to = $to.' 23:59:59';
        if ($user != null){
            if ($user->is_admin == 1){
                $subuser = SubUser::where('username', $subusername)->first();
                if ($subuser != null){
                    $messages = DB::table('api_messages')
                        ->select((DB::raw('messageid,message,senderid,mobilno,created_at,dlrstatus,deliverytime')))
                        ->whereBetween('created_at', [$from,$to])
                        ->where('master_subuser_id', '=',$user->id)
                        ->where('sub_user_id', '=',$subuser->id)
                        ->latest()->get();
                    for ($i=0; $i<sizeof($messages); $i++){
                        $smselt = array();
                        $smselt['id'] = $messages[$i]->messageid;
                        $smselt['message'] = $messages[$i]->message;
                        $smselt['sender'] = $messages[$i]->senderid;
                        $smselt['number'] = $messages[$i]->mobilno;
                        $smselt['status'] = $messages[$i]->dlrstatus=='DELIVRD'?'DELIVERED':'UNDELIVERED';
                        $smselt['senttime'] = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($messages[$i]->created_at)));;
                        $smselt['deliverytime'] = $messages[$i]->deliverytime;
                        array_push($smsarr,$smselt);
                    }
                    return  array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,"messages" => $smsarr);
                }else{
                    //subuser name invalid
                    return array("responsecode" => 0,"responsedescription" => "error","errcode" => 100,"message_fr" => "Sous compte introuvable","message_en" => "Sub-Account not found");
                }
            }else{
                //user or password invalid
                return array("responsecode" => 0,"responsedescription" => "error","errcode" => 101,"message_fr" => "Role Administrateur requis","message_en" => "Administrator role required");
            }
        }else{
            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 102,"message_fr" => "Login ou mot de passe invalide","message_en" => "Invalid username or password");
        }
    }


    public function rechargeteleossuser($username, $credit, $validity)
    {
        $settings = Settings::first();
        $admin = User::find($settings->admin_user_id);
        $result = $this->telerecharge($admin,$username,$validity,$credit);
        $error =  $result->error;
        if ($error == ""){
            $pinbalance = (int)$result->pinbalance;
            $balance = (int)$result->balance;
            $subuser = User::where('username',$username)->first();
            if ($subuser->phone != null) {
                $message_subuser = str_replace(array('$CREDIT','$BALANCE'),array($pinbalance,$balance),$settings->recharge_message_user);
                $this->sendsms($admin->username, $admin->password, $subuser->phone, $message_subuser, $admin->senderid, null);
            }
            $message_admin = str_replace(array('$USER','$CREDIT','$BALANCE'),array($subuser->username,$pinbalance,$balance),$settings->recharge_message_admin);
            $this->sendsms($admin->username, $admin->password, $admin->phone, $message_admin, $admin->senderid, null);
        }
        return $result;
    }

    public function notify($username, $password, $notify)
    {
        $user = SubUser::where('username',$username)
            ->where('password',$password)
            ->first();
        if ($user != null){
            $user->notification_enable = $notify;
            $user->save();
            return array("responsecode" => 1, "responsedescription" => "success");
        }else{
            return array("responsecode" => 0, "responsedescription" => "error", "message_fr" => "Paramètres de connexion invalide", "message_en" => "Invalid username or password");
        }
    }
}
