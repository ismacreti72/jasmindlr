<?php
/**
 * Created by PhpStorm.
 * User: eddy
 * Date: 13/12/18
 * Time: 10:47
 */

namespace App\Library\Services;

use App\Library\Services\Contracts\SMSAPIServiceInterface;
use App\Models\ApiConfigs\Providers;
use App\Models\ApiConfigs\SendingService;
use App\Models\ApiConfigs\SendingTraffic;
use App\Models\ApiConfigs\Settings;
use App\Models\ApiConfigs\Traffic;
use App\Models\Sms\MessageAPI;
use App\Models\Sms\Recharge;
use App\Models\User\SubUser;
use App\Models\User\User;
use Illuminate\Support\Facades\DB;

class SMSAPIService extends CoreSMSAPI implements SMSAPIServiceInterface
{


    public function sendbulksms($username, $password, $smsarr, $scheduletime)
    {
        $user = SubUser::where('username', $username)->where('password', $password)->first();
        $settings = Settings::first();
        $smsresultarr = array();
        for ($i = 0; $i < sizeof($smsarr); $i++) {
            $total_sms_unit = ceil(strlen($smsarr[$i]['message'])/$settings->sms_size);
            $smscredit = $this->balancecheck($username, $password);
            $smselt = array();
            if ($smscredit["credit"] >= $total_sms_unit) {

                if ($this->isBlockedSender($smsarr[$i]['senderid'])) {
                    $smselt['status'] = 'error';
                    $smselt['smsclientid'] = null;
                    $smselt['messageid'] = null;
                    $smselt['mobileno'] = $this->getNumber($smsarr[$i]['mobileno']);
                    $smselt['errorcode'] = -10018;
                    $smselt['errordescription'] = "Unauthorized senderid";
                    $smselt['senderid'] = $smsarr[$i]['senderid'];
                    $smselt['idsmsskysoft'] = $smsarr[$i]['idsmsskysoft'];
                    $smselt['message'] = $smsarr[$i]['message'];
                }else {

                    if (array_key_exists("campaignid",$smsarr[$i])){
                        //$core = new CoreSMSAPI();
                        /*if ($core->checkIfExist($smsarr[$i]['idsmsskysoft'], $smsarr[$i]['campaignid'])){
                            $smselt['status'] = 'error';
                            $smselt['smsclientid'] = null;
                            $smselt['messageid'] = null;
                            $smselt['mobileno'] = $this->getNumber($smsarr[$i]['mobileno']);
                            $smselt['errorcode'] = "NXH401";
                            $smselt['errordescription'] = "Message Already exist";
                            $smselt['senderid'] = $smsarr[$i]['senderid'];
                            $smselt['idsmsskysoft'] = $smsarr[$i]['idsmsskysoft'];
                            $smselt['message'] = $smsarr[$i]['message'];
                        }else{*/
                            $sending_traffic_id = $user->sending_traffic_id;
                            $sending_traffic = SendingTraffic::find($sending_traffic_id);
                            $sendingservice = SendingService::find($sending_traffic->sending_service_id);
                            $service_name = $sendingservice->name;

                            $traffic_id = $sending_traffic->traffic_id;
                            $traffic = Traffic::find($traffic_id);

                            if ($scheduletime == null) {
                                //send request to traffic url and get response
                                $smselt = json_decode($this->sendRequest(null, $traffic->url != null ? $traffic : $this->checktraffic($smsarr[$i]['mobileno']),
                                    $username, $password, $smsarr[$i]['mobileno'], $smsarr[$i]['message'],
                                    $smsarr[$i]['senderid'], $service_name, $smsarr[$i]['idsmsskysoft'], $smsarr[$i]['campaignid']));

                                $smselt->senderid = $smsarr[$i]['senderid'];
                                $smselt->idsmsskysoft = $smsarr[$i]['idsmsskysoft'];
                                $smselt->message = $smsarr[$i]['message'];
                            } else {
                                //Save schedule message
                                $total_sms_unit = $this->debitcredit($user, $smsarr[$i]['message'], $settings);
                                $smselt = $this->saveScheduleMessage($username, $password, User::find($traffic->account_id), $user, $smsarr[$i]['mobileno'], $smsarr[$i]['senderid'], $smsarr[$i]['message'], $total_sms_unit, $scheduletime, $smsarr[$i]['idsmsskysoft'], $smsarr[$i]['campaignid']);
                                $smselt['senderid'] = $smsarr[$i]['senderid'];
                                $smselt['idsmsskysoft'] = $smsarr[$i]['idsmsskysoft'];
                                $smselt['message'] = $smsarr[$i]['message'];
                                $smselt['scheduletime'] = $scheduletime;
                            }
                        //}
                    }else {

                        $sending_traffic_id = $user->sending_traffic_id;
                        $sending_traffic = SendingTraffic::find($sending_traffic_id);
                        $sendingservice = SendingService::find($sending_traffic->sending_service_id);
                        $service_name = $sendingservice->name;

                        $traffic_id = $sending_traffic->traffic_id;
                        $traffic = Traffic::find($traffic_id);

                        if ($scheduletime == null) {
                            //send request to traffic url and get response
                            $smselt = json_decode($this->sendRequest(null, $traffic->url != null ? $traffic : $this->checktraffic($smsarr[$i]['mobileno']),
                                $username, $password, $smsarr[$i]['mobileno'], $smsarr[$i]['message'],
                                $smsarr[$i]['senderid'], $service_name, $smsarr[$i]['idsmsskysoft'],  null));

                            $smselt->senderid = $smsarr[$i]['senderid'];
                            $smselt->idsmsskysoft = $smsarr[$i]['idsmsskysoft'];
                            $smselt->message = $smsarr[$i]['message'];
                        } else {
                            //Save schedule message
                            $total_sms_unit = $this->debitcredit($user, $smsarr[$i]['message'], $settings);
                            $smselt = $this->saveScheduleMessage($username, $password, User::find($traffic->account_id), $user, $smsarr[$i]['mobileno'], $smsarr[$i]['senderid'], $smsarr[$i]['message'], $total_sms_unit, $scheduletime, $smsarr[$i]['idsmsskysoft'],  null);
                            $smselt['senderid'] = $smsarr[$i]['senderid'];
                            $smselt['idsmsskysoft'] = $smsarr[$i]['idsmsskysoft'];
                            $smselt['message'] = $smsarr[$i]['message'];
                            $smselt['scheduletime'] = $scheduletime;
                        }
                    }
                }

            } else {
                $smselt['status'] = 'error';
                $smselt['smsclientid'] = null;
                $smselt['messageid'] = null;
                $smselt['mobileno'] = $this->getNumber($smsarr[$i]['mobileno']);
                $smselt['errorcode'] = -10008;
                $smselt['errordescription'] = "Balance not enough";
                $smselt['senderid'] = $smsarr[$i]['senderid'];
                $smselt['idsmsskysoft'] = $smsarr[$i]['idsmsskysoft'];
                $smselt['message'] = $smsarr[$i]['message'];
            }
            $smsresultarr[$i] = $smselt;
        }
        return array("user" => $username, "password" => $password, "responsecode" => 1, "responsedescription" => "success", "responsemessage" => "success", "sms" => $smsresultarr);

    }

    /*public function sendbulksms($username, $password, $smsarr, $scheduletime, $stackid, $campaignid)
    {
        $user = SubUser::where('username', $username)->where('password', $password)->first();
        $settings = Settings::first();
        $smsresultarr = array();
        for ($i = 0; $i < sizeof($smsarr); $i++) {
            $total_sms_unit = ceil(strlen($smsarr[$i]['message'])/$settings->sms_size);
            $smscredit = $this->balancecheck($username, $password);
            $smselt = array();
            if ($smscredit["credit"] >= $total_sms_unit) {

                if ($this->isBlockedSender($smsarr[$i]['senderid'])) {
                    $smselt['status'] = 'error';
                    $smselt['smsclientid'] = null;
                    $smselt['messageid'] = null;
                    $smselt['mobileno'] = $this->getNumber($smsarr[$i]['mobileno']);
                    $smselt['errorcode'] = -10018;
                    $smselt['errordescription'] = "Unauthorized senderid";
                    $smselt['senderid'] = $smsarr[$i]['senderid'];
                    $smselt['idsmsskysoft'] = $smsarr[$i]['idsmsskysoft'];
                    $smselt['message'] = $smsarr[$i]['message'];
                }else {
                    $sending_traffic_id = $user->sending_traffic_id;
                    $sending_traffic = SendingTraffic::find($sending_traffic_id);
                    $sendingservice = SendingService::find($sending_traffic->sending_service_id);
                    $service_name = $sendingservice->name;

                    $traffic_id = $sending_traffic->traffic_id;
                    $traffic = Traffic::find($traffic_id);

                    if ($scheduletime == null) {
                        //send request to traffic url and get response
                        $smselt = json_decode($this->sendRequest(null, $traffic->url != null ? $traffic : $this->checktraffic($smsarr[$i]['mobileno']),
                            $username, $password, $smsarr[$i]['mobileno'], $smsarr[$i]['message'],
                            $smsarr[$i]['senderid'], $service_name, $stackid, $campaignid));

                        $smselt->senderid = $smsarr[$i]['senderid'];
                        $smselt->idsmsskysoft = $smsarr[$i]['idsmsskysoft'];
                        $smselt->message = $smsarr[$i]['message'];
                    } else {
                        //Save schedule message
                        $total_sms_unit = $this->debitcredit($user, $smsarr[$i]['message'], $settings);
                        $smselt = $this->saveScheduleMessage($username, $password, User::find($traffic->account_id), $user, $smsarr[$i]['mobileno'], $smsarr[$i]['senderid'], $smsarr[$i]['message'], $total_sms_unit, $scheduletime, $stackid, $campaignid);
                        $smselt['senderid'] = $smsarr[$i]['senderid'];
                        $smselt['idsmsskysoft'] = $smsarr[$i]['idsmsskysoft'];
                        $smselt['message'] = $smsarr[$i]['message'];
                        $smselt['scheduletime'] = $scheduletime;
                    }
                }

            } else {
                $smselt['status'] = 'error';
                $smselt['smsclientid'] = null;
                $smselt['messageid'] = null;
                $smselt['mobileno'] = $this->getNumber($smsarr[$i]['mobileno']);
                $smselt['errorcode'] = -10008;
                $smselt['errordescription'] = "Balance not enough";
                $smselt['senderid'] = $smsarr[$i]['senderid'];
                $smselt['idsmsskysoft'] = $smsarr[$i]['idsmsskysoft'];
                $smselt['message'] = $smsarr[$i]['message'];
            }
            $smsresultarr[$i] = $smselt;
        }
        return array("user" => $username, "password" => $password, "responsecode" => 1, "responsedescription" => "success", "responsemessage" => "success", "sms" => $smsresultarr);

    }*/

    public function userorsubuser($username,$password){
        $user = SubUser::where('username', $username)->where('password', $password)->first();
        if ($user == null){
            $user =User::where('username', $username)->where('password', $password)->first();
        }
        return $user;
    }


    public function sendsms($username, $password, $numbers, $message, $sender, $scheduletime, $stackid, $campaignid){
        $numbers = str_replace(" ", "", $numbers);
        $mobilnos = explode(',', $numbers);
        $user  = SubUser::where('username', $username)->where('password', $password)->first();
        $core = new CoreSMSAPI();

        $settings = Settings::first();
        $smsarr = array();
        for ($i=0; $i<sizeof($mobilnos); $i++) {
            if ($core->checktraffic(str_replace("+","",$mobilnos[$i])) != null) {
                $smscredit = $this->balancecheck($username, $password);
                $total_sms_unit = ceil(strlen($message) / $settings->sms_size);
                $smselt = array();
                if ($smscredit["credit"] >= $total_sms_unit) {
                    $sending_traffic_id = $user->sending_traffic_id;
                    $sending_traffic = SendingTraffic::find($sending_traffic_id);
                    $sendingservice = SendingService::find($sending_traffic->sending_service_id);
                    $service_name = $sendingservice->name;

                    $traffic_id = $sending_traffic->traffic_id;
                    $traffic = Traffic::find($traffic_id);

                    if ($scheduletime == null) {
                        //send request to traffic url and get response
                        $smselt = json_decode($this->sendRequest(null, $traffic->url != null ? $traffic : $this->checktraffic($mobilnos[$i]), $username, $password, $mobilnos[$i], $message, $sender, $service_name, $stackid, $campaignid));
                    } else {
                        //Save schedule message
                        $total_sms_unit = $this->debitcredit($user, $message, $settings);
                        $smselt = $this->saveScheduleMessage($username, $password, User::find($traffic->account_id), $user, $mobilnos[$i], $sender, $message, $total_sms_unit, $scheduletime, $stackid, $campaignid);
                    }
                } else {
                    $smselt['status'] = 'error';
                    $smselt['smsclientid'] = null;
                    $smselt['messageid'] = null;
                    $smselt['mobileno'] = $this->getNumber($mobilnos[$i]);
                    $smselt['errorcode'] = -10008;
                    $smselt['errordescription'] = "Balance not enough";
                }
            }else{
                $smselt['status'] = 'error';
                $smselt['smsclientid'] = null;
                $smselt['messageid'] = null;
                $smselt['mobileno'] = $mobilnos[$i];
                $smselt['errorcode'] = 402;
                $smselt['errordescription'] = "Invalid mobile number";
            }
            $smsarr[$i] = $smselt;
        }
        return array("responsecode"  => 1, "responsedescription"  => "success" , "responsemessage"  => "success", "sms"  => $smsarr);
    }

    public function getNumber($mobilno){
        $provider = $this->getProvider(str_replace('+','',$mobilno));
        $number = (substr( $mobilno, 0, 3 ) == $provider->prefix)?'+'.$mobilno:'+'.$provider->prefix.$mobilno;
        return $number;
    }


    public function saveScheduleMessage($username, $password, $admin,$user, $mobilno, $senderid,
                                        $message,$total_sms_unit,$scheduletime, $stackid, $campaignid)
    {
        try {
            $provider = $this->getProvider(str_replace('+237', '', $mobilno));
            $uuid_sms_id = $campaignid != null ? ("nxh" . "-" . $stackid . '-' . $campaignid) : $stackid;
            $number = (substr($mobilno, 0, 3) == $provider->prefix) ? '+' . $mobilno : '+' . $provider->prefix . $mobilno;
            $message = new MessageAPI([
                'mobilno' => $number,
                'user_id' => $admin->id,
                'sub_user_id' => $user->id,
                'master_subuser_id' => $user->master_subuser_id,
                'username' => $username,
                'password' => $password,
                'scheduletime' => $scheduletime,
                'senderid' => $senderid,
                'message' => $message,
                'stackid' => $stackid,
                'campaignid' => $campaignid,
                'uuid_sms_id' => $uuid_sms_id,
                'provider_id' => $provider->id,
                'total_sms_unit' => $total_sms_unit,
                'dlrstatus' => '',
                'responsecode' => 3,
                'responsedescription' => 'Waiting scheduletime'
            ]);
            $message->save();
            $message->messageid = md5($message->id);
            $message->save();

            $smselt = array();
            $smselt['status'] = 'success';
            $smselt['smsclientid'] = null;
            $smselt['messageid'] = $message->messageid;
            $smselt['mobileno'] = $number;
            $smselt['errorcode'] = null;
            $smselt['errordescription'] = null;
            return $smselt;
        }catch (\Exception $exception){
            $smselt = array();
            $smselt['status'] = 'error';
            $smselt['smsclientid'] = null;
            $smselt['messageid'] = null;
            $smselt['mobileno'] = $number;
            $smselt['errorcode'] = "NXH500";
            $smselt['errordescription'] = "Message already exist";
        }
    }

    public function balancecheck($username, $password)
    {
        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        if ($user != null){
            return array(
                "error" => null,
                "accountexpdate"  => $user->accountexpdate,
                "balanceexpdate"  => $user->balanceexpdate,
                "credit"  => $user->credit
            );
        }else{
            return array("error" => "Invalid username or password");
        }
    }


    public function rechargesubuser($username, $password, $subuserid, $credit)
    {
        $settings = Settings::first();

        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        $subuser = SubUser::find($subuserid);

        if ($user != null && $subuser != null && $subuser->master_subuser_id = $user->id){
            $recharge = $subuser->credit + $credit;
            if (($user->credit >= $credit && $recharge >= 0) || ($user->credit == 0 && $credit < 0 && $recharge >= 0)) {
                try {
                    $recharge = new Recharge([
                        'master_subuser_id' => $user->id,
                        'sub_user_id' => $subuser->id,
                        'credit' => $credit,
                        'oldcredit' => $subuser->credit,
                        'newcredit' => $subuser->credit + $credit
                    ]);
                    $recharge->save();

                    $oldCredit = $subuser->credit;
                    $newCredit = $oldCredit + $credit;
                    $subuser->credit = $newCredit;
                    $subuser->balance = $newCredit;
                    $subuser->credit_rate = null;
                    $subuser->save();

                    $user->credit = $user->credit - $credit;
                    $user->save();

                    if ($subuser->phone != null && $user->phone != null) {
                        if ($credit > 0){

                            /*
                            * Cher Client, votre compte a été crédité de $CREDIT SMS. Votre nouveau solde est de $BALANCE SMS.
                            * Nous vous remercions pour votre confiance.
                            */

                            if ($user->notification_enable == 1) {
                                $message_user = str_replace(array('$USER','$CREDIT','$BALANCE'),array($subuser->username,$credit,$subuser->credit),$user->id=4?$settings->recharge_message_user:$settings->recharge_message_subuser);
                                $this->sendsms($user->username, $user->password, $subuser->phone, $message_user, $user->company_sender,null, null, null);
                            }
                            return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,"message_fr" => "Compte crédité","message_en" => "Account credited");
                        }else{
                            if ($user->notification_enable == 1) {
                                $message_subuser = str_replace(array('$USER', '$CREDIT', '$BALANCE'), array($subuser->username, abs($credit), $subuser->credit), $settings->debit_message_subuser);
                                $message_user = str_replace(array('$USER', '$CREDIT', '$BALANCE'), array($user->username, abs($credit), $user->credit), $settings->recharge_message_subuser);
                                $this->sendsms($user->username, $user->password, $subuser->phone, $message_subuser, $user->company_sender,null, null, null);
                                $this->sendsms($user->username, $user->password, $user->phone, $message_user, $user->company_sender,null, null, null);
                            }
                            return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,"message_fr" => "Compte débité","message_en" => "Account debited");
                        }
                    }

                } catch (\Exception $e) {
                    return array("responsecode" => 0,"responsedescription" => "error","errcode" => -100,"message_fr" => $e->getMessage(), "message_en" => $e->getMessage());
                }

            }else{
                return array("responsecode" => 0,"responsedescription" => "error","errcode" => 100,"message_fr" => "Solde insuffisant","message_en" => "Balance not enough");
            }

        }else{
            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 101,"message_fr" => "Utilisateur invalide","message_en" => "Invalid Sub user");
        }
    }


    public function recharge($username, $password, $credit)
    {
        $settings = Settings::first();
        $admin = User::find($settings ->admin_subuser_id);

        $subuser = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();

        if ($subuser != null){
            try {

                $recharge = new Recharge([
                    'master_subuser_id' => $subuser->master_subuser_id,
                    'sub_user_id' => $subuser->id,
                    'credit' => $credit,
                    'oldcredit' => $subuser->credit,
                    'newcredit' => $subuser->credit + $credit
                ]);
                $recharge->save();

                $oldCredit = $subuser->credit;
                $newCredit = $oldCredit + $credit;
                $subuser->credit = $newCredit;
                $subuser->balance = $newCredit;
                $subuser->save();

                if ($subuser->phone != null) {
                    $message_subuser = str_replace(array('$CREDIT','$BALANCE'),array($credit,$subuser->credit),$settings->recharge_message_user);
                    $this->sendsms($admin->username, $admin->password, $subuser->phone, $message_subuser, $admin->senderid, null, null, null);
                }
                /*$message_admin = str_replace(array('$USER','$CREDIT','$BALANCE'),array($subuser->username,$credit,$subuser->credit),$settings->recharge_message_admin);
                $this->sendsms($admin->username, $admin->password, $admin->phone, $message_admin, $admin->senderid, null, null, null);*/

                return array("status" => "Account credited");
            }catch (\Exception $e){
                return array("error" => $e->getMessage());
            }
        }else {
            return array("error" => "Invalid username or password");
        }

    }


    public function getDLRLocal($username, $password, $messageid)
    {

        $result = array();
        $user = SubUser::where('username', $username)->where('password', $password)->first();
        if ($user != null){
            $message = MessageAPI::where('messageid',$messageid)
                ->where('sub_user_id',$user->id)
                ->first();
            if ($message != null){
                $provider = Providers::find($message->provider_id);
                $result["reponsecode"] = $message->responsecode;
                $result["reponsedescription"] = $message->responsedescription;
                $result["mobileno"] = $message->mobilno;
                $result["messageid"] = $message->messageid;
                $result["provider"] = $provider!=null?$provider->name:'Invalid Number';
                $result["sms_unit"] = $message->total_sms_unit;
                $result["submittime"] = $message->submittime;
                $result["senttime"] = $message->senttime;
                $result["deliverytime"] = $message->deliverytime;
                $result["status"] = $message->dlrstatus;
            }else{
                $result["reponsecode"] = 0;
                $result["reponsedescription"] = "Message not found";
            }
        }else{
            $result["reponsecode"] = 0;
            $result["reponsedescription"] = "Invalid Username or Password";
        }

        return $result;
    }


    public function createapiuser($username, $password, $phone,
                                  $credit, $has_dlr_url, $dlr_url,
                                  $batch_dlr_url,$accountexpdate,
                                  $balanceexpdate, $company,$sending_traffic_id,$senderid)
    {
        try {
            $user = new User([
                'username' => $username,
                'password' => $password,
                'phone' => $phone,
                'credit' => $credit,
                'company' => $company,
                'sending_traffic_id' => $sending_traffic_id,
                'has_dlr_url' => $has_dlr_url,
                'dlr_url' => $dlr_url,
                'batch_dlr_url' => $batch_dlr_url,
                'senderid' => $senderid,
                'accountexpdate' => $accountexpdate,
                'balanceexpdate' => $balanceexpdate
            ]);
            $user->save();
            return array("responsecode" => 1, "responsedescription" => "Success", "message" => "User successfully created");
        }catch (\Exception $e){
            return array("responsecode" => 0, "responsedescription" => "error", "message" => $e->getMessage());
        }
    }


    public function addsubuser($username,$password,$subusername,$subuserpassword, $phone,$senderid,$credit,$company,$isAdmin)
    {

            try {
                $is_valid = $this->mincharacter($subuserpassword);
                if (!$is_valid){
                    return array("responsecode" => 0, "responsedescription" => "error", "message_fr" => "entrer un mot de passe de 8 caractères minimum");
                }
                $user = SubUser::where('username', $subusername)->first();
                $settings = Settings::first();
                if ($user == null) {
                    $submaster = SubUser::where('username', $username)
                        ->where('password', $password)
                        ->first();
                    if ($submaster != null) {
                        if ($submaster->credit >= $credit) {
                            $user = new SubUser([
                                'username' => $subusername,
                                'password' => $subuserpassword,
                                'phone' => $phone,
                                'credit' => $credit,
                                'company' => $company,
                                'is_admin' => $isAdmin,
                                'master_subuser_id' => $submaster->id,
                                'senderid' => $senderid,
                                'accountexpdate' => $submaster->accountexpdate,
                                'balanceexpdate' => $submaster->balanceexpdate
                            ]);
                            $user->save();
                            $submaster->credit = $submaster->credit - $credit;
                            $submaster->save();
                            if ($user->phone != null && $submaster->phone != null) {
                                if ($submaster->notification_enable == 1) {
                                    $message_user = str_replace(array('$USER', '$PWD', '$CREDIT'), array($user->username, $user->password, $user->credit), $settings->create_message_subuser);
                                    $this->sendsms($submaster->username, $submaster->password, $user->phone, $message_user, $submaster->company_sender, null, null, null);
                                }
                            }
                        } else {
                            return array("responsecode" => 0, "responsedescription" => "error", "errcode" => 100, "message_fr" => "Solde insuffisant", "message_en" => "Balance not enough");
                        }
                    } else {
                        return array("responsecode" => 0, "responsedescription" => "error", "errcode" => 102, "message_fr" => "user admin invalide", "message_en" => "Invalid admin user");
                    }
                } else {
                    return array("responsecode" => 0, "responsedescription" => "error", "errcode" => 101, "message_fr" => "Login existant", "message_en" => "User already exist");
                }

                return array("responsecode" => 1, "responsedescription" => "Success", "user" => $user, "message_fr" => "Utilisateur enregistré", "message_en" => "User successfully created");
            } catch (\Exception $e) {
                return array("responsecode" => 0, "responsedescription" => "error", "errcode" => -100, "message_en" => $e->getMessage());
            }
    }


    public function mincharacter($value){
        return strlen(trim($value)) > 7;
    }

    public function updatepassword($username, $password, $newpassword)
    {

        $is_valid = $this->mincharacter($newpassword);
        if (!$is_valid){
            return array("responsecode" => 0, "responsedescription" => "error", "message_fr" => "entrer un mot de passe de 8 caractères minimum");
        }
        $user = $this->userorsubuser($username,$password);
        if ($user==null){
            return array("responsecode" => 0, "responsedescription" => "error", "errcode" => 101, "message_fr" =>"Mot de passe actuel incorrect", "message_en" => "Invalid actual password");
        }

        try{
            $user->password = $newpassword;
            $user->save();
            return array("responsecode" => 1, "responsedescription" => "Success", "message_fr" => "Mot de passe modifier avec succès", "message_en" => "update password succes");
        }catch (\Exception $e){
            return array("responsecode" => 0, "responsedescription" => "error", "errcode" => 100, "message_fr" => $e->getMessage(), "message_en" => $e->getMessage());
        }


    }

    public function getapisms($username, $password, $from, $to)
    {
        if ($username != null && $password != null) {
            $user = SubUser::where('username', $username)
                ->where('password', $password)
                ->first();
            if ($user != null) {
                $smsarr = MessageAPI::where('sub_user_id', $user->id)
                    ->whereBetween('created_at', [$from, $to])
                    ->OrWhere('master_subuser_id', $user->id)
                    ->whereBetween('created_at', [$from, $to])
                    ->latest()
                    ->get();
            }else {
                return array("responsecode" => 0, "responsedescription" => "Error", "message" => "Invalid username or password");
            }
        } else{
            $smsarr = MessageAPI::whereBetween('created_at', [$from, $to])
                ->latest()
                ->get();
        }
        return array("responsecode" => 1, "responsedescription" => "Success", "sms" => $smsarr);
    }

    public function getusers()
    {
        $users = User::get();
        if ($users != null){
            return array("responsecode" => 1, "responsedescription" => "Success", "users" => $users);
        }else{
            return array("responsecode" => 0, "responsedescription" => "Error", "message" => "Users not found");
        }

    }

    public function getsubusers()
    {
        $users = SubUser::get();
        if ($users != null){
            return array("responsecode" => 1, "responsedescription" => "Success", "users" => $users);
        }else{
            return array("responsecode" => 0, "responsedescription" => "Error", "message" => "Users not found");
        }

    }

    public function deliveryinfos($username, $password, $from, $to)
    {
        $from = $from. ' 00:00:00';
        $to = $to. ' 23:59:59';
        $infos = array();

        if ($username == null && $password == null) {
            return $this->getAllReport($from, $to);
        }else{

            $result = $this->getMessageByUser($username, $password, $this->dateToUTC($from), $this->dateToUTC($to));

            $nbsent = count($result['smsarr']);
            $info = array(
                'user' => $result['user']->username,
                'credit' => $result['user']->credit,
                'from' => $from,
                'to' => $to,
                'sent' => $nbsent,
                'dlrsentcount' => 0,
                'delivered' => 0,
                'dlvr_rate' => "0%",
                'undelivered' => 0,
                'undlvr_rate' => "0%",
                'pending' => 0,
                'pending_rate' => "0%"
            );
            foreach ($result['smsarr'] as $sms){
                if ($sms->dlrstatus == 'DELIVRD'){
                    $info['delivered'] =  $info['delivered'] + 1;
                }else if($sms->dlrstatus == 'UNDELIV'){
                    $info['undelivered'] =  $info['undelivered'] + 1;
                }else{
                    $info['pending'] =  $info['pending'] + 1;
                }
                if ($sms->dlrsent == 1){
                    $info['dlrsentcount'] =  $info['dlrsentcount'] + 1;
                }
            }

            $info['dlvr_rate'] =  $nbsent==0?'0%':round(($info['delivered'] * 100)/ $nbsent, 2).'%';
            $info['undlvr_rate'] =  $nbsent==0?'0%':round(($info['undelivered'] * 100)/ $nbsent, 2).'%';
            $info['pending_rate'] =  $nbsent==0?'0%':round(($info['pending'] * 100)/ $nbsent, 2).'%';

            array_push($infos, $info);
            return array("responsecode" => 1, "responsedescription" => "Success", "infos" => $infos);
        }

    }


    public function getMessageByUser($username, $password, $from, $to){

        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        $smsarr = null;
        if ($user != null) {

            $smsarr = MessageAPI::where('master_subuser_id', $user->id)
                ->whereNotNull("messageid")
                ->whereBetween('created_at', [$from, $to])
                ->orWhere('sub_user_id', $user->id)
                ->whereBetween('created_at', [$from, $to])
                ->whereNotNull("messageid")
                ->latest()
                ->get();
        }
        return array('user' => $user, 'smsarr' => $smsarr);
    }


    public function report($from, $to){

        $smsarr = MessageAPI::whereBetween('created_at', [$from, $to])
            ->whereNotNull("messageid")
            ->get();
        $nbsent = count($smsarr);

        $info = array(
            'sent' => $nbsent,
            'delivered' => 0,
            'dlvr_rate' => "0%",
            'undelivered' => 0,
            'undlvr_rate' => "0%",
            'pending' => 0,
            'pending_rate' => "0%"
        );
        foreach ($smsarr as $sms){
            if ($sms->dlrstatus == 'DELIVRD'){
                $info['delivered'] =  $info['delivered'] + 1;
            }else if($sms->dlrstatus == 'UNDELIV'){
                $info['undelivered'] =  $info['undelivered'] + 1;
            }else{
                $info['pending'] =  $info['pending'] + 1;
            }
        }

        $info['dlvr_rate'] =  $nbsent==0?'0%':round(($info['delivered'] * 100)/ $nbsent, 2).'%';
        $info['undlvr_rate'] =  $nbsent==0?'0%':round(($info['undelivered'] * 100)/ $nbsent, 2).'%';
        $info['pending_rate'] =  $nbsent==0?'0%':round(($info['pending'] * 100)/ $nbsent, 2).'%';

        return $info;

    }

    public function getAllReport($from, $to){
        $users = SubUser::get();
        $infos = array();
        $resume = $this->report($this->dateToUTC($from), $this->dateToUTC($to));
        foreach ($users as $user){
            $smsarr = MessageAPI::where('sub_user_id', '=',$user->id)
                ->whereNotNull("messageid")
                ->whereBetween('created_at', [$this->dateToUTC($from),$this->dateToUTC($to)])
                ->latest()
                ->get();
            /*$smsarr = MessageAPI::where('master_subuser_id', '=',$user->id)
                ->whereNotNull("messageid")
                ->whereBetween('created_at', [$this->dateToUTC($from),$this->dateToUTC($to)])
                ->orWhere('sub_user_id', '=',$user->id)
                ->whereNotNull("messageid")
                ->whereBetween('created_at', [$this->dateToUTC($from),$this->dateToUTC($to)])
                ->latest()
                ->get();*/

            $nbsent = count($smsarr);
            if ($nbsent > 0){
                $info = array(
                    'user' => $user->username,
                    'credit' => $user->credit,
                    'sent' => $nbsent,
                    'delivered' => 0,
                    'dlvr_rate' => "0%",
                    'undelivered' => 0,
                    'undlvr_rate' => "0%",
                    'pending' => 0,
                    'pending_rate' => "0%"
                );
                foreach ($smsarr as $sms){
                    if ($sms->dlrstatus == 'DELIVRD'){
                        $info['delivered'] =  $info['delivered'] + 1;
                    }else if($sms->dlrstatus == 'UNDELIV'){
                        $info['undelivered'] =  $info['undelivered'] + 1;
                    }else{
                        $info['pending'] =  $info['pending'] + 1;
                    }
                }

                $info['dlvr_rate'] =  $nbsent==0?'0%':round(($info['delivered'] * 100)/ $info['sent'], 2).'%';
                $info['undlvr_rate'] =  $nbsent==0?'0%':round(($info['undelivered'] * 100)/ $info['sent'], 2).'%';
                $info['pending_rate'] =  $nbsent==0?'0%':round(($info['pending'] * 100)/ $info['sent'], 2).'%';

                array_push($infos, $info);
            }

        }
        return array("responsecode" => 1, "responsedescription" => "Success",
            "from" => $from,"to" => $to,"resume" => $resume,"infos" => $infos);
    }




    public function updatesettings($dlrservicestatus,$sendailyrepport,$phone_regex,$sms_size, $admin_user_id){
        try {
            $settings = Settings::first();
            if ($dlrservicestatus != null)
                $settings->dlrservicestatus = $dlrservicestatus;
            if ($sendailyrepport != null)
                $settings->sendailyrepport = $sendailyrepport;
            if ($phone_regex != null)
                $settings->phone_regex = $phone_regex;
            if ($sms_size != null)
                $settings->sms_size = $sms_size;
            if ($admin_user_id != null)
                $settings->admin_user_id = $admin_user_id;
            $settings->save();
            return array("responsecode" => 1, "responsedescription" => "success", "message" => "settings updated successfully");
        }catch (\Exception $e){
            return array("responsecode" => 0, "responsedescription" => "error", "message" => $e->getMessage());
        }
    }

    public function checkuser($username, $password)
    {
        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        if ($user == null){
            $user = User::where('username', $username)
                ->where('password', $password)
                ->first();
        }

        if ($user != null){
            return array("responsecode" => 1, "responsedescription" => "success", "user" => $user);
        }else{
            return array("responsecode" => 0, "responsedescription" => "error", "message" => "user not found");
        }
    }


    public function subusers($username, $password)
    {
        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        if ($user != null){
            $subusers = SubUser::where('master_subuser_id', $user->id)
                ->where('id', '<>', $user->id)
                ->get();
            return array("responsecode" => 1, "responsedescription" => "Success", "subusers" => $subusers);
        }else{
            return array("responsecode" => 0, "responsedescription" => "Error", "message_fr" => "Login ou mot de passe invalide","message_en" => "Invalid username or password");
        }

    }



    public function deletesubuser($username, $password, $subuserid)
    {
        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        $subuser = SubUser::find($subuserid);

        if ($user != null && $subuser != null && $subuser->master_subuser_id = $user->id){
            $subuser->delete();
            return array("responsecode" => 1,"responsedescription" => "error","errcode" => 200,"message_fr" => "Compte supprimé","message_en" => "User deleted");
        }else{
            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 100,"message_fr" => "Utilisateur invalide","message_en" => "Invalid Sub user");
        }

    }

    public function getsubuser($username, $password, $subuserid)
    {
        $user = SubUser::where('username', $username)
            ->where('password', $password)
            ->first();
        $subuser = SubUser::find($subuserid);

        if ($user != null && $subuser != null && $subuser->master_subuser_id = $user->id){
            return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,"subuser" => $subuser);
        }else{
            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 100,"message_fr" => "Utilisateur Invalide","message_en" => "Invalid Sub user");
        }
    }

    public function updatesubuser($admin_user, $admin_password, $subuserid, $username,
                                  $password, $phone, $senderid, $company)
    {
        try {
            $master = SubUser::where('username', $admin_user)
                ->where('password', $admin_password)
                ->first();
            if ($master != null) {
                $subuser = SubUser::find($subuserid);
                if ($subuser != null){
                    if ($subuser->master_subuser_id == $master->id) {
                        $subuser->username = $username;
                        $subuser->password = $password;
                        $subuser->phone = $phone;
                        $subuser->senderid = $senderid;
                        $subuser->company = $company;
                        $subuser->save();
                        return array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,"message_fr" => "Compte modifié","message_en" => "Account updated");
                    }else {
                        return array("responsecode" => 0,"responsedescription" => "error","errcode" => 100,"message_fr" => "Utilisateur Invalide","message_en" => "Invalid Sub user");
                    }
                }else{
                    return array("responsecode" => 0,"responsedescription" => "error","errcode" => 102,"message_fr" => "Sous compte introuvable","message_en" => "Account not found");
                }
            }else {
                return array("responsecode" => 0, "responsedescription" => "error", "errcode" => 101,"message_fr" => "Compte introuvable","message_en" => "Invalid Username or password");
            }
        }catch (\Exception $e){
            return array("responsecode" => 0, "responsedescription" => "error", "errcode" => -100,"message_fr" => $e->getMessage(),"message_en" => $e->getMessage());
        }
    }

    public function reportsubuser($user, $password, $from, $to)
    {
        $subuser = SubUser::where('username', $user)
            ->where('password', $password)
            ->first();
        $smsarr = array();
        $from = $from.' 00:00:00';
        $to = $to.' 23:59:59';
        if ($subuser != null){
            $messages = DB::table('api_messages')
                ->select((DB::raw('messageid,message,senderid,mobilno,total_sms_unit,provider_id,created_at,dlrstatus,deliverytime')))
                ->where('sub_user_id', '=',$subuser->id)
                ->where('master_subuser_id', '=',$subuser->id)
                ->whereBetween('created_at', [$from,$to])
                ->orWhere('sub_user_id', '=',$subuser->id)
                ->whereBetween('created_at', [$from,$to])
                ->latest()->get();

            for ($i=0; $i<sizeof($messages); $i++){
                $smselt = array();
                $provider = Providers::find($messages[$i]->provider_id);
                $smselt['id'] = $messages[$i]->messageid;
                $smselt['message'] = $messages[$i]->message;
                $smselt['sender'] = $messages[$i]->senderid;
                $smselt['number'] = $messages[$i]->mobilno;
                $smselt['provider'] = $provider!=null?$provider->name:'Invalid Number';
                $smselt['sms_unit'] = $messages[$i]->total_sms_unit;
                $smselt['status'] = $messages[$i]->dlrstatus=='DELIVRD'?'DELIVERED':'UNDELIVERED';
                $smselt['senttime'] = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($messages[$i]->created_at)));;
                $smselt['deliverytime'] = $messages[$i]->deliverytime;
                array_push($smsarr,$smselt);
            }
            return  array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,"messages" => $smsarr);
        }else{
            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 100,"message_fr" => "Sous compte introuvable","message_en" => "Account not found");
        }
    }

    public function reportuser($user, $password, $subusername, $from, $to)
    {
        $user = SubUser::where('username', $user)
            ->where('password', $password)
            ->first();
        $smsarr = array();
        $from = $from.' 00:00:00';
        $to = $to.' 23:59:59';
        if ($user != null){
            if ($user->is_admin == 1){
                $subuser = SubUser::where('username', $subusername)->first();
                if ($subuser != null){
                    $messages = DB::table('api_messages')
                        ->select((DB::raw('messageid,message,senderid,mobilno,created_at,dlrstatus,deliverytime')))
                        ->whereBetween('created_at', [$from,$to])
                        ->where('master_subuser_id', '=',$user->id)
                        ->where('sub_user_id', '=',$subuser->id)
                        ->latest()->get();
                    for ($i=0; $i<sizeof($messages); $i++){
                        $smselt = array();
                        $smselt['id'] = $messages[$i]->messageid;
                        $smselt['message'] = $messages[$i]->message;
                        $smselt['sender'] = $messages[$i]->senderid;
                        $smselt['number'] = $messages[$i]->mobilno;
                        $smselt['status'] = $messages[$i]->dlrstatus=='DELIVRD'?'DELIVERED':'UNDELIVERED';
                        $smselt['senttime'] = date("Y-m-d H:i:s", strtotime('+1 hour', strtotime($messages[$i]->created_at)));;
                        $smselt['deliverytime'] = $messages[$i]->deliverytime;
                        array_push($smsarr,$smselt);
                    }
                    return  array("responsecode" => 1,"responsedescription" => "success","errcode" => 200,"messages" => $smsarr);
                }else{
                    //subuser name invalid
                    return array("responsecode" => 0,"responsedescription" => "error","errcode" => 100,"message_fr" => "Sous compte introuvable","message_en" => "Sub-Account not found");
                }
            }else{
                //user or password invalid
                return array("responsecode" => 0,"responsedescription" => "error","errcode" => 101,"message_fr" => "Role Administrateur requis","message_en" => "Administrator role required");
            }
        }else{
            return array("responsecode" => 0,"responsedescription" => "error","errcode" => 102,"message_fr" => "Login ou mot de passe invalide","message_en" => "Invalid username or password");
        }
    }


    public function rechargeteleossuser($username, $credit, $validity)
    {
        $settings = Settings::first();
        $admin = User::find($settings->admin_user_id);
        $result = $this->telerecharge($admin,$username,$validity,$credit);
        $error =  $result->error;
        if ($error == ""){
            $pinbalance = (int)$result->pinbalance;
            $balance = (int)$result->balance;
            $subuser = User::where('username',$username)->first();
            if ($subuser->phone != null) {
                $message_subuser = str_replace(array('$CREDIT','$BALANCE'),array($pinbalance,$balance),$settings->recharge_message_user);
                $this->sendsms($admin->username, $admin->password, $subuser->phone, $message_subuser, $admin->senderid, null, null, null);
            }
            $message_admin = str_replace(array('$USER','$CREDIT','$BALANCE'),array($subuser->username,$pinbalance,$balance),$settings->recharge_message_admin);
            $this->sendsms($admin->username, $admin->password, $admin->phone, $message_admin, $admin->senderid, null, null, null);
        }
        return $result;
    }

    public function notify($username, $password, $notify)
    {
        $user = SubUser::where('username',$username)
            ->where('password',$password)
            ->first();
        if ($user != null){
            $user->notification_enable = $notify;
            $user->save();
            return array("responsecode" => 1, "responsedescription" => "success");
        }else{
            return array("responsecode" => 0, "responsedescription" => "error", "message_fr" => "Paramètres de connexion invalide", "message_en" => "Invalid username or password");
        }
    }

    public function getsmsstatus($smsids,$numbers, $campaignids)
    {
        $messages = MessageAPI::whereIn('stackid', explode(",",$smsids))
            ->whereIn('mobilno', explode(",",$numbers))
            ->whereIn('campaignid', explode(",",$campaignids))
            ->get();
        if ($messages != null){
            return array("responsecode" => 1, "responsedescription" => "success", "sms" => $messages);
        }else{
            return array("responsecode" => 0, "responsedescription" => "error", "message_fr" => "Message non trouvé", "message_en" => "Message not found");
        }
    }

    public function getOcmBalance()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://smsvas.com/operatorsmsapi/public/index.php/api/v1/ocmsmscredit");
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function listActiveUsers($username, $password, $from, $to)
    {
        $user = SubUser::where("username",$username)
            ->where("password",$password)
            ->first();
        if ($user != null) {
            $from = $from . ' 00:00:00';
            $to = $to . ' 23:59:59';
            $users = SubUser::get();
            $infos = array();
            $resume = $this->report($this->dateToUTC($from), $this->dateToUTC($to));
            foreach ($users as $user) {
                $smsarr = MessageAPI::where('sub_user_id', '=', $user->id)
                    ->where('master_subuser_id', '=', $user->id)
                    ->whereNotNull("messageid")
                    ->whereBetween('created_at', [$this->dateToUTC($from), $this->dateToUTC($to)])
                    ->orWhere('sub_user_id', '=', $user->id)
                    ->whereNotNull("messageid")
                    ->whereBetween('created_at', [$this->dateToUTC($from), $this->dateToUTC($to)])
                    ->latest()
                    ->get();

                $nbsent = count($smsarr);
                if ($nbsent > 0) {
                    $info = array(
                        'user' => $user,
                        'sent' => $nbsent,
                        'delivered' => 0,
                        'dlvr_rate' => "0%",
                        'undelivered' => 0,
                        'undlvr_rate' => "0%",
                        'pending' => 0,
                        'pending_rate' => "0%"
                    );
                    foreach ($smsarr as $sms) {
                        if ($sms->dlrstatus == 'DELIVRD') {
                            $info['delivered'] = $info['delivered'] + 1;
                        } else if ($sms->dlrstatus == 'UNDELIV') {
                            $info['undelivered'] = $info['undelivered'] + 1;
                        } else {
                            $info['pending'] = $info['pending'] + 1;
                        }
                    }

                    $info['dlvr_rate'] = $nbsent == 0 ? '0%' : round(($info['delivered'] * 100) / $info['sent'], 2) . '%';
                    $info['undlvr_rate'] = $nbsent == 0 ? '0%' : round(($info['undelivered'] * 100) / $info['sent'], 2) . '%';
                    $info['pending_rate'] = $nbsent == 0 ? '0%' : round(($info['pending'] * 100) / $info['sent'], 2) . '%';

                    array_push($infos, $info);
                }

            }
            return array("responsecode" => 1, "responsedescription" => "Success",
                "from" => $from, "to" => $to, "resume" => $resume, "infos" => $infos);
        }else{
            return array("responsecode" => 0, "responsedescription" => "error",
                "message" => "Invalid username or password");
        }
    }

    public function listdlr($messageids)
    {
        $smsclientids = explode(",",$messageids);
        array_pop($smsclientids);
        $messages = MessageAPI::whereIn('dlrstatus', array('DELIVRD','UNDELIV'))
            ->whereIn('smsclientid', $smsclientids)
            ->get();

        return $messages;
    }

    public function sendrealsms($data)
    {

        $curl = curl_init();

        $json = json_encode($data);
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://smsvas.com/bulk/public/index.php/api/v1/sendsms",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>$json,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}
