<?php
/**
 * Created by PhpStorm.
 * User: eddy
 * Date: 13/12/18
 * Time: 10:47
 */

namespace App\Library\Services;

use App\Models\ApiConfigs\SendingService;
use App\Models\ApiConfigs\SendingTraffic;
use App\Models\ApiConfigs\Settings;
use App\Models\Sms\MessageAPI;
use App\Models\Sms\Recharge;
use App\Models\User\User;

class SMSAPIService_ extends CoreSMSAPI //implements SMSAPIServiceInterface
{

    public function checkoperator($phone){
        if (preg_match_all('/^(237)?(65[0-4])?(67[0-9])?(680)?[0-9]{6}$/', $phone, $out)) {
            return 'MTN';
        }else{
            return 'ORANGE';
        }
    }


    public function sendbulksms($username, $password, $smsarr)
    {
        $user = User::where('username', $username)->where('password', $password)->first();
        $smsresultarr = array();
        for ($i=0; $i<sizeof($smsarr); $i++) {
            $smscredit = $this->balancecheck($username, $password);
            $smselt = array();
            if ($smscredit["credit"] > 0) {
                if (preg_match_all('/^(237)?[0-9]{9}$/', $smsarr[$i]['mobileno'], $out)) {
                    $operator = $this->checkoperator($smsarr[$i]['mobileno']);
                    $settings = Settings::first();

                    $sending_traffic_id = $settings->sending_traffic_id;
                    $sending_traffic = SendingTraffic::find($sending_traffic_id);
                    $sendingservice = SendingService::find($sending_traffic->sending_service_id);
                    $service_name = $sendingservice->name;

                    if ($sending_traffic_id === 1){
                        $smselt = $this->sendTeleossSMS($user, $username, $password, $smsarr[$i]['mobileno'], $smsarr[$i]['message'], $smsarr[$i]['senderid'], $service_name,1, 0);
                    }else if ($sending_traffic_id === 2){
                        $smselt = $this->sendsmsOrange($user, $username, $password, $smsarr[$i]['mobileno'],  $smsarr[$i]['message'], $smsarr[$i]['senderid'], $service_name,0, 1);
                    }else if ($sending_traffic_id === 3){
                        if ($operator === 'MTN'){
                            $smselt = $this->sendTeleossSMS($user, $username, $password, $smsarr[$i]['mobileno'],  $smsarr[$i]['message'], $smsarr[$i]['senderid'], $service_name,1, 0);
                        }else{
                            $smselt = $this->sendsmsOrange($user, $username, $password, $smsarr[$i]['mobileno'],  $smsarr[$i]['message'], $smsarr[$i]['senderid'], $service_name,0, 1);
                        }
                    }
                }else{
                    $smselt['status'] = 'error';
                    $smselt['smsclientid'] = null;
                    $smselt['messageid'] = null;
                    $smselt['mobileno'] = "+237" . str_replace("237", "", $smsarr[$i]['mobileno']);
                    $smselt['errorcode'] = -10003;
                    $smselt['errordescription'] = "Invalid mobile number";
                }
            }else{
                $smselt['status'] = 'error';
                $smselt['smsclientid'] = null;
                $smselt['messageid'] = null;
                $smselt['mobileno'] = "+237" . str_replace("237", "", $smsarr[$i]['mobileno']);
                $smselt['errorcode'] = -10008;
                $smselt['errordescription'] = "Balance not enough";
            }
            $smselt['senderid'] = $smsarr[$i]['senderid'];
            $smselt['idsmsskysoft'] = $smsarr[$i]['idsmsskysoft'];
            $smselt['message'] = $smsarr[$i]['message'];
            $smsresultarr[$i] = $smselt;
        }
        return array("user"  => $username, "password"  => $password ,"responsecode"  => 1, "responsedescription"  => "success" , "responsemessage"  => "success", "sms"  => $smsresultarr);

    }

    public function sendsms($username, $password, $numbers, $message, $sender){
        $numbers = str_replace(" ", "", $numbers);
        $mobilnos = explode(',', $numbers);
        $user = User::where('username', $username)->where('password', $password)->first();

        $smsarr = array();
        for ($i=0; $i<sizeof($mobilnos); $i++) {
            $smscredit = $this->balancecheck($username, $password);
            $smselt = array();
            if ($smscredit["credit"] > 0) {
                if (preg_match_all('/^(237)?[0-9]{9}$/', $mobilnos[$i], $out)) {
                    $operator = $this->checkoperator($mobilnos[$i]);
                    $settings = Settings::first();

                    $sending_traffic_id = $settings->sending_traffic_id;
                    $sending_traffic = SendingTraffic::find($sending_traffic_id);
                    $sendingservice = SendingService::find($sending_traffic->sending_service_id);
                    $service_name = $sendingservice->name;

                    if ($sending_traffic_id === 1){
                        $smselt = $this->sendTeleossSMS($user, $username, $password, $mobilnos[$i], $message, $sender, $service_name,1, 0);
                    }else if ($sending_traffic_id === 2){
                        $smselt = $this->sendsmsOrange($user, $username, $password, $mobilnos[$i], $message, $sender, $service_name,0, 1);
                    }else if ($sending_traffic_id === 3){
                        if ($operator === 'MTN'){
                            $smselt = $this->sendTeleossSMS($user, $username, $password, $mobilnos[$i], $message, $sender, $service_name,1, 0);
                        }else{
                            $smselt = $this->sendsmsOrange($user, $username, $password, $mobilnos[$i], $message, $sender, $service_name,0, 1);
                        }
                    }
                }else{
                    $smselt['status'] = 'error';
                    $smselt['smsclientid'] = null;
                    $smselt['messageid'] = null;
                    $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                    $smselt['errorcode'] = -10003;
                    $smselt['errordescription'] = "Invalid mobile number";
                }
            }else{
                $smselt['status'] = 'error';
                $smselt['smsclientid'] = null;
                $smselt['messageid'] = null;
                $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                $smselt['errorcode'] = -10008;
                $smselt['errordescription'] = "Balance not enough";
            }
            $smsarr[$i] = $smselt;
        }
        return array("responsecode"  => 1, "responsedescription"  => "success" , "responsemessage"  => "success", "sms"  => $smsarr);

    }


    public function sendTeleossSMS($user, $username, $password, $mobil, $message, $sender, $sendingservice, $teleosscount, $ocmcount){
        //Log::info("Call API TeleossSMS : user = " . $username .' Phone = ' . $mobil);
        $response = $this->teleossapi($username, $password, $mobil, $message, $sender);
        if ($response != null) {
            $xml = simplexml_load_string($response->getBody()->getContents());
            $sms = $xml->sms[0];
            if ($sms != null) {

                //Success
                $smselt['status'] = 'success';
                $smsclientid = (string)intval($sms->{"messageid"});
                $smselt['smsclientid'] = (string)intval($sms->{"messageid"});
                $messageid = md5(intval($sms->{"messageid"}));
                $smselt['messageid'] = $messageid;
                $mobilno = (string)$sms->{"mobile-no"};
                $smselt['mobileno'] = $mobilno;
                $smselt['errorcode'] = null;
                $smselt['errordescription'] = null;
                $this->saveMessage($messageid, $smsclientid, $mobilno, $username, $password, $sender, $message, $sendingservice, 'MTNC');
                //Update user credit
                $oldcredit = $user->credit;
                $user->credit = $oldcredit - 1;
                $user->save();
                //Log::info("Call API TeleossSMS SUCCESS: user = " . $username . ' Phone = ' . $mobil);

            } else {
                //Error AND send with Orange

                if ($teleosscount == 1 && $ocmcount == 1) {
                    //Log::info("Call API TeleossSMS FAIL: user = " . $username . ' Phone = ' . $mobil);

                    $smselt['status'] = 'error';
                    $smsclientid = intval($xml->error[0]->smsclientid);
                    $smselt['smsclientid'] = $smsclientid;
                    $smselt['messageid'] = null;
                    $smselt['mobileno'] = "+237" . str_replace("237", "", $mobil);
                    $error_code = intval($xml->error[0]->{"error-code"});
                    $smselt['errorcode'] = $error_code;
                    $error_description = (string)$xml->error[0]->{"error-description"};
                    $smselt['errordescription'] = $error_description;
                } else {
                    //Send with Orange
                    //Log::info("Call API TeleossSMS FAIL: user = " . $username . ' Phone = ' . $mobil . ' Error = '.(string)$xml->error[0]->{"error-description"});
                    $smselt = $this->sendsmsOrange($user, $username, $password, $mobil, $message, $sender, $sendingservice, $teleosscount, 1);
                }

            }
        }else{

            //Log::info("Call API TeleossSMS FAIL: user = " . $username . ' Phone = ' . $mobil . ' Error = Service unavailable');
            $smselt = $this->sendsmsOrange($user, $username, $password, $mobil, $message, $sender, $sendingservice, $teleosscount, 1);
        }
        return $smselt;
    }


    public function sendsmsOrange($user, $username, $password, $mobil, $message, $sender, $sendingservice, $teleosscount, $ocmcount){
        $reponse = $this->ocmapi($mobil, $message, $sender);
        //Log::info("Call API ORANGE : user = " . $username .' Phone = ' . $mobil. ' code = ' . $reponse['code']);

        if ($reponse['code'] == "00") {
            //Log::info("Call API ORANGE SUCCESS: user = " . $username .' Phone = ' . $mobil);

            //Success
            $smselt['status'] = 'success';
            $smselt['smsclientid'] = $reponse['messageId'];
            $smselt['messageid'] = md5($reponse['messageId']);
            $smselt['mobileno'] = "+237" . str_replace("237", "", $mobil);
            $smselt['errorcode'] = null;
            $smselt['errordescription'] = null;
            $this->saveMessage($smselt['messageid'], $smselt['smsclientid'], $smselt['mobileno'], $username, $password, $sender, $message, $sendingservice, 'OCM');
            //Update user credit
            $oldcredit = $user->credit;
            $user->credit = $oldcredit - 1;
            $user->save();
        } else {
            //Log::info("Call API ORANGE FAIL: user = " . $username .' Phone = ' . $mobil. ' errcode = ' . $reponse['code']);
            if ($teleosscount == 1 && $ocmcount == 1) {
                $smselt['status'] = 'error';
                $smselt['smsclientid'] = null;
                $smselt['messageid'] = null;
                $smselt['mobileno'] = "+237" . str_replace("237", "", $mobil);
                $smselt['errorcode'] = $reponse['code'];
                $smselt['errordescription'] = $reponse['responsedescription'];
            }else{
                $smselt = $this->sendTeleossSMS($user, $username, $password, $mobil, $message, $sender, $sendingservice,1, $ocmcount);
            }
        }
        return $smselt;
    }


    /*public function sendsms($username, $password, $numbers, $message, $sender)
    {
        $numbers = str_replace(" ", "", $numbers);
        $mobilnos = explode(',', $numbers);
        $user = User::where('username', $username)->where('password', $password)->first();

        $smsarr = array();
        for ($i=0; $i<sizeof($mobilnos); $i++) {
            $smscredit = $this->balancecheck($username, $password);
            $smselt = array();
            if ($smscredit["credit"] > 0) {
                if (preg_match_all('/^(237)?[0-9]{9}$/', $mobilnos[$i], $out)) {
                    $response = $this->teleosssmsapi($username, $password, $mobilnos[$i], $message, $sender);
                    $xml = simplexml_load_string($response->getBody()->getContents());
                    $sms = $xml->sms[0];
                    if ($sms != null) {
                        //Success
                        $smselt['status'] = 'success';
                        $smsclientid = intval($sms->{"smsclientid"});
                        $smselt['smsclientid'] = $smsclientid;
                        $messageid = intval($sms->{"messageid"});
                        $smselt['messageid'] = $messageid;
                        $mobilno = (string)$sms->{"mobile-no"};
                        $smselt['mobileno'] = $mobilno;
                        $smselt['errorcode'] = '';
                        $smselt['errordescription'] = '';
                        $this->saveMessage($messageid, $smsclientid, $mobilno, $username, $password, $sender, $message, 'MTN');
                        //Update user credit
                        $oldcredit = $user->credit;
                        $user->credit = $oldcredit - 1;
                        $user->save();
                    } else {
                        //Error
                        $smselt['status'] = 'error';
                        $smsclientid = intval($xml->error[0]->smsclientid);
                        $smselt['smsclientid'] = $smsclientid;
                        $smselt['messageid'] = '';
                        $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                        $error_code = intval($xml->error[0]->{"error-code"});
                        $smselt['errorcode'] = $error_code;
                        $error_description = (string)$xml->error[0]->{"error-description"};
                        $smselt['errordescription'] = $error_description;
                    }
                }else{
                    $smselt['status'] = 'error';
                    $smselt['smsclientid'] = '';
                    $smselt['messageid'] = '';
                    $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                    $smselt['errorcode'] = -10003;
                    $smselt['errordescription'] = "Invalid mobile number";
                }
            }else{
                $smselt['status'] = 'error';
                $smselt['smsclientid'] = '';
                $smselt['messageid'] = '';
                $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                $smselt['errorcode'] = -10008;
                $smselt['errordescription'] = "Balance not enough";
            }
            $smsarr[$i] = $smselt;

        }
        return array("responsecode"  => 1, "responsedescription"  => "success" , "responsemessage"  => "success", "sms"  => $smsarr);
    }*/


    public function sendscheduledsms($username, $password, $numbers, $message, $sender, $scheduletime)
    {
        $numbers = str_replace(" ", "", $numbers);
        $mobilnos = explode(',', $numbers);
        $user = User::where('username', $username)->where('password', $password)->first();

        $smsarr = array();
        for ($i=0; $i<sizeof($mobilnos); $i++) {
            $smscredit = $this->balancecheck($username, $password);
            $smselt = array();
            if ($smscredit["credit"] > 0) {
                if (preg_match_all('/^(237)?[0-9]{9}$/', $mobilnos[$i], $out)) {
                    $settings = Settings::first();
                    $sendingservice = $settings->sendingservice;
                    $response = $this->teleoss_schedulesmsapi($username, $password, $mobilnos[$i], $message, $sender, $scheduletime);
                    $xml = simplexml_load_string($response->getBody()->getContents());
                    $sms = $xml->sms[0];
                    if ($sms != null) {
                        //Success
                        $smselt['status'] = 'success';
                        $smsclientid = intval($sms->{"smsclientid"});
                        $smselt['smsclientid'] = $smsclientid;
                        $messageid = intval($sms->{"messageid"});
                        $smselt['messageid'] = $messageid;
                        $mobilno = (string)$sms->{"mobile-no"};
                        $smselt['mobileno'] = $mobilno;
                        $smselt['errorcode'] = '';
                        $smselt['errordescription'] = '';
                        $this->saveMessage($messageid, $smsclientid, $mobilno, $username, $password, $sender, $message, $sendingservice, 'MTNC');
                        //Update user credit
                        $oldcredit = $user->credit;
                        $user->credit = $oldcredit - 1;
                        $user->save();
                    } else {
                        //Error
                        $smselt['status'] = 'error';
                        $smsclientid = intval($xml->error[0]->smsclientid);
                        $smselt['smsclientid'] = $smsclientid;
                        $smselt['messageid'] = '';
                        $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                        $error_code = intval($xml->error[0]->{"error-code"});
                        $smselt['errorcode'] = $error_code;
                        $error_description = (string)$xml->error[0]->{"error-description"};
                        $smselt['errordescription'] = $error_description;
                    }
                }else{
                    $smselt['status'] = 'error';
                    $smselt['smsclientid'] = '';
                    $smselt['messageid'] = '';
                    $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                    $smselt['errorcode'] = -10003;
                    $smselt['errordescription'] = "Invalid mobile number";
                }
            }else{
                $smselt['status'] = 'error';
                $smselt['smsclientid'] = '';
                $smselt['messageid'] = '';
                $smselt['mobileno'] = "+237" . str_replace("237", "", $mobilnos[$i]);
                $smselt['errorcode'] = -10008;
                $smselt['errordescription'] = "Balance not enough";
            }
            $smsarr[$i] = $smselt;
        }
        return array("responsecode"  => 1, "responsedescription"  => "success" , "responsemessage"  => "success", "sms"  => $smsarr);
    }


    /*public function response($xml, $username, $password, $sender, $message, $mobilnos, $operator){
        $returnarr = array();
        for ($i=0; $i<$xml->count();$i++){
            $sms = $xml->sms[$i];

            $responsearr = array(
                'smsclientid' => '',
                'messageid' => '',
                'mobileno' => '',
                'status' => '',
                'errorcode' => '',
                'errordescription' => '',
            );

            if ($sms != null) {
                $responsearr['status'] = 'success';
                $smsclientid = intval($sms->{"smsclientid"});
                $responsearr['smsclientid'] = $smsclientid;
                $messageid = intval($sms->{"messageid"});
                $responsearr['messageid'] = $messageid;
                $mobilno = (string)$sms->{"mobile-no"};
                $responsearr['mobileno'] = $mobilno;
                $this->saveMessage($messageid, $smsclientid, $mobilno, $username, $password, $sender, $message, $operator, 'OCM');
            }else{
                $responsearr['status'] = 'error';
                $smsclientid = intval($xml->error[0]->smsclientid);
                $responsearr['smsclientid'] = $smsclientid;
                $error_code = intval($xml->error[0]->{"error-code"});
                $responsearr['errorcode'] = $error_code;
                $error_description = (string)$xml->error[0]->{"error-description"};
                $responsearr['errordescription'] = $error_description;
                $responsearr['mobileno'] = "+237" . str_replace("237", "",$mobilnos[$i]);
            }
            $returnarr[$i] = $responsearr;
        }

        return array("responsecode"  => 1, "responsedescription"  => "success" , "responsemessage"  => "success", "sms"  => $returnarr);
    }*/

    public function saveMessage($messageid, $smsclientid, $mobilno, $username, $password, $senderid, $message, $sendingservice, $network)
    {
        $message = new MessageAPI([
            'messageid' => $messageid,
            'smsclientid' => $smsclientid,
            'mobilno' => $mobilno,
            'username' => $username,
            'password' => $password,
            'senderid' => $senderid,
            'message' => $message,
            'messagetype' => $sendingservice,
            'network' => $network,
            'dlrstatus' => '',
            'responsecode' => 2,
            'responsedescription' => 'waiting DLR'
        ]);
        $message->save();
    }


    public function smscredit($username, $password)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://smsvas.com/teleoss/smscredit.jsp', [
            'headers' => ['Accept' => 'application/xml'],
            'form_params' => [
                'user' => $username,
                'password' => $password]
        ]);
        $xml = simplexml_load_string($response->getBody()->getContents());
        return $xml;
    }

    public function balancecheck($username, $password)
    {
        $user = User::where('username', $username)
            ->where('password', $password)
            ->first();

        if ($user != null){
            return array(
                "error" => null,
                "accountexpdate"  => $user->accountexpdate,
                "balanceexpdate"  => $user->balanceexpdate,
                "credit"  => $user->credit
            );
        }else{
            return array("error" => "Invalid username or password");
        }
    }


    public function recharge($username, $password, $credit)
    {
        $user = User::where('username', $username)
            ->where('password', $password)
            ->first();
        if ($user != null){
            try {
                $recharge = new Recharge([
                    'userid' => $user->id,
                    'credit' => $credit,
                    'oldcredit' => $user->credit,
                    'newcredit' => $user->credit + $credit
                ]);
                $recharge->save();

                $oldCredit = $user->credit;
                $newCredit = $oldCredit + $credit;
                $user->credit = $newCredit;
                $user->save();
                if ($user->phone != null) {
                    $message = "Cher client, votre compte a été credité de "
                        . $credit . " SMS. Votre nouveau solde est de "
                        . $user->credit . " SMS.\nMerci de nous faire confiance !";
                    $this->sendsms('stdadmin', 'Admin@1', $user->phone, $message, 'NEXAH');
                }
                return array("status" => "Account credited");
            }catch (\Exception $e){
                return array("error" => $e->getMessage());
            }
        }else{
            return array("error" => "Invalid username or password");
        }
    }


    public function signup($username, $password, $firstname, $lastname, $mobileno, $email, $address,
                           $ccname, $ccemail, $ccmobileno, $tcname, $tcemail, $tcmobileno, $city)
    {

        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://smsvas.com/teleoss/api/signupUser.jsp?', [
            'headers' => ['Accept' => 'application/xml'],
            'form_params' => [
                'apikey' => 'A18CA5DC0B329940',
                'user-id' => $username,
                'password' => $password,
                'password-hint' => $password,
                'first-name' => $firstname,
                'last-name' => $lastname,
                'mobile-number' => $mobileno,
                'email-id' => $email,
                'address' => $address,
                'cc-name' => $ccname,
                'cc-email-id' => $ccemail,
                'cc-mobile-number' => $ccmobileno,
                'tc-name' => $tcname,
                'tc-email-id' => $tcemail,
                'tc-mobile-number' => $tcmobileno,
                'city' => $city,
                'auto-credit' => 'yes']
        ]);
        $result = simplexml_load_string($response->getBody()->getContents());
        return $result;
    }

    public function getDLRLocal($username, $password, $messageid)
    {

        $result = array();
        $user = User::where('username', $username)->where('password', $password)->first();
        if ($user != null){
            $message = MessageAPI::findOrFail($messageid);
            if ($message != null){
                $result["reponsecode"] = $message->responsecode;
                $result["reponsedescription"] = $message->responsedescription;
                $result["mobileno"] = $message->mobilno;
                $result["messageid"] = $message->messageid;
                $result["submittime"] = $message->submittime;
                $result["senttime"] = $message->senttime;
                $result["deliverytime"] = $message->deliverytime;
                $result["status"] = $message->dlrstatus;
            }else{
                $result["reponsecode"] = 0;
                $result["reponsedescription"] = "Message not found";
            }
        }else{
            $result["reponsecode"] = 0;
            $result["reponsedescription"] = "Invalid Username or Password";
        }

        return $result;
    }

    public function getDLR($username, $password, $messageid)
    {


        $form_params = [
            'userid' => $username,
            'password' => $password,
            'messageid' => $messageid,
            'redownload'  => 'yes',
            'responcetype' => 'xml'];


        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://smsvas.com/teleoss/getDLR.jsp?', [
            'headers' => ['Accept' => 'application/xml'],
            'form_params' => $form_params
        ]);

        $result = simplexml_load_string($response->getBody()->getContents(),null,LIBXML_NOCDATA);
        return $result;
    }


    public function createapiuser($username, $password, $phone, $credit, $has_dlr_url, $dlr_url, $batch_dlr_url,$accountexpdate, $balanceexpdate, $company)
    {
        try {
            $user = new User([
                'username' => $username,
                'password' => $password,
                'phone' => $phone,
                'credit' => $credit,
                'company' => $company,
                'has_dlr_url' => $has_dlr_url,
                'dlr_url' => $dlr_url,
                'batch_dlr_url' => $batch_dlr_url,
                'accountexpdate' => $accountexpdate,
                'balanceexpdate' => $balanceexpdate
            ]);
            $user->save();
            return array("responsecode" => 1, "responsedescription" => "Success", "message" => "User successfully created");
        }catch (\Exception $e){
            return array("responsecode" => 0, "responsedescription" => "error", "message" => $e->getMessage());
        }
    }


    public function getapisms($username, $password, $from, $to, $limit)
    {
        $user = User::where('username', $username)
            ->where('password', $password)
            ->first();

        if ($user != null){

            $smsarr = MessageAPI::where('username', $username)
                ->where('password', $password)
                ->whereBetween('created_at', [$from, $to])
                ->offset(0)
                ->limit($limit)
                ->latest()
                ->get();

            return array("responsecode" => 1, "responsedescription" => "Success", "sms" => $smsarr);
        }else{
            return array("responsecode" => 0, "responsedescription" => "Error", "message" => "Invalid username or password");
        }

    }

    public function getusers()
    {
        $users = User::get();
        if ($users != null){
            return array("responsecode" => 1, "responsedescription" => "Success", "users" => $users);
        }else{
            return array("responsecode" => 0, "responsedescription" => "Error", "message" => "Users not found");
        }

    }

    public function deliveryinfos($username, $password, $from, $to)
    {
        $infos = array();
        $from = $from. ' 00:00:00';
        $to = $to. ' 23:59:59';

        if ($username != null && $password != null){
            $user = User::where('username', $username)
                ->where('password', $password)
                ->first();

            if ($user != null){

                $smsarr = MessageAPI::where('username', $username)
                    ->where('password', $password)
                    ->whereBetween('created_at', [$from, $to])
                    ->latest()
                    ->get();

                $nbsent = count($smsarr);
                $info = array(
                    'user' => $username,
                    'credit' => $user->credit,
                    'from' => $from,
                    'to' => $to,
                    'sent' => $nbsent,
                    'dlrsentcount' => 0,
                    'delivered' => 0,
                    'dlvr_rate' => "0%",
                    'undelivered' => 0,
                    'undlvr_rate' => "0%",
                );
                foreach ($smsarr as $sms){
                    if ($sms->dlrstatus == 'DELIVRD'){
                        $info['delivered'] =  $info['delivered'] + 1;
                    }else if($sms->dlrstatus == 'Undelivered'){
                        $info['undelivered'] =  $info['undelivered'] + 1;
                    }
                    if ($sms->dlrsent == 1){
                        $info['dlrsentcount'] =  $info['dlrsentcount'] + 1;
                    }
                }

                $info['dlvr_rate'] =  $nbsent==0?'0%':round(($info['delivered'] * 100)/ $nbsent, 2).'%';
                $info['undlvr_rate'] =  $nbsent==0?'0%':round(($info['undelivered'] * 100)/ $nbsent, 2).'%';
                array_push($infos, $info);
                return array("responsecode" => 1, "responsedescription" => "Success", "infos" => $infos);

            }else{
                return array("responsecode" => 0, "responsedescription" => "Error", "message" => "Invalid username or password");
            }

        }else{
            $users = User::all();
            foreach ($users as $user){
                $smsarr = MessageAPI::where('username', $user->username)
                    ->where('password', $user->password)
                    ->whereBetween('created_at', [$from, $to])
                    ->latest()
                    ->get();

                $nbsent = count($smsarr);
                $info = array(
                    'user' => $user->username,
                    'credit' => $user->credit,
                    'from' => $from,
                    'to' => $to,
                    'sent' => $nbsent,
                    'dlrsentcount' => 0,
                    'delivered' => 0,
                    'dlvr_rate' => "0%",
                    'undelivered' => 0,
                    'undlvr_rate' => "0%"
                );
                foreach ($smsarr as $sms){
                    if ($sms->dlrstatus == 'DELIVRD'){
                        $info['delivered'] =  $info['delivered'] + 1;
                    }else if($sms->dlrstatus == 'Undelivered'){
                        $info['undelivered'] =  $info['undelivered'] + 1;
                    }
                    if ($sms->dlrsent == 1){
                        $info['dlrsentcount'] =  $info['dlrsentcount'] + 1;
                    }
                }

                $info['dlvr_rate'] =  $nbsent==0?'0%':round(($info['delivered'] * 100)/ $info['sent'], 2).'%';
                $info['undlvr_rate'] =  $nbsent==0?'0%':round(($info['undelivered'] * 100)/ $info['sent'], 2).'%';
                array_push($infos, $info);
            }
            return array("responsecode" => 1, "responsedescription" => "Success", "infos" => $infos);
        }
    }

    public function updatesettings($service_traffic_id, $dlrservicestatus,$sendailyrepport){
        try {
            $settings = Settings::first();
            $settings->service_traffic_id = $service_traffic_id;
            $settings->dlrservicestatus = $dlrservicestatus;
            $settings->sendailyrepport = $sendailyrepport;
            $settings->save();
            return array("responsecode" => 1, "responsedescription" => "success", "message" => "settings updated successfully");
        }catch (\Exception $e){
            return array("responsecode" => 0, "responsedescription" => "error", "message" => $e->getMessage());
        }
    }

    public function checkuser($username, $password)
    {
        $user = User::where('username', $username)
            ->where('password', $password)
            ->first();

        if ($user != null){
            return array("responsecode" => 1, "responsedescription" => "success", "user" => $user);
        }else{
            return array("responsecode" => 0, "responsedescription" => "error", "message" => "user not found");
        }
    }
}
