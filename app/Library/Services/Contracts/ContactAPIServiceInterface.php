<?php
/**
 * Created by PhpStorm.
 * User: eddy
 * Date: 13/12/18
 * Time: 10:57
 */

namespace App\Library\Services\Contracts;


interface ContactAPIServiceInterface
{

    public function filter($ville, $quartier, $sexe, $age);
    public function createcampaign($id, $username, $password, $mobilesarr, $mobilesstr, $message, $sender, $filters, $scheduletime);


}
