<?php
/**
 * Created by PhpStorm.
 * User: eddy
 * Date: 13/12/18
 * Time: 10:57
 */

namespace App\Library\Services\Contracts;


interface SSSMSAPIServiceInterface

{

    //Configure SMS API Setting
    public function sendsmsteleoss($smsid, $username, $password, $mobil, $message,
                                   $sender, $sendingservice, $traffic_id1_count,
                                   $traffic_id2_count,$traffic_name,$traffic_id, $stackid, $campaignid);
    public function sendsmsocm    ($smsid, $username, $password, $mobil, $message,
                                   $sender, $sendingservice, $traffic_id1_count,
                                   $traffic_id2_count, $traffic_name,$traffic_id, $stackid, $campaignid);
    public function sendsmsmtn    ($smsid, $username, $password, $mobil, $message,
                                   $sender, $sendingservice, $traffic_id1_count,
                                   $traffic_id2_count, $traffic_name,$traffic_id, $stackid, $campaignid);
    public function sendsmsbankai($smsid, $username, $password, $mobil, $message,
                                  $sender, $sendingservice, $traffic_name,$traffic_id, $stackid, $campaignid);
}
