<?php
/**
 * Created by PhpStorm.
 * User: eddy
 * Date: 13/12/18
 * Time: 10:57
 */

namespace App\Library\Services\Contracts;


interface EventAPIServiceInterface

{

    //public function createEvent($username,$password,$name,$message,$senderid,$details,$status,$hour,$day,$repeat);
    public function createEvent($username,$password,$name,$message,$senderid,$details,$status,$week, $file);
    public function updateEvent($username,$password,$eventid,$name,$message,$senderid,$details,$status,$week, $file);
    public function deleteEvent($username,$password, $eventid);
    public function enable($username,$password,$eventid,$status);
    public function listEvent($username,$password);
    public function listEventContact($username,$password,$eventid);
    public function listEventMessages($username,$password,$eventid);
}
