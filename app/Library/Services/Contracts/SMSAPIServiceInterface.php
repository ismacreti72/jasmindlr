<?php
/**
 * Created by PhpStorm.
 * User: eddy
 * Date: 13/12/18
 * Time: 10:57
 */

namespace App\Library\Services\Contracts;


interface SMSAPIServiceInterface

{

    public function isBlockedSender($sender);

    public function checkuser($username,$password);
    public function notify($username,$password,$notify);
    public function getsmsstatus($smsids,$numbers, $campaignids);
    //Teleoss APIs
    public function getapisms($username,$password,$from,$to);
    public function deliveryinfos($username,$password,$from,$to);
    public function listActiveUsers($username,$password,$from,$to);


    public function sendsms($username, $password, $numbers, $message, $sender,$scheduletime, $stackid, $campaignid);
    //public function sendsms2($username, $password, $numbers, $message, $sender);

    //public function sendscheduledsms($username, $password, $numbers, $message, $sender, $scheduletime);
    //public function sendbulksms($username, $password, $smsarr, $scheduletime, $stackid, $campaignid);
    public function sendbulksms($username, $password, $smsarr, $scheduletime);
    public function sendrealsms($data);


    //Configure SMS API Setting
    //public function sendTeleossSMS($user, $username, $password, $mobil, $message, $sender, $sendingservice, $teleosscount, $ocmcount);
    //public function sendsmsOrange($user, $username, $password, $mobil, $message, $sender, $sendingservice, $teleosscount, $ocmcount);

    public function smscredit($username, $password);
    public function balancecheck($username, $password);

    public function recharge($username, $password, $credit);
    public function rechargesubuser($username, $password, $subuserid,$credit);
    public function rechargeteleossuser($username, $credit,$validity);

    public function getsubuser($username, $password, $subuserid);

    public function createapiuser($username, $password, $phone, $credit,
                                  $has_dlr_url, $dlr_url, $batch_dlr_url,
                                  $accountexpdate, $balanceexpdate,
                                  $company,$sending_traffic_id,$senderid);

    public function addsubuser($username,$password,$subusername,$subuserpassword, $phone,$senderid,$credit,$company,$isAdmin);
    public function updatesubuser($admin_user,$admin_password,$subuserid,$username,
                                  $password,$phone,$senderid,$company);

    public function deletesubuser($username, $password,$subuserid);

    public function getusers();
    public function getsubusers();


    public function signup($username, $password, $firstname, $lastname, $mobileno, $email, $address,
                           $ccname, $ccemail, $ccmobileno, $tcname, $tcemail, $tcmobileno, $city);

    public function getDLR($username, $password, $messageid);
    public function getDLRLocal($username, $password, $messageid);
    public function listdlr($messageids);

    public function updatesettings($dlrservicestatus,$sendailyrepport,$phone_regex,$sms_size, $admin_user_id);
    public function updatepassword($username, $password, $newpassword);

    public function subusers($username, $password);
    public function reportsubuser($user,$password,$from,$to);
    public function reportuser($user,$password,$subusername,$from,$to);

    public function getOcmBalance();

}
