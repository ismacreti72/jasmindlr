<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="referrer" content="origin">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400" rel="stylesheet">

    <title>Nexah SMS Report</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

</head>
<body>
<p>Bonjour,</p>
@if($data['type'] == "Daily")
    <p>Vous trouverez ci-dessous le rapport quotidien des Push SMS.</p>
@elseif($data['type'] == "Weekly")
    <p>Vous trouverez ci-dessous le rapport hebdomadaire des Push SMS.</p>
@elseif($data['type'] == "Monthly")
    <p>Vous trouverez ci-dessous le rapport mensuel des Push SMS.</p>
@endif
<a href="">
    <img src="{{ $message->embed(str_replace("/var/www/html/","https://smsvas.com/",$data['file_url'])) }}" style="max-width: 100%;padding-top: 20px;" alt="report_image">
</a>
</body>
</html>
