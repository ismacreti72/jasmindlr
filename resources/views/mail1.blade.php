<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="referrer" content="origin">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/backend.css') }}" >
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400" rel="stylesheet">

</head>
<body id="body" style="font-family: 'Quicksand', sans-serif;align-content: center" class="justify-content-center">
<div>
    <div class="row justify-content-center" style="margin-top: 20px">
        <div class="col-sm-1">
        </div>
        <div class="col-sm-3">
            <img src="{{ asset("img/nexah_logo.jpg") }}" width="200" height="70" alt="nexah_logo">
        </div>
        <div class="col-sm-6" style="margin-top: 15px">
            <strong><h1>Nexah Bulk SMS {{ $report->type }} Report </h1></strong>
        </div>
        <div class="col-sm-2">
        </div>
    </div>
    <div class="row justify-content-center" style="margin-top: 20px">
        <div class="col-sm-2">
            <div class="card">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm text-primary">{{ $traffic['stat']['total_sms_unit'] }}</div>
                        <div class="text-value-sm">{{ $traffic['stat']['mtn'] }}<small style="color: #d39e00">[ MTN ]</small></div>
                        <div class="text-value-sm">{{ $traffic['stat']['ocm'] }}<small style="color: #cb2027">[ OCM ]</small></div>
                        <div class="text-value-sm">{{ $traffic['stat']['bankai'] }}<small>[ BANKAI ]</small></div>
                        <div class="text-muted text-uppercase font-weight-bold small">Crédit Consommé</div>
                    </div>
                </div>
            </div>

            <div class="card" style="margin-top: -25px">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm text-primary">{{ $traffic['stat']['sent'] }}</div>
                        <div class="text-muted text-uppercase font-weight-bold small">SMS Envoyé</div>
                    </div>
                </div>
            </div>
            <!-- /.col-->
            <div class="card" style="margin-top: -25px">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm">{{ $traffic['stat']['delivered'] }} <small style="color: #009926">[ {{ $traffic['stat']['delivRate'] }} ]</small></div>
                        <div class="text-muted text-uppercase font-weight-bold small">SMS Délivré</div>
                    </div>
                </div>
            </div>
            <!-- /.col-->
            <div class="card" style="margin-top: -25px">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm">{{ $traffic['stat']['undelivered'] }} <small style="color: #cb2027">[ {{ $traffic['stat']['undelivRate'] }} ]</small></div>
                        <div class="text-muted text-uppercase font-weight-bold small">SMS Non délivré</div>
                    </div>
                </div>
            </div>
            <!-- /.col-->
            <div class="card" style="margin-top: -25px">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm">{{ $traffic['stat']['pending'] }} <small style="color: #d39e00">[ {{ $traffic['stat']['pendingRate'] }} ]</small></div>
                        <div class="text-muted text-uppercase font-weight-bold small">SMS encours</div>
                    </div>
                </div>
            </div>
            <div class="card" style="margin-top: -25px">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm">{{ $report->current_ocm_balance }} </div>
                        <div class="text-muted text-uppercase font-weight-bold small">Solde Actuel OCM</div>
                    </div>
                </div>
            </div>
            <div class="card" style="margin-top: -25px">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm">---</div>
                        <div class="text-muted text-uppercase font-weight-bold small">Solde Actuel MTN</div>
                    </div>
                </div>
            </div>
            <div class="card" style="margin-top: -25px">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm">---</div>
                        <div class="text-muted text-uppercase font-weight-bold small">Solde Actuel BANKAI</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6" style="margin-left: -20px; margin-right: -20px">
            <div class="card">
                <div class="card-body">
                    <div class="chart-wrapper" id="chart-wrapper">
                        <canvas id="canvas"></canvas>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- /.col-->
                <div class="col-sm-4">
                    <div class="card text-black-50 bg-orange">
                        <div class="card-body p-3 d-flex align-items-center">
                            <div>
                                <div class="text-value-sm">{{ $traffic['stat']['mtn'] }}</div>
                                <div class="text-muted text-uppercase font-weight-bold small">CREDIT SMS MTN</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-->
                <div class="col-sm-4">
                    <div class="card text-black-50 bg-stack-overflow">
                        <div class="card-body p-3 d-flex align-items-center">
                            <div>
                                <div class="text-value-sm">{{ $traffic['stat']['ocm'] }}</div>
                                <div class="text-muted text-uppercase font-weight-bold small">CREDIT SMS OCM</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-->
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <div>
                                <div class="text-value-sm">{{ $traffic['stat']['bankai'] }}</div>
                                <div class="text-muted text-uppercase font-weight-bold small">CREDIT SMS BANKAI</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- /.col-->
                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <div>
                                <div class="text-value-sm text-primary">{{ $traffic['stat']['total_sms_unit'] }}</div>
                                <div class="text-muted text-uppercase font-weight-bold small">SMS Envoyé BANKAI</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-->
                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <div>
                                <div class="text-value-sm">{{ $traffic['stat']['delivered'] }} <small style="color: #009926">[ {{ $traffic['stat']['delivRate'] }} ]</small></div>
                                <div class="text-muted text-uppercase font-weight-bold small">SMS Délivré BANKAI</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-->
                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <div>
                                <div class="text-value-sm">{{ $traffic['stat']['delivered'] }} <small style="color: #cb2027">[ {{ $traffic['stat']['delivRate'] }} ]</small></div>
                                <div class="text-muted text-uppercase font-weight-bold small">SMS Non Délivré BANKAI</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <div>
                                <div class="text-value-sm">{{ $traffic['stat']['delivered'] }} <small style="color: #d39e00">[ {{ $traffic['stat']['delivRate'] }} ]</small></div>
                                <div class="text-muted text-uppercase font-weight-bold small">SMS encours BANKAI</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card" style="margin-top: 10px">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <h6 class="card-title mb-0">
                                <strong>Statut des Utilisateurs</strong>
                            </h6>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-value-sm">Actif <span class="badge badge-secondary badge-pill" id="valid_mdn"> {{ $status['active_users'] }} </span> </th>
                                        <th class="text-value-sm">Dormant <span class="badge badge-secondary badge-pill" id="valid_mdn"> {{ $status['dormant_users'] }} </span></th>
                                        <th class="text-value-sm">Inactif <span class="badge badge-secondary badge-pill" id="valid_mdn"> {{ $status['inactive_users'] }} </span></th>
                                        <th class="text-value-sm">Total <span class="badge badge-secondary badge-pill" id="valid_mdn"> {{ $status['total_users'] }} </span></th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="card">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm text-primary">{{ $traffic['stat']['total_sms_unit'] }}</div>
                        <div class="text-muted text-uppercase font-weight-bold small">SMS Envoyé MTN</div>
                    </div>
                </div>
            </div>

            <div class="card" style="margin-top: -25px">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm">{{ $traffic['stat']['delivered'] }} <small style="color: #009926">[ {{ $traffic['stat']['delivRate'] }} ]</small></div>
                        <div class="text-muted text-uppercase font-weight-bold small">SMS Délivré MTN</div>
                    </div>
                </div>
            </div>
            <!-- /.col-->
            <div class="card" style="margin-top: -25px">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm">{{ $traffic['stat']['delivered'] }} <small style="color: #cb2027">[ {{ $traffic['stat']['delivRate'] }} ]</small></div>
                        <div class="text-muted text-uppercase font-weight-bold small">SMS Non Délivré MTN</div>
                    </div>
                </div>
            </div>
            <div class="card" style="margin-top: -25px">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm">{{ $traffic['stat']['delivered'] }} <small style="color: #d39e00">[ {{ $traffic['stat']['delivRate'] }} ]</small></div>
                        <div class="text-muted text-uppercase font-weight-bold small">SMS encours MTN</div>
                    </div>
                </div>
            </div>
            <!-- /.col-->
            <div class="card">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm text-primary">{{ $traffic['stat']['total_sms_unit'] }}</div>
                        <div class="text-muted text-uppercase font-weight-bold small">SMS Envoyé OCM</div>
                    </div>
                </div>
            </div>

            <div class="card" style="margin-top: -25px">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm">{{ $traffic['stat']['delivered'] }} <small style="color: #009926">[ {{ $traffic['stat']['delivRate'] }} ]</small></div>
                        <div class="text-muted text-uppercase font-weight-bold small">SMS Délivré OCM</div>
                    </div>
                </div>
            </div>
            <!-- /.col-->
            <div class="card" style="margin-top: -25px">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm">{{ $traffic['stat']['delivered'] }} <small style="color: #cb2027">[ {{ $traffic['stat']['delivRate'] }} ]</small></div>
                        <div class="text-muted text-uppercase font-weight-bold small">SMS Non Délivré OCM</div>
                    </div>
                </div>
            </div>
            <div class="card" style="margin-top: -25px">
                <div class="card-body p-3 d-flex align-items-center">
                    <div>
                        <div class="text-value-sm">{{ $traffic['stat']['delivered'] }} <small style="color: #d39e00">[ {{ $traffic['stat']['delivRate'] }} ]</small></div>
                        <div class="text-muted text-uppercase font-weight-bold small">SMS encours OCM</div>
                    </div>
                </div>
            </div>
            <!-- /.col-->

        </div>
    </div>

</div>
</body>
<script src="{{asset("js/chart.min.js")}}"></script>
<script src="{{asset("js/utils.js")}}"></script>
<script src="{{asset("js/jquery-3.3.1.js")}}"></script>
<script type="text/javascript">
    var config = {
        type: 'line',
        data: {
            labels: {!! json_encode($traffic['labels']) !!},

            datasets: [{
                label: 'Envoyé',// [ '+"{{ $traffic['stat']['sent'] }}"+' ]',
                fill: false,
                backgroundColor: window.chartColors.blue,
                borderColor: window.chartColors.blue,

                data: {!! json_encode($traffic['sent']) !!},
            }, {
                label: 'Délivré',// [ '+"{{ $traffic['stat']['delivered'] }}"+' ] ',
                fill: false,
                backgroundColor: window.chartColors.green,
                borderColor: window.chartColors.green,
                data: {!! json_encode($traffic['delivered']) !!},
            }, {
                label: 'Non délivré',// [ '+"{{ $traffic['stat']['undelivered'] }}"+' ] ',
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,
                data: {!! json_encode($traffic['undelivered']) !!},
                fill: false,
            }, {
                label: 'En cours',// [ '+"{{ $traffic['stat']['pending'] }}"+' ] ',
                backgroundColor: window.chartColors.orange,
                borderColor: window.chartColors.orange,
                data: {!! json_encode($traffic['pending']) !!},
                fill: false,
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'TRAFFIC SMS'
            },
            tooltips: {
                mode: 'index',
                intersect: true,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: "{{ $report->axeX }}"
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: "{{ $report->axeY }}"
                    }
                }]
            }
        }
    };

    window.onload = function() {
        var ctx = document.getElementById('canvas').getContext('2d');
        window.myLine = new Chart(ctx, config);
    };


</script>

</html>
