<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="referrer" content="origin">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/backend.css') }}" >
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400" rel="stylesheet">

</head>
<body id="body" style="font-family: 'Quicksand', sans-serif;align-content: center" class="justify-content-center">
<div class="card">
    <div class="card-body">
        <div class="row justify-content-center" style="margin-top: 20px">
            <table id="countday-table" class="table table-striped table-bordered"
                   style="width:100%; border: 5px" >
                <thead>
                <tr style="font-weight: bold;">
                    <td>{{$data["message"]}}</td>
                </tr>
                <tr style="font-weight: bold;">
                    <td>Nom du commercial</td>
                    <td>Nombre d'entreprise(s)</td>
                    <td>Quota</td>
                </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr style="font-weight: normal;">
                            <td>{{$user["name"]}}</td>
                            <td>{{$user["entreprise"]}}</td>
                            <td>{{$user["percent"]}} %</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
