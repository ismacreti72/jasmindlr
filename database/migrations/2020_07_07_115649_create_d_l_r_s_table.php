<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDLRSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('d_l_r_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uid');
            $table->string('connector');
            $table->string('message_status');
            $table->string('done_date');
            $table->string('sub');
            $table->integer('level');
            $table->string('message');
            $table->string('error');
            $table->string('id_smsc');
            $table->string('delivred');
            $table->string('submit_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_l_r_s');
    }
}
